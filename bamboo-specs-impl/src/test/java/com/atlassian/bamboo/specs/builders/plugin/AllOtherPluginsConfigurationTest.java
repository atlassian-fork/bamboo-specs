package com.atlassian.bamboo.specs.builders.plugin;

import com.atlassian.bamboo.specs.api.builders.plan.configuration.AllOtherPluginsConfiguration;
import com.atlassian.bamboo.specs.api.model.plan.configuration.AllOtherPluginsConfigurationProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.util.MapBuilder;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

public class AllOtherPluginsConfigurationTest {

    @Test
    public void shouldBuildProperties() {
        //given
        //when
        final AllOtherPluginsConfigurationProperties properties = EntityPropertiesBuilders.build(new AllOtherPluginsConfiguration()
                .configuration(new MapBuilder()
                        .put("bar", true)
                        .put("baz", false)
                        .build()));

        //then
        assertThat(properties.getAtlassianPlugin().getCompleteModuleKey(), equalTo(AllOtherPluginsConfigurationProperties.ALL_OTHER_PLUGINS_MODULE_KEY));
        assertThat(properties.getConfiguration().get("bar"), equalTo(Boolean.TRUE));
        assertThat(properties.getConfiguration().get("baz"), equalTo(Boolean.FALSE));
    }

    @Test
    public void shouldHandleNull() {
        //given
        //when
        final AllOtherPluginsConfigurationProperties properties = EntityPropertiesBuilders.build(new AllOtherPluginsConfiguration()
                .configuration(new MapBuilder().put("bar", null).build()));

        //then
        assertThat(properties.getConfiguration().get("bar"), nullValue());
    }


}