package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.model.task.AntTaskProperties;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;

public class AntTaskTest {
    @Test
    public void testBuilderOK() {
        final String target = "clean test.run";
        final String environment = "ANT_OPTS=\"-Dvery.important.flag=true\"";
        final String directory = "directory";
        final String executableLabel = "Ant";

        final AntTask antBuilder = new AntTask()
                .target(target)
                .executableLabel(executableLabel)
                .hasTests(true)
                .environmentVariables(environment)
                .workingSubdirectory(directory);

        final AntTaskProperties properties = antBuilder.build();
        assertThat(properties.getTarget(), is(target));
        assertThat(properties.getExecutableLabel(), is(executableLabel));
        assertThat(properties.getEnvironmentVariables(), is(environment));
        assertThat(properties.getWorkingSubdirectory(), is(directory));
        assertThat(properties.getAtlassianPlugin().getCompleteModuleKey(), is("com.atlassian.bamboo.plugins.ant:task.builder.ant"));
        assertTrue(properties.isHasTests());
    }

    @Test(expected = PropertiesValidationException.class)
    public void testValidationFails() {
        final AntTask antBuilder = new AntTask()
                .executableLabel("")
                .target("");

        antBuilder.build();
    }
}