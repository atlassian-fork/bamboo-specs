package com.atlassian.bamboo.specs.model.trigger;

import com.atlassian.bamboo.specs.api.builders.trigger.RepositoryBasedTrigger.TriggeringRepositoriesType;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Collections;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;

public class RemoteTriggerPropertiesTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testGetAtlassianPlugin() throws Exception {
        assertThat(new RemoteTriggerProperties(
                        "My remote trigger",
                        true,
                        TriggeringRepositoriesType.ALL,
                        Collections.emptyList(),
                        null
                ).getAtlassianPlugin().getCompleteModuleKey(),
                equalTo("com.atlassian.bamboo.triggers.atlassian-bamboo-triggers:remote"));
    }

    @Test
    public void testGetTriggerIPAddresses() throws Exception {
        assertThat(new RemoteTriggerProperties(
                "My remote trigger",
                true,
                TriggeringRepositoriesType.ALL,
                Collections.emptyList(),
                "1.1.1.1, 2.2.2.2"
        ).getTriggerIPAddresses(), equalTo("1.1.1.1, 2.2.2.2"));
    }

    @Test
    public void testValidateTriggerIpAddressSingleIPShouldNotFail() throws Exception {
        new RemoteTriggerProperties(
                "My remote trigger",
                true,
                TriggeringRepositoriesType.ALL,
                Collections.emptyList(),
                "1.1.1.1"
        ).validate();
    }

    @Test
    public void testValidateTriggerIpAddressMultipleIPsShouldNotFail() throws Exception {
        new RemoteTriggerProperties(
                "My remote trigger",
                true,
                TriggeringRepositoriesType.ALL,
                Collections.emptyList(),
                "1.1.1.1, 2.2.2.2,3.3.3.3"
        ).validate();
    }

    @Test
    public void testValidateTriggerIpAddressEmptyShouldNotFail() throws Exception {
        new RemoteTriggerProperties(
                "My remote trigger",
                true,
                TriggeringRepositoriesType.ALL,
                Collections.emptyList(),
                ""
        ).validate();
    }

    @Test
    public void testValidateTriggerIpAddressNullShouldNotFail() throws Exception {
        new RemoteTriggerProperties(
                "My remote trigger",
                true,
                TriggeringRepositoriesType.ALL,
                Collections.emptyList(),
                null
        ).validate();
    }

    @Test
    public void testValidateTriggerIpAddressIncorrectIPShouldFail() throws Exception {
        expectedException.expect(PropertiesValidationException.class);
        expectedException.expectMessage("Invalid coma separated IPs/CIDRs: A.B.C.D, error when validating A.B.C.D");
        new RemoteTriggerProperties(
                "My remote trigger",
                true,
                TriggeringRepositoriesType.ALL,
                Collections.emptyList(),
                "A.B.C.D"
        ).validate();
    }

    @Test
    public void testValidateTriggerIpAddressIncorrectIPsShouldFail() throws Exception {
        expectedException.expect(PropertiesValidationException.class);
        expectedException.expectMessage("Invalid coma separated IPs/CIDRs: 1.1.1.1, 2.2.2.2, A.B.C.D, 3.3.3.3, error when validating A.B.C.D");
        new RemoteTriggerProperties(
                "My remote trigger",
                true,
                TriggeringRepositoriesType.ALL,
                Collections.emptyList(),
                "1.1.1.1, 2.2.2.2, A.B.C.D, 3.3.3.3"
        ).validate();
    }

    @Test
    public void testEqualsOverridden() throws Exception {
        assertThat(new RemoteTriggerProperties(
                        "My remote trigger",
                        true,
                        TriggeringRepositoriesType.ALL,
                        Collections.emptyList(),
                        "1.1.1.1, 2.2.2.2,3.3.3.3"),
                equalTo(new RemoteTriggerProperties(
                        "My remote trigger",
                        true,
                        TriggeringRepositoriesType.ALL,
                        Collections.emptyList(),
                        "1.1.1.1, 2.2.2.2,3.3.3.3"
                ))
        );
    }

    @Test
    public void testEqualsOverriddenNotEqual() throws Exception {
        assertThat(new RemoteTriggerProperties(
                        "My remote trigger",
                        true,
                        TriggeringRepositoriesType.ALL,
                        Collections.emptyList(),
                        "1.1.1.1, 2.2.2.2,3.3.3.3"),
                not(equalTo(new RemoteTriggerProperties(
                        "My remote trigger",
                        true,
                        TriggeringRepositoriesType.ALL,
                        Collections.emptyList(),
                        "1.1.1.1, 3.3.3.3"
                )))
        );
    }
}
