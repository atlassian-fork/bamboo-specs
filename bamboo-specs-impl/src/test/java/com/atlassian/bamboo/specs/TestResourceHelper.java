package com.atlassian.bamboo.specs;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertNotNull;

/**
 * Helpers for retrieving resources from classpath for tests.
 */
public final class TestResourceHelper {
    private TestResourceHelper() {
    }

    /**
     * Returns a URL for the given resource.
     * <p>
     * This method will try to locate the resource in a smart way, i.a. by using {@link Class#getSimpleName()} as a
     * possible resource location. For example, for a class {@code a.b.c.MyClass}, and a resource
     * {@code a/b/c/MyClass/my-resource}, it is sufficient to call {@code getResourceUrl(MyClass.class, "my-resource")}.
     *
     * @param clazz    class for which the resource is being retrieved
     * @param resource path to the resource
     * @return URL for the given resource if found, null otherwise
     */
    @Nullable
    public static URL getResourceUrl(@NotNull Class<?> clazz, @NotNull String resource) {
        final ClassLoader classLoader = clazz.getClassLoader();
        final Stream<URL> resourcesStream = Stream.of(
                clazz.getResource(resource),
                clazz.getResource(clazz.getSimpleName() + "/" + resource),
                classLoader != null ? classLoader.getResource(resource) : null,
                classLoader != null ? classLoader.getResource(clazz.getSimpleName() + "/" + resource) : null);
        return resourcesStream
                .filter(Objects::nonNull)
                .findFirst()
                .orElse(null);
    }

    /**
     * Obtain a resource from the class path as string.
     * <p>
     * This method will try to locate the resource in a smart way, i.a. by using {@link Class#getSimpleName()} as a
     * possible resource location. For example, for a class {@code a.b.c.MyClass}, and a resource
     * {@code a/b/c/MyClass/my-resource}, it is sufficient to call
     * {@code getResourceAsString(MyClass.class, "my-resource")}.
     *
     * @param clazz    class for which the resource is being retrieved
     * @param resource path to the resource
     * @return resource as string
     * @throws Exception      on any error
     * @throws AssertionError if the resource can't be retrieved
     */
    @NotNull
    public static String getResourceAsString(@NotNull Class<?> clazz, @NotNull String resource) throws Exception {
        final URL resourceUrl = getResourceUrl(clazz, resource);
        assertNotNull("Unable to find resource " + resource, resourceUrl);
        try (InputStream is = resourceUrl.openStream()) {
            assertNotNull("Unable to open resource " + resource, is);
            return new BufferedReader(new InputStreamReader(is)).lines().collect(Collectors.joining("\n"));
        }
    }
}
