package com.atlassian.bamboo.specs.util;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class WhitelistedYamlConstructorTest {

    private WhitelistedYamlConstructor constructor = new WhitelistedYamlConstructor();

    @Test
    public void isClassAllowed() {
        assertClassAllowed("com.atlassian.bamboo.specs.util.BambooSpecProperties", true);
        assertClassAllowed("com.atlassian.bamboo.specs.api.model.plan.PlanProperties", true);
        assertClassAllowed("com.atlassian.bamboo.specs.api.model.repository.PlanRepositoryLinkProperties$LinkedGlobalRepository", true);
        assertClassAllowed("com.atlassian.bamboo.specs.model.task.ScriptTaskProperties", true);

        assertClassAllowed("com.atlassian.bamboo.NonSpecsClass", false);
        assertClassAllowed("java.util.Date", false);
    }

    private void assertClassAllowed(String className, boolean allowed) {
        assertThat(constructor.isClassAllowed(className), is(allowed));
    }
}