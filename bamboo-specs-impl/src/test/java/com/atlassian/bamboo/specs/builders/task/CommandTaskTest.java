package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.requirement.Requirement;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.model.task.CommandTaskProperties;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Collections;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class CommandTaskTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testEqualsOnAllProperties() throws Exception {
        final Requirement requirement = Requirement.equals(RandomStringUtils.randomAlphabetic(5), RandomStringUtils.randomAlphabetic(5));
        assertThat(new CommandTask()
                        .description("hello command 1")
                        .enabled(true)
                        .executable("pwd")
                        .argument("arg1 arg2 arg3")
                        .environmentVariables("SOME_HOME=/home ANOTHER_HOME=/home")
                        .workingSubdirectory("work/dir")
                        .requirements(requirement)
                        .build(),
                equalTo(new CommandTaskProperties(
                        "hello command 1",
                        true,
                        "pwd",
                        "arg1 arg2 arg3",
                        "SOME_HOME=/home ANOTHER_HOME=/home",
                        "work/dir",
                        Collections.singletonList(EntityPropertiesBuilders.build(requirement)),
                        Collections.emptyList()))
        );
    }

    @Test
    public void testMinimalConfiguration() throws Exception {
        new CommandTask()
                .executable("foo")
                .build();
    }

    @Test
    public void testValidation() throws Exception {
        exception.expect(PropertiesValidationException.class);
        exception.expectMessage("Command task: Executable is not defined");
        new CommandTask()
                .build();
    }

}