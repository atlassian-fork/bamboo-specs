package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.BasicTaskConfigProvider;
import com.atlassian.bamboo.specs.api.builders.repository.VcsRepositoryIdentifier;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;
import com.atlassian.bamboo.specs.model.task.VcsPushTaskProperties;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Collections;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class VcsPushTaskTest {
    @Test
    @Parameters(source = BasicTaskConfigProvider.class)
    public void worksWithDefaultRepository(boolean enabled, String description) {
        assertEquals(
                new VcsPushTaskProperties(
                        description,
                        enabled,
                        Collections.emptyList(),
                        Collections.emptyList(),
                        true,
                        null,
                        null),
                new VcsPushTask()
                        .description(description)
                        .enabled(enabled)
                        .defaultRepository()
                        .build());
    }

    @Test
    @Parameters(source = BasicTaskConfigProvider.class)
    public void worksWithRepositoryName(boolean enabled, String description) {
        assertEquals(
                new VcsPushTaskProperties(
                        description,
                        enabled,
                        Collections.emptyList(),
                        Collections.emptyList(),
                        false,
                        new VcsRepositoryIdentifierProperties("bamboo-node-plugin", null),
                        null),
                new VcsPushTask()
                        .description(description)
                        .enabled(enabled)
                        .repository("bamboo-node-plugin")
                        .build());
    }

    @Test
    @Parameters(source = BasicTaskConfigProvider.class)
    public void worksWithRepositoryIdentifier(boolean enabled, String description) {
        assertEquals(
                new VcsPushTaskProperties(
                        description,
                        enabled,
                        Collections.emptyList(),
                        Collections.emptyList(),
                        false,
                        new VcsRepositoryIdentifierProperties(null, new BambooOidProperties("123456789")),
                        null),
                new VcsPushTask()
                        .description(description)
                        .enabled(enabled)
                        .repository(new VcsRepositoryIdentifier().oid("123456789"))
                        .build());
    }

    @Test(expected = PropertiesValidationException.class)
    @Parameters(source = BasicTaskConfigProvider.class)
    public void repositoryIsRequired(boolean enabled, String description) {
        new VcsPushTask()
                .description(description)
                .enabled(enabled)
                .build();
    }
}
