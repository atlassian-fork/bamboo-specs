package com.atlassian.bamboo.specs.codegen.emitters.task;

import com.atlassian.bamboo.specs.TestResourceHelper;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;
import com.atlassian.bamboo.specs.model.task.VcsTagTaskProperties;
import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.assertEquals;

public class VcsTagTaskEmitterTest {
    @Test
    public void emitCodeWithDefaultRepository() throws Exception {
        final VcsTagTaskProperties taggingTaskProperties = new VcsTagTaskProperties(
                "", true, Collections.emptyList(), Collections.emptyList(), true, null, "sub/directory", "custom-tag");
        final String expectedCode = TestResourceHelper.getResourceAsString(getClass(), "taskWithDefaultRepository.java");
        final String emittedCode = new VcsTagTaskEmitter().emitCode(new CodeGenerationContext(), taggingTaskProperties);
        assertEquals(expectedCode, emittedCode);
    }

    @Test
    public void emitCodeWithCustomRepository() throws Exception {
        final VcsTagTaskProperties taskProperties = new VcsTagTaskProperties(
                "", true, Collections.emptyList(), Collections.emptyList(), false,
                new VcsRepositoryIdentifierProperties("bamboo-plugin", null), "sub/directory", "custom-tag");
        final String expectedCode = TestResourceHelper.getResourceAsString(getClass(), "taskWithCustomRepository.java");
        final String emittedCode = new VcsTagTaskEmitter().emitCode(new CodeGenerationContext(), taskProperties);
        assertEquals(expectedCode, emittedCode);
    }
}
