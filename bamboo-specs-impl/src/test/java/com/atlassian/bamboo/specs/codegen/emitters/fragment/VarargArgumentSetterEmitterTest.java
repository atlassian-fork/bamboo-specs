package com.atlassian.bamboo.specs.codegen.emitters.fragment;

import com.atlassian.bamboo.specs.api.codegen.CodeEmitter;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.api.codegen.annotations.CodeGenerator;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.google.common.collect.Lists;
import java.util.Collections;
import java.util.Objects;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;


public class VarargArgumentSetterEmitterTest {
    NonEmptyEntityProperties nonEmpty = new NonEmptyEntityProperties();
    EmptyEntityProperties empty = new EmptyEntityProperties();
    ErrorEntityProperties error = new ErrorEntityProperties();
    
    
    public VarargArgumentSetterEmitterTest() {
    }
    
    @Before
    public void setup() throws CodeGenerationException {
    }

    @Test
    public void testEmptyVarArg() throws CodeGenerationException {
        CodeGenerationContext context = new CodeGenerationContext();
        VarargArgumentSetterEmitter varArg = new VarargArgumentSetterEmitter("methodName");
        
        String result = varArg.emitCode(context, Collections.emptyList());
        assertEquals("", result);
    }
    
    @Test
    public void testOnlyEmptyEmitters() throws CodeGenerationException {
        CodeGenerationContext context = new CodeGenerationContext();
        VarargArgumentSetterEmitter varArg = new VarargArgumentSetterEmitter("methodName");
        
        String result = varArg.emitCode(context, Lists.newArrayList(empty, empty, empty));
        assertEquals("", result);
    }

    @Test
    public void testOnlyNonEmptyEmitters() throws CodeGenerationException {
        CodeGenerationContext context = new CodeGenerationContext();
        VarargArgumentSetterEmitter varArg = new VarargArgumentSetterEmitter("methodName");
        
        String result = varArg.emitCode(context, Lists.newArrayList(nonEmpty, nonEmpty, nonEmpty));
        assertEquals(
            ".methodName(new Code(),\n" +
            "    new Code(),\n" +
            "    new Code())", result);
    }
    
    @Test
    public void testFirstNonEmptyEmitterFollowedByEmpty() throws CodeGenerationException {
        CodeGenerationContext context = new CodeGenerationContext();
        VarargArgumentSetterEmitter varArg = new VarargArgumentSetterEmitter("methodName");
        
        String result = varArg.emitCode(context, Lists.newArrayList(nonEmpty, empty, empty));
        assertEquals(
            ".methodName(new Code())", result);
    }
    
    @Test
    public void testFirstEmptyEmitterFollowedByNonEmpty() throws CodeGenerationException {
        
        CodeGenerationContext context = new CodeGenerationContext();
        VarargArgumentSetterEmitter varArg = new VarargArgumentSetterEmitter("methodName");
        
        String result = varArg.emitCode(context, Lists.newArrayList(empty, nonEmpty));
        assertEquals(
            ".methodName(new Code())", result);
    }
    
    @Test
    public void testComplex1() throws CodeGenerationException {
        
        CodeGenerationContext context = new CodeGenerationContext();
        VarargArgumentSetterEmitter varArg = new VarargArgumentSetterEmitter("methodName");
        
        String result = varArg.emitCode(context, Lists.newArrayList(empty, nonEmpty, error, nonEmpty, empty));
        assertEquals(
            ".methodName(new Code(),\n" +
            "    //error\n" +
            "    new Code())", result);
    }
    
    @Test
    public void testComplex2() throws CodeGenerationException {
        
        CodeGenerationContext context = new CodeGenerationContext();
        VarargArgumentSetterEmitter varArg = new VarargArgumentSetterEmitter("methodName");
        
        String result = varArg.emitCode(context, Lists.newArrayList(nonEmpty, empty, error, empty, nonEmpty));
        assertEquals(
            ".methodName(new Code(),\n" +
            "    //error\n" +
            "    new Code())", result);
    }
    
    @Test
    public void testOnlyErrorAndEmpty() throws CodeGenerationException {
        
        CodeGenerationContext context = new CodeGenerationContext();
        VarargArgumentSetterEmitter varArg = new VarargArgumentSetterEmitter("methodName");
        
        String result = varArg.emitCode(context, Lists.newArrayList(empty, error, empty, error));
        assertEquals(
            ".methodName(//error\n" +
            "    //error\n" +
            "    )", result);
    }
    @Test
    public void testErrorFirstNonEmptyLast() throws CodeGenerationException {
        
        CodeGenerationContext context = new CodeGenerationContext();
        VarargArgumentSetterEmitter varArg = new VarargArgumentSetterEmitter("methodName");
        
        String result = varArg.emitCode(context, Lists.newArrayList(error, nonEmpty, error, nonEmpty));
        assertEquals(
            ".methodName(//error\n" +
            "    new Code(),\n" +
            "    //error\n" +
            "    new Code())", result);
    }
    
    @Test
    public void testOnlyError() throws CodeGenerationException {
        
        CodeGenerationContext context = new CodeGenerationContext();
        VarargArgumentSetterEmitter varArg = new VarargArgumentSetterEmitter("methodName");
        
        String result = varArg.emitCode(context, Lists.newArrayList(error ));
        assertEquals(
            ".methodName(//error\n" +
            "    )", result);
    }
    
    
    
    @CodeGenerator(EmptyEmitter.class)
    public static class EmptyEntityProperties implements EntityProperties {
        
        private String name;

        private EmptyEntityProperties() {
        }

        public EmptyEntityProperties(String name) {
            this.name = name;
        }
        

        @Override
        public void validate() {
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 89 * hash + Objects.hashCode(this.name);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final EmptyEntityProperties other = (EmptyEntityProperties) obj;
            if (!Objects.equals(this.name, other.name)) {
                return false;
            }
            return true;
        }
    }
        
    @CodeGenerator(NonEmptyEmitter.class)
    public static class NonEmptyEntityProperties implements EntityProperties {
        
        private String name;

        private NonEmptyEntityProperties() {
        }

        public NonEmptyEntityProperties(String name) {
            this.name = name;
        }
        

        @Override
        public void validate() {
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 89 * hash + Objects.hashCode(this.name);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final NonEmptyEntityProperties other = (NonEmptyEntityProperties) obj;
            if (!Objects.equals(this.name, other.name)) {
                return false;
            }
            return true;
        }
    }
    
    @CodeGenerator(ErrorEmitter.class)
    public static class ErrorEntityProperties implements EntityProperties {
        
        private String name;

        private ErrorEntityProperties() {
        }

        public ErrorEntityProperties(String name) {
            this.name = name;
        }
        

        @Override
        public void validate() {
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 89 * hash + Objects.hashCode(this.name);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final ErrorEntityProperties other = (ErrorEntityProperties) obj;
            if (!Objects.equals(this.name, other.name)) {
                return false;
            }
            return true;
        }
    }
    
    
    public static class EmptyEmitter implements CodeEmitter<EmptyEntityProperties> {

        @Override
        public String emitCode(CodeGenerationContext context, EmptyEntityProperties value) throws CodeGenerationException {
            return "";
        }
        
    }
    
    public static class NonEmptyEmitter implements CodeEmitter<NonEmptyEntityProperties> {

        @Override
        public String emitCode(CodeGenerationContext context, NonEmptyEntityProperties value) throws CodeGenerationException {
            return "new Code()";
        }
        
    }
    
    public static class ErrorEmitter implements CodeEmitter<ErrorEntityProperties> {

        @Override
        public String emitCode(CodeGenerationContext context, ErrorEntityProperties value) throws CodeGenerationException {
            throw new CodeGenerationException("error");
        }
        
    }
    
    
}
