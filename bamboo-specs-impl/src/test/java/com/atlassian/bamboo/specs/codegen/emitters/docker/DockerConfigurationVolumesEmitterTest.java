package com.atlassian.bamboo.specs.codegen.emitters.docker;

import com.atlassian.bamboo.specs.TestResourceHelper;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.model.docker.DockerConfigurationProperties;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(JUnitParamsRunner.class)
public class DockerConfigurationVolumesEmitterTest {
    @Test
    @Parameters(method = "testEmitCodeParams")
    public void testEmitCode(Map<String, String> volumes, String expectedResultResource) throws Exception {
        final String emittedCode = new DockerConfigurationVolumesEmitter().emitCode(new CodeGenerationContext(), volumes);
        final String expectedCode = TestResourceHelper.getResourceAsString(getClass(), expectedResultResource);
        assertThat(emittedCode, is(expectedCode));
    }

    private Object[] testEmitCodeParams() {
        final Map<String, String> emptyVolumes = Collections.emptyMap();
        final Map<String, String> defaultVolumes = DockerConfigurationProperties.DEFAULT_VOLUMES;
        final Map<String, String> customVolumes = new LinkedHashMap<>();
        customVolumes.put("/var", "/opt/var");
        customVolumes.put("${bamboo.variable}", "${bamboo.other.variable}");
        customVolumes.put("/tmp", "/opt/tmp");
        final Map<String, String> mixedVolumes = new LinkedHashMap<>();
        mixedVolumes.putAll(defaultVolumes);
        mixedVolumes.putAll(customVolumes);

        return new Object[][]{
                {emptyVolumes, "emptyVolumes.java"},
                {defaultVolumes, "defaultVolumes.java"},
                {customVolumes, "customVolumes.java"},
                {mixedVolumes, "mixedVolumes.java"}
        };
    }
}
