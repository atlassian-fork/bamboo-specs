package com.atlassian.bamboo.specs.codegen.emitters.permission;

import com.atlassian.bamboo.specs.api.builders.permission.PermissionType;
import com.atlassian.bamboo.specs.api.builders.permission.Permissions;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.equalToIgnoringWhiteSpace;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class PermissionPropertiesEmitterTest {

    private CodeGenerationContext codeGenerationContext;

    @Before
    public void setUp() {
        codeGenerationContext = new CodeGenerationContext();
    }

    @Test
    public void emitCodeForEmptyPermissionProperties() throws Exception {
        final Permissions permission = new Permissions();

        final String code = new PermissionPropertiesEmitter().emitCode(codeGenerationContext, EntityPropertiesBuilders.build(permission));

        assertThat(code, equalTo("new Permissions()"));
    }

    @Test
    public void emitCodeForOnlyUserPermissionProperties() throws Exception {
        final Permissions permission = new Permissions().userPermissions("user1", PermissionType.BUILD, PermissionType.ADMIN);

        final String code = new PermissionPropertiesEmitter().emitCode(codeGenerationContext, EntityPropertiesBuilders.build(permission));

        assertThat(code, equalToIgnoringWhiteSpace("new Permissions()\n" +
                ".userPermissions(\"user1\", PermissionType.BUILD, PermissionType.ADMIN)"));
    }

    @Test
    public void emitCodeForOnlyGroupPermissionProperties() throws Exception {
        final Permissions permission = new Permissions().groupPermissions("group1", PermissionType.VIEW, PermissionType.ADMIN);

        final String code = new PermissionPropertiesEmitter().emitCode(codeGenerationContext, EntityPropertiesBuilders.build(permission));

        assertThat(code, equalToIgnoringWhiteSpace("new Permissions()\n" +
                ".groupPermissions(\"group1\", PermissionType.VIEW, PermissionType.ADMIN)"));
    }

    @Test
    public void emitCodeForOnlyLoggedInUserPermissionProperties() throws Exception {
        final Permissions permission = new Permissions().loggedInUserPermissions(PermissionType.VIEW, PermissionType.DELETE);

        final String code = new PermissionPropertiesEmitter().emitCode(codeGenerationContext, EntityPropertiesBuilders.build(permission));

        assertThat(code, equalToIgnoringWhiteSpace("new Permissions()\n" +
                ".loggedInUserPermissions(PermissionType.VIEW, PermissionType.DELETE)"));
    }

    @Test
    public void emitCodeForOnlyAnonymousUserPermissionProperties() throws Exception {
        final Permissions permission = new Permissions().anonymousUserPermissionView();

        final String code = new PermissionPropertiesEmitter().emitCode(codeGenerationContext, EntityPropertiesBuilders.build(permission));

        assertThat(code, equalToIgnoringWhiteSpace("new Permissions()\n" +
                ".anonymousUserPermissionView()"));
    }

}