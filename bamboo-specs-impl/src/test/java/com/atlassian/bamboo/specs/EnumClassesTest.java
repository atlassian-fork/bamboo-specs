package com.atlassian.bamboo.specs;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;

import java.util.Arrays;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;

/**
 * Tests which assert that all enum classes used for importing/exporting fulfill their contracts.
 */
public class EnumClassesTest {

    /**
     * Ensures that enum constants used throughout Bamboo Specs are upper-cased. (Regression test for BDEV-13158.)
     */
    @Test
    public void enumConstantNamesAreUpperCased() throws Exception {
        getAllEnumClasses().forEach(enumClass ->
                Arrays.stream(enumClass.getEnumConstants()).forEach(enumConstant ->
                        assertThat(
                                String.format("Enum constant %s#%s should be written in capital letters", enumClass.getName(), enumConstant.name()),
                                enumConstant.name(), is(validEnumName()))));
    }

    private Matcher<String> validEnumName() {
        return new BaseMatcher<String>() {
            @Override
            public boolean matches(Object item) {
                if (!(item instanceof String)) {
                    return false;
                }
                final String enumName = (String) item;
                return enumName.matches("[A-Z0-9_]+");
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("a valid enum constant name");
            }
        };
    }

    private Set<Class<? extends Enum>> getAllEnumClasses() {
        final Reflections reflections = new Reflections(
                ClasspathHelper.forPackage("com.atlassian.bamboo.specs"),
                new SubTypesScanner(false));
        final Set<Class<? extends Enum>> result = reflections.getSubTypesOf(Enum.class);
        assertThat(result, is(not(empty())));
        return result;
    }
}
