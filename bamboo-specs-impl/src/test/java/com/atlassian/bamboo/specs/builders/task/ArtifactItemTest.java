package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

public class ArtifactItemTest {

    private final PlanIdentifier plan = new PlanIdentifier("PROJ", "PLAN");

    @Test
    public void testDefaults() {
        final ArtifactItem item = new ArtifactItem();

        assertThat(item.build().getArtifactName(), nullValue());
        assertThat(item.build().isAllArtifacts(), is(true));
        assertThat(item.build().getSourcePlan(), nullValue());
    }

    @Test
    public void testLastAllArtifactsWins() {
        final ArtifactItem item = new ArtifactItem().sourcePlan(plan).artifact("one").allArtifacts().artifact("two").allArtifacts();
        assertThat(item.build().getArtifactName(), nullValue());
    }

    @Test
    public void testLastArtifactWins() {
        final ArtifactItem item2 = new ArtifactItem().sourcePlan(plan).artifact("one").allArtifacts().artifact("two");
        assertThat(item2.build().getArtifactName(), equalTo("two"));
    }
}