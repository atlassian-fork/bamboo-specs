package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.requirement.Requirement;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.model.task.NUnitRunnerTaskProperties;
import org.apache.commons.lang3.RandomStringUtils;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Collections;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertThat;


public class NUnitRunnerTaskTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testValidTaskDefinition() {
        final Requirement requirement = Requirement.equals(RandomStringUtils.randomAlphabetic(5), RandomStringUtils.randomAlphabetic(5));
        assertThat(new NUnitRunnerTask().executable("exec")
                        .nUnitVersion2()
                        .nUnitTestFiles("files")
                        .resultFilename("result.xml")
                        .testsToRun("ccc")
                        .testCategoriesToInclude("aaa")
                        .testCategoriesToExclude("bbb")
                        .commandLineOptions("options")
                        .environmentVariables("variables")
                        .description("description")
                        .enabled(false)
                        .requirements(requirement)
                        .build(),
                Matchers.equalTo(new NUnitRunnerTaskProperties("description", false, "exec", NUnitRunnerTask.NUnitVersion.NUNIT_2, "files", "result.xml",
                        singletonList("ccc"), singletonList("aaa"), singletonList("bbb"), "options", "variables",
                        Collections.singletonList(EntityPropertiesBuilders.build(requirement)), Collections.emptyList())));
    }

    @Test
    public void testMinimalTaskDefinition() {
        final Requirement requirement = Requirement.equals(RandomStringUtils.randomAlphabetic(5), RandomStringUtils.randomAlphabetic(5));
        assertThat(new NUnitRunnerTask().executable("exec")
                        .nUnitVersion2()
                        .nUnitTestFiles("files")
                        .resultFilename("result.xml")
                        .description("description")
                        .requirements(requirement)
                        .build(),
                Matchers.equalTo(new NUnitRunnerTaskProperties("description", true, "exec",
                        NUnitRunnerTask.NUnitVersion.NUNIT_2, "files", "result.xml",
                        emptyList(), emptyList(), emptyList(), null, null,
                        Collections.singletonList(EntityPropertiesBuilders.build(requirement)), Collections.emptyList())));
    }

    @Test
    public void testValidation() {
        exception.expect(PropertiesValidationException.class);
        exception.expectMessage("executable");
        new NUnitRunnerTask()
                .nUnitTestFiles("files")
                .resultFilename("result.xml")
                .testsToRun("ccc")
                .testCategoriesToInclude("aaa")
                .testCategoriesToExclude("bbb")
                .commandLineOptions("options")
                .environmentVariables("variables")
                .description("description")
                .build();
    }

}