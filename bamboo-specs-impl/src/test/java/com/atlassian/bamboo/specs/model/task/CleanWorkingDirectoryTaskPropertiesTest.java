package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.builders.task.CleanWorkingDirectoryTask;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class CleanWorkingDirectoryTaskPropertiesTest
{
    @Test
    public void testTaskEnabledFlagIsPropagated()
    {
        final CleanWorkingDirectoryTask task = new CleanWorkingDirectoryTask().enabled(false).description("description");
        final CleanWorkingDirectoryTaskProperties taskProperties = EntityPropertiesBuilders.build(task);

        assertThat(taskProperties.isEnabled(), is(false));
        assertThat(taskProperties.getDescription(), is("description"));
    }
}
