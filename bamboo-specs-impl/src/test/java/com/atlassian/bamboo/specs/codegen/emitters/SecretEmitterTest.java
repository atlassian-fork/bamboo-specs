package com.atlassian.bamboo.specs.codegen.emitters;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.codegen.annotations.Builder;
import com.atlassian.bamboo.specs.api.codegen.annotations.Secret;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.codegen.BambooSpecsGenerator;
import org.junit.Test;

import java.util.Objects;

import static org.junit.Assert.assertFalse;

public class SecretEmitterTest {

    private static final String SECRET_PASSWORD = "MY supper secret password";

    @Test
    public void testSecretEmitter() throws Exception {
        BambooSpecsGenerator generator = new BambooSpecsGenerator(new SecretBuilder()
                .password(SECRET_PASSWORD)
                .build());

        String result = generator.emitCode();

        assertFalse("Emitter shouldn't leak secret fields", result.contains(SECRET_PASSWORD));
    }

    @Builder(SecretBuilder.class)
    public static class SecretProperties implements EntityProperties {

        @Secret
        private final String password;

        private SecretProperties() {
            password = null;
        }

        public SecretProperties(String password) {
            this.password = password;

            validate();
        }

        @Override
        public void validate() {

        }

        public String getPassword() {
            return password;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            SecretProperties that = (SecretProperties) o;
            return Objects.equals(password, that.password);
        }

        @Override
        public int hashCode() {
            return Objects.hash(password);
        }
    }

    public static class SecretBuilder extends EntityPropertiesBuilder<SecretProperties> {

        private String password;

        public SecretBuilder password(String password) {
            this.password = password;
            return this;
        }

        @Override
        protected SecretProperties build() {
            return new SecretProperties(password);
        }
    }
}
