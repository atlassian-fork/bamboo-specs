package com.atlassian.bamboo.specs.codegen.emitters.task;

import com.atlassian.bamboo.specs.TestResourceHelper;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;
import com.atlassian.bamboo.specs.builders.task.BuildWarningParserTask;
import com.atlassian.bamboo.specs.model.task.BuildWarningParserTaskProperties;
import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.assertEquals;

public class BuildWarningParserTaskEmitterTest {
    @Test
    public void emitCodeWithDefaultRepository() throws Exception {
        final BuildWarningParserTaskProperties taskProperties = new BuildWarningParserTaskProperties(
                "",
                true,
                Collections.emptyList(),
                Collections.emptyList(),
                "Clang (LLVM)",
                null,
                true,
                true,
                null,
                true,
                1,
                BuildWarningParserTask.WarningSeverity.LOW
        );
        final String expectedCode = TestResourceHelper.getResourceAsString(getClass(), "taskWithDefaultRepository.java");
        final String emittedCode = new BuildWarningParserTaskEmitter().emitCode(new CodeGenerationContext(), taskProperties);
        assertEquals(expectedCode, emittedCode);
    }

    @Test
    public void emitCodeWithNoRepositoryAndNoFailBuild() throws Exception {
        final BuildWarningParserTaskProperties taskProperties = new BuildWarningParserTaskProperties(
                "",
                true,
                Collections.emptyList(),
                Collections.emptyList(),
                "Clang (LLVM)",
                null,
                false,
                false,
                null,
                false,
                0,
                BuildWarningParserTask.WarningSeverity.LOW
        );
        final String expectedCode = TestResourceHelper.getResourceAsString(getClass(), "taskWithNoRepository.java");
        final String emittedCode = new BuildWarningParserTaskEmitter().emitCode(new CodeGenerationContext(), taskProperties);
        assertEquals(expectedCode, emittedCode);
    }

    @Test
    public void emitCodeWithCustomRepository() throws Exception {
        final BuildWarningParserTaskProperties taskProperties = new BuildWarningParserTaskProperties(
                "",
                true,
                Collections.emptyList(),
                Collections.emptyList(),
                "Clang (LLVM)",
                null,
                true,
                false,
                new VcsRepositoryIdentifierProperties("bamboo-node-plugin", null),
                true,
                1,
                BuildWarningParserTask.WarningSeverity.LOW
        );
        final String expectedCode = TestResourceHelper.getResourceAsString(getClass(), "taskWithCustomRepository.java");
        final String emittedCode = new BuildWarningParserTaskEmitter().emitCode(new CodeGenerationContext(), taskProperties);
        assertEquals(expectedCode, emittedCode);
    }
}
