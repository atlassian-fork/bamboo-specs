package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.model.task.DumpVariablesTaskProperties;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class DumpVariablesTaskTest {
    @Test
    public void testBuilderOK() {
        final DumpVariablesTask dumpVariablesBuilder = new DumpVariablesTask();

        final DumpVariablesTaskProperties properties = dumpVariablesBuilder.build();

        assertThat(properties.getAtlassianPlugin().getCompleteModuleKey(), is("com.atlassian.bamboo.plugins.bamboo-variable-inject-plugin:dump"));
    }
}