package com.atlassian.bamboo.specs.model.repository.bitbucket.server;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.builders.repository.bitbucket.server.BitbucketServerMirror;
import org.hamcrest.CoreMatchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertThat;

public class BitbucketServerMirrorPropertiesTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldAcceptOneProperty() {
        //given
        //when
        final BitbucketServerMirrorProperties bitbucketServerMirrorProperties = EntityPropertiesBuilders.build(new BitbucketServerMirror().name("name"));

        //then
        assertThat(bitbucketServerMirrorProperties.getName(), CoreMatchers.equalTo("name"));
    }

    @Test
    public void shouldAcceptTwoProperties() {
        //given
        //when
        final BitbucketServerMirrorProperties bitbucketServerMirrorProperties = EntityPropertiesBuilders.build(new BitbucketServerMirror().name("name").url("url"));

        //then
        assertThat(bitbucketServerMirrorProperties.getName(), CoreMatchers.equalTo("name"));
        assertThat(bitbucketServerMirrorProperties.getUrl(), CoreMatchers.equalTo("url"));
    }

    @Test
    public void shouldAcceptRejectWithoutProperties() {
        expectedException.expect(PropertiesValidationException.class);
        final BitbucketServerMirrorProperties bitbucketServerMirrorProperties = EntityPropertiesBuilders.build(new BitbucketServerMirror());

    }

}