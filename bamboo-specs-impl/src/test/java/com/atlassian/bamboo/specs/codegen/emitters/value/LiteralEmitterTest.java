package com.atlassian.bamboo.specs.codegen.emitters.value;

import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;


public class LiteralEmitterTest {
    LiteralEmitter emitter = new LiteralEmitter();

    @Test
    public void testNull() throws Exception {
        Assert.assertThat(emitter.emitCode(new CodeGenerationContext(), null), Matchers.equalTo("null"));
    }

    @Test
    public void testBoolean() throws Exception {
        Assert.assertThat(emitter.emitCode(new CodeGenerationContext(), true), Matchers.equalTo("true"));
    }

    @Test
    public void testBooleanPrimitive() throws Exception {
        Assert.assertThat(emitter.emitCode(new CodeGenerationContext(), Boolean.TRUE), Matchers.equalTo("true"));
    }


}