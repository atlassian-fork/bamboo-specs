package com.atlassian.bamboo.specs.codegen.emitters.notification;

import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.builders.notification.AnyNotificationType;
import com.atlassian.bamboo.specs.api.builders.notification.NotificationType;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.model.notification.AnyNotificationTypeProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.builders.notification.CommentAddedNotification;
import com.atlassian.bamboo.specs.builders.notification.DeploymentFailedNotification;
import com.atlassian.bamboo.specs.builders.notification.DeploymentFinishedNotification;
import com.atlassian.bamboo.specs.builders.notification.FirstFailedJobNotification;
import com.atlassian.bamboo.specs.builders.notification.JobFailedNotification;
import com.atlassian.bamboo.specs.builders.notification.JobHungNotification;
import com.atlassian.bamboo.specs.builders.notification.JobStatusChangedNotification;
import com.atlassian.bamboo.specs.builders.notification.JobTimeoutNotification;
import com.atlassian.bamboo.specs.builders.notification.JobWithoutAgentNotification;
import com.atlassian.bamboo.specs.builders.notification.PlanFailedNotification;
import com.atlassian.bamboo.specs.builders.notification.PlanStatusChangedNotification;
import com.atlassian.bamboo.specs.builders.notification.ResponsibilityChangedNotification;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class AnyNotificationTypeEmitterTest {

    @Parameters(method = "emitCodeParameters")
    @Test
    public void testEmitCode(AnyNotificationTypeProperties properties, String expectedCode) throws Exception {
        String result = new AnyNotificationTypeEmitter().emitCode(new CodeGenerationContext(), properties);

        assertEquals(expectedCode, result);
    }


    private Object[][] emitCodeParameters() {
        return new Object[][]{
                {buildProperties(new DeploymentFailedNotification()), "new DeploymentFailedNotification()"},
                {buildProperties(new DeploymentFinishedNotification()), "new DeploymentFinishedNotification()"},
                {buildProperties(new CommentAddedNotification()), "new CommentAddedNotification()"},
                {buildProperties(new JobFailedNotification()), "new JobFailedNotification()"},
                {buildProperties(new FirstFailedJobNotification()), "new FirstFailedJobNotification()"},
                {buildProperties(new JobHungNotification()), "new JobHungNotification()"},
                {buildProperties(new JobTimeoutNotification()), "new JobTimeoutNotification()"},
                {buildProperties(new JobStatusChangedNotification()), "new JobStatusChangedNotification()"},
                {buildProperties(new JobWithoutAgentNotification()), "new JobWithoutAgentNotification()"},
                {buildProperties(new PlanFailedNotification()), "new PlanFailedNotification()"},
                {buildProperties(new PlanStatusChangedNotification()), "new PlanStatusChangedNotification()"},
                {buildProperties(new ResponsibilityChangedNotification()), "new ResponsibilityChangedNotification()"},
                {buildProperties(new AnyNotificationType(new AtlassianModule("com.atlassian.bamboo.plugin.system.notifications:chainCompleted.failedChains"))),
                        "new PlanFailedNotification()"},
                {buildProperties(new AnyNotificationType(new AtlassianModule("o:ther"))),
                        "new AnyNotificationType(new AtlassianModule(\"o:ther\"))"},
        };
    }

    @NotNull
    private <T extends AnyNotificationTypeProperties> AnyNotificationTypeProperties buildProperties(NotificationType<?, T> builder) {
        return EntityPropertiesBuilders.build(builder);
    }

}