package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.requirement.Requirement;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.model.task.SshTaskProperties;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.Collections;

import static com.atlassian.bamboo.specs.util.TestFileUtils.getPathTo;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

public class SshTaskTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Rule
    public TemporaryFolder sourceDirectory = new TemporaryFolder();

    @Test
    public void testEquals() throws Exception {
        final Requirement requirement = Requirement.equals(RandomStringUtils.randomAlphabetic(5), RandomStringUtils.randomAlphabetic(5));

        assertThat(new SshTask()
                        .description("My SSH task")
                        .enabled(true)
                        .host("1.1.1.1")
                        .username("myusername")
                        .authenticateWithKeyWithPassphrase("mykey", "mypassphrase")
                        .command("echo Hello!")
                        .hostFingerprint("hostFingerprint")
                        .port(23)
                        .keepAliveInterval(Duration.ofSeconds(30L))
                        .requirements(requirement).build(),
                equalTo(new SshTaskProperties("My SSH task",
                        true,
                        "1.1.1.1",
                        "myusername",
                        SshTaskProperties.AuthenticationType.KEY_WITH_PASSPHRASE,
                        null,
                        "mykey",
                        "mypassphrase",
                        null,
                        "hostFingerprint",
                        23,
                        30,
                        "echo Hello!",
                        Collections.singletonList(EntityPropertiesBuilders.build(requirement)),
                        Collections.emptyList())));
    }

    @Test
    public void testNotEqualsOnCommand() throws Exception {
        final Requirement requirement = Requirement.equals(RandomStringUtils.randomAlphabetic(5), RandomStringUtils.randomAlphabetic(5));

        assertThat(new SshTask()
                        .description("My SSH task")
                        .enabled(true)
                        .host("1.1.1.1")
                        .username("myusername")
                        .authenticateWithKeyWithPassphrase("mykey", "mypassphrase")
                        .command("echo Hello!")
                        .hostFingerprint("hostFingerprint")
                        .port(23)
                        .keepAliveInterval(Duration.ofSeconds(30L))
                        .requirements(requirement).build(),
                not(equalTo(new SshTaskProperties("My SSH task",
                        true,
                        "1.1.1.1",
                        "myusername",
                        SshTaskProperties.AuthenticationType.KEY_WITH_PASSPHRASE,
                        null,
                        "mykey",
                        "mypassphrase",
                        null,
                        "hostFingerprint",
                        23,
                        30,
                        "echo By By!",
                        Collections.singletonList(EntityPropertiesBuilders.build(requirement)),
                        Collections.emptyList()))));
    }

    @Test
    public void testAuthenticationWithPassword() throws Exception {
        final SshTask sshTask = new SshTask()
                .description("My SSH task")
                .enabled(true)
                .host("1.1.1.1")
                .username("myusername")
                .authenticateWithPassword("mypassword")
                .command("echo Hello!");
        assertThat(sshTask.build().getAuthenticationType(), equalTo(SshTaskProperties.AuthenticationType.PASSWORD));
        assertThat(sshTask.build().getPassword(), equalTo("mypassword"));
    }

    @Test
    public void testAuthenticationWithKeyWithPassphrase() throws Exception {
        final SshTask sshTask = new SshTask()
                .description("My SSH task")
                .enabled(true)
                .host("1.1.1.1")
                .username("myusername")
                .authenticateWithKeyWithPassphrase("mykey", "mypassphrase")
                .command("echo Hello!");
        assertThat(sshTask.build().getAuthenticationType(), equalTo(SshTaskProperties.AuthenticationType.KEY_WITH_PASSPHRASE));
        assertThat(sshTask.build().getKey(), equalTo("mykey"));
        assertThat(sshTask.build().getPassphrase(), equalTo("mypassphrase"));
    }

    @Test
    public void testAuthenticationWithKeyNoPassphrase() throws Exception {
        final SshTask sshTask = new SshTask()
                .description("My SSH task")
                .enabled(true)
                .host("1.1.1.1")
                .username("myusername")
                .authenticateWithKey("mykey")
                .command("echo Hello!");
        assertThat(sshTask.build().getAuthenticationType(), equalTo(SshTaskProperties.AuthenticationType.KEY_WITHOUT_PASSPHRASE));
        assertThat(sshTask.build().getKey(), equalTo("mykey"));
        assertThat(sshTask.build().getPassphrase(), is(nullValue()));
    }


    @Test
    public void testAuthenticationWithKeyFromFile() throws Exception {
        final File fileWithKey = sourceDirectory.newFile("testFileWithKey.key");
        Files.write(fileWithKey.toPath(), "KKK-EEE-YYY".getBytes());

        final SshTask sshTask = new SshTask()
                .description("My SSH task")
                .enabled(true)
                .host("1.1.1.1")
                .username("myusername")
                .authenticateWithKey(fileWithKey.toPath())
                .command("echo Hello!");
        assertThat(sshTask.build().getAuthenticationType(), equalTo(SshTaskProperties.AuthenticationType.KEY_WITHOUT_PASSPHRASE));
        assertThat(sshTask.build().getKey(), equalTo("KKK-EEE-YYY"));
        assertThat(sshTask.build().getPassphrase(), is(nullValue()));
    }

    @Test
    public void testAuthenticationWithKeyFromFileWhenFileDoesntExist() throws Exception {
        final Path pathToNonExistingKey = new File("/path/to/nonexisting/key").toPath();

        assertThat(Files.exists(pathToNonExistingKey), equalTo(false));

        exception.expect(PropertiesValidationException.class);
        exception.expectMessage(String.format("File %s does not exist", pathToNonExistingKey));

        new SshTask()
                .description("My SSH task")
                .enabled(true)
                .host("1.1.1.1")
                .username("myusername")
                .authenticateWithKey(pathToNonExistingKey)
                .command("echo Hello!");
    }

    @Test
    public void testValidationNoCommand() throws Exception {
        exception.expect(PropertiesValidationException.class);
        exception.expectMessage("SSH task: Property command is required.");

        new SshTask()
                .description("My SSH task")
                .enabled(true)
                .host("1.1.1.1")
                .username("myusername")
                .authenticateWithPassword("password")
                .build()
                .validate();
    }

    @Test
    public void testInlineBodyFromPathFileDoesntExist() throws Exception {
        final Path pathToNonExistingScript = getPathTo(sourceDirectory.getRoot(), "myscript.sh");
        assertThat(Files.exists(pathToNonExistingScript), equalTo(false));

        exception.expect(PropertiesValidationException.class);
        exception.expectMessage(String.format("File %s does not exist", pathToNonExistingScript));

        new SshTask()
                .command(getPathTo(sourceDirectory.getRoot(), "myscript.sh"));
    }
}