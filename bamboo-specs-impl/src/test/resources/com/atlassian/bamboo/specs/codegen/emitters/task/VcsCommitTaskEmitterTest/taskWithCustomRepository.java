new VcsCommitTask()
    .repository(new VcsRepositoryIdentifier()
            .name("bamboo-plugin"))
    .workingSubdirectory("sub/directory")
    .commitMessage("hello, world")