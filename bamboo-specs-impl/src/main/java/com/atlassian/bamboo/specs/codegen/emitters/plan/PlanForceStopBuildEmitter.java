package com.atlassian.bamboo.specs.codegen.emitters.plan;

import com.atlassian.bamboo.specs.api.codegen.CodeEmitter;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import org.jetbrains.annotations.NotNull;

public class PlanForceStopBuildEmitter implements CodeEmitter<Boolean> {
    @NotNull
    @Override
    public String emitCode(@NotNull CodeGenerationContext context, @NotNull Boolean value) throws CodeGenerationException {
        if (Boolean.TRUE.equals(value)) {
            return ".forceStopHungBuilds()";
        } else {
            return ".ignoreHungBuilds()";
        }
    }
}
