package com.atlassian.bamboo.specs.codegen.emitters.task;

import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.builders.task.VcsPushTask;
import com.atlassian.bamboo.specs.model.task.VcsPushTaskProperties;
import org.jetbrains.annotations.NotNull;

public class VcsPushTaskEmitter extends BaseVcsTaskEmitter<VcsPushTaskProperties> {
    @NotNull
    @Override
    public String emitCode(@NotNull CodeGenerationContext context, @NotNull VcsPushTaskProperties entity) throws CodeGenerationException {
        builderClass = VcsPushTask.class;
        return super.emitCode(context, entity);
    }
}
