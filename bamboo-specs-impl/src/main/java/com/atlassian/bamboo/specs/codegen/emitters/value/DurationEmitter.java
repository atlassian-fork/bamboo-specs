package com.atlassian.bamboo.specs.codegen.emitters.value;

import com.atlassian.bamboo.specs.api.codegen.CodeEmitter;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import org.jetbrains.annotations.NotNull;

import java.time.Duration;

/**
 * Generates value of {@link Duration}.
 */
public class DurationEmitter implements CodeEmitter<Duration> {
    @NotNull
    @Override
    public String emitCode(@NotNull final CodeGenerationContext context, @NotNull final Duration value) throws CodeGenerationException {
        return context.importClassName(Duration.class) + ".ofSeconds(" + value.getSeconds() + ")";
    }
}
