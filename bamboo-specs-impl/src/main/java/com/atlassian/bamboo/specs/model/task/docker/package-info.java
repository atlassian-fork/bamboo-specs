/**
 * Properties classes related to Docker CLI task.
 */
package com.atlassian.bamboo.specs.model.task.docker;
