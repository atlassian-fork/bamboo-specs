package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.model.task.MavenTaskProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Maven 2/3 build task builder.
 */
public class MavenTask extends Task<MavenTask, MavenTaskProperties> {
    public enum TestDirectoryOption {
        STANDARD, CUSTOM
    }

    public static final int MAVEN_V2 = 2;
    public static final int MAVEN_V3 = 3;

    @NotNull
    protected String goal;
    @Nullable
    protected String projectFile;
    @Nullable
    protected String environmentVariables;
    @Nullable
    protected String jdk;
    @Nullable
    protected String label;
    protected boolean hasTests;
    @Nullable
    protected TestDirectoryOption testDirectoryOption;
    @Nullable
    protected String testResultsDirectory;
    @Nullable
    protected String workingSubdirectory;
    protected boolean useMavenReturnCode;
    protected int version = MAVEN_V3;

    public MavenTask goal(@NotNull String goal) {
        this.goal = goal;
        return this;
    }

    public MavenTask projectFile(String projectFile) {
        this.projectFile = projectFile;
        return this;
    }

    public MavenTask environmentVariables(String environmentVariables) {
        this.environmentVariables = environmentVariables;
        return this;
    }

    public MavenTask jdk(String jdk) {
        this.jdk = jdk;
        return this;
    }

    public MavenTask executableLabel(String label) {
        this.label = label;
        return this;
    }

    public MavenTask hasTests(boolean hasTests) {
        this.hasTests = hasTests;
        return this;
    }

    public MavenTask standardResultsPath() {
        this.testDirectoryOption = TestDirectoryOption.STANDARD;
        return this;
    }

    public MavenTask testResultsPath(String testResultsDirectory) {
        this.testDirectoryOption = TestDirectoryOption.CUSTOM;
        this.testResultsDirectory = testResultsDirectory;
        return this;
    }

    public MavenTask workingSubdirectory(String workingSubdirectory) {
        this.workingSubdirectory = workingSubdirectory;
        return this;
    }

    public MavenTask useMavenReturnCode(boolean useMavenReturnCode) {
        this.useMavenReturnCode = useMavenReturnCode;
        return this;
    }

    public MavenTask version2() {
        this.version = MAVEN_V2;
        return this;
    }

    public MavenTask version3() {
        this.version = MAVEN_V3;
        return this;
    }

    @NotNull
    @Override
    protected MavenTaskProperties build() {
        return new MavenTaskProperties(description,
                taskEnabled,
                goal,
                projectFile,
                environmentVariables,
                jdk,
                label,
                hasTests,
                testDirectoryOption,
                testResultsDirectory,
                workingSubdirectory,
                useMavenReturnCode,
                version,
                requirements,
                conditions);
    }

}
