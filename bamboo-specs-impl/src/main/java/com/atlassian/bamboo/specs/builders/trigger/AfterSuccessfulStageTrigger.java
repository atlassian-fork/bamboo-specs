package com.atlassian.bamboo.specs.builders.trigger;

import com.atlassian.bamboo.specs.api.builders.trigger.Trigger;
import com.atlassian.bamboo.specs.model.trigger.AfterSuccessfulStageTriggerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Trigger which schedule a deployment when plan stage build complete.
 */
public class AfterSuccessfulStageTrigger extends Trigger<AfterSuccessfulStageTrigger, AfterSuccessfulStageTriggerProperties> {
    private String stageName;
    private String releaseBranch;

    /**
     * Schedule a deployment when plan stage build complete.
     * @param stageName name of stage
     */
    public AfterSuccessfulStageTrigger(@NotNull final String stageName) {
        this.stageName = stageName;
    }

    /**
     * Configure trigger to start deployment when plan master branch is updated.
     */
    public AfterSuccessfulStageTrigger triggerByMasterBranch() {
        releaseBranch = null;
        return this;
    }

    /**
     * Configure trigger to start deployment when plan branch is updated.
     *
     * @param planBranchName name of plan branch. If value is null it's the same as {@link #triggerByMasterBranch()}
     */
    public AfterSuccessfulStageTrigger triggerByBranch(@Nullable String planBranchName) {
        this.releaseBranch = planBranchName;
        return this;
    }

    @Override
    protected AfterSuccessfulStageTriggerProperties build() {
        return new AfterSuccessfulStageTriggerProperties(description, triggerEnabled, stageName, releaseBranch);
    }
}
