package com.atlassian.bamboo.specs.util;

public interface UserPasswordCredentials {
    String getUsername();

    String getPassword();
}
