package com.atlassian.bamboo.specs.codegen.emitters.task;

import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.builders.task.DockerRunContainerTask;
import com.atlassian.bamboo.specs.codegen.emitters.value.EntityPropertiesEmitter;
import com.atlassian.bamboo.specs.model.task.docker.DockerRunContainerTaskProperties;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.stream.Collectors;

public class DockerRunContainerEmitter extends EntityPropertiesEmitter<DockerRunContainerTaskProperties> {

    @NotNull
    @Override
    public String emitCode(@NotNull final CodeGenerationContext context, @NotNull final DockerRunContainerTaskProperties entity) throws CodeGenerationException {
        builderClass = DockerRunContainerTask.class;

        fieldsToSkip.add("portMappings");
        fieldsToSkip.add("volumeMappings");

        String builderCode = super.emitCode(context, entity);
        try {
            context.incIndentation();

            return String.format("%s%s%s%s.clearVolumeMappings()%s.%s",
                    builderCode,
                    context.newLine(),
                    emitPortMappings(context, entity),
                    context.newLine(),
                    context.newLine(),
                    emitVolumeMappings(context, entity));
        } finally {
            context.decIndentation();
        }
    }

    private String emitPortMappings(CodeGenerationContext context, DockerRunContainerTaskProperties entity) {
        final String portMappingCode = entity.getPortMappings()
                .entrySet()
                .stream()
                .map(mapping -> String.format("appendPortMapping(%d, %d)", mapping.getKey(), mapping.getValue()))
                .collect(Collectors.joining(context.newLine() + "."));
        return StringUtils.isBlank(portMappingCode) ? "" : String.format(".%s", portMappingCode);
    }

    private String emitVolumeMappings(CodeGenerationContext context, DockerRunContainerTaskProperties entity) {
        return entity.getVolumeMappings()
                .entrySet()
                .stream()
                .map(mapping -> String.format("appendVolumeMapping(\"%s\", \"%s\")", mapping.getKey(), mapping.getValue()))
                .collect(Collectors.joining(context.newLine() + "."));
    }
}
