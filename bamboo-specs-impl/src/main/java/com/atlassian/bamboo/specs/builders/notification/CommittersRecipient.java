package com.atlassian.bamboo.specs.builders.notification;

import com.atlassian.bamboo.specs.api.builders.Applicability;
import com.atlassian.bamboo.specs.api.builders.notification.NotificationRecipient;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.notification.AnyNotificationRecipientProperties;
import org.jetbrains.annotations.NotNull;

import java.util.EnumSet;

/**
 * Represents a users who have committed to the build.
 */
public class CommittersRecipient extends NotificationRecipient<CommittersRecipient, AnyNotificationRecipientProperties> {

    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugin.system.notifications:recipient.committer");

    @NotNull
    @Override
    protected AnyNotificationRecipientProperties build() {
        return new AnyNotificationRecipientProperties(ATLASSIAN_PLUGIN, "", EnumSet.of(Applicability.PLANS));
    }

}
