package com.atlassian.bamboo.specs.util;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.AbstractConstruct;
import org.yaml.snakeyaml.error.YAMLException;
import org.yaml.snakeyaml.nodes.Node;
import org.yaml.snakeyaml.nodes.ScalarNode;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

class IncludeTag extends AbstractConstruct {
    private static final String YAML_SUFFIX_1 = ".yaml";
    private static final String YAML_SUFFIX_2 = ".yml";
    private final int maxDepth;
    private final int depth;
    private final Path yamlParentDirectory;

    IncludeTag(int maxDepth, int depth, Path yamlParentDirectory) {
        this.maxDepth = maxDepth;
        this.depth = depth;
        this.yamlParentDirectory = yamlParentDirectory;
    }

    public Object construct(Node node) {
        ScalarNode sn = (ScalarNode) node;
        String includeFileName = sn.getValue();
        Path includeFilePath = getIncludeFilePath(includeFileName, yamlParentDirectory);
        InputStream inputStream = null;
        try {
            if (depth >= maxDepth) {
                throw new YAMLException(String.format("To many recursive include calls"));
            }
            Yaml yamlWithRepositoryIncludes = Yamlizator.getYamlWithRepositoryIncludes(maxDepth, depth + 1, yamlParentDirectory);
            inputStream = new FileInputStream(includeFilePath.toString());
            return yamlWithRepositoryIncludes.load(inputStream);
        } catch (FileNotFoundException e) {
            // should not happens due we check if file exists in #getIncludeFilePath
            throw new YAMLException(String.format("Include file %s not found", includeFileName), e);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException ioe) {
                    throw new YAMLException("", ioe);
                }
            }
        }
    }

    private Path getIncludeFilePath(final String filePath, final Path yamlParentDirectory) {
        Path includeFilePath = Paths.get(filePath);
        if (!includeFilePath.isAbsolute()) {
            includeFilePath = Paths.get(yamlParentDirectory.toAbsolutePath() + FileSystems.getDefault().getSeparator() + filePath);
        }
        if (!Files.exists(includeFilePath)) {
            throw new YAMLException(String.format("Include file %s does not exist", filePath));
        }
        if (!Files.isReadable(includeFilePath)) {
            throw new YAMLException(String.format("Include file %s is not readable", filePath));
        }
        if (Files.isSymbolicLink(includeFilePath)) {
            throw new YAMLException(String.format("Include file %s is a symbolic link", filePath));
        }
        if (!filePath.endsWith(YAML_SUFFIX_1) && !filePath.endsWith(YAML_SUFFIX_2)) {
            throw new YAMLException(String.format("Include file %s has not proper suffix %s or %s", filePath, YAML_SUFFIX_1, YAML_SUFFIX_2));
        }

        File yamlDirectoryFile = yamlParentDirectory.toFile();
        File includeFile = includeFilePath.toFile();
        try {
            if (!includeFile.getCanonicalPath().startsWith(yamlDirectoryFile.getCanonicalPath() + FileSystems.getDefault().getSeparator())) {
                throw new YAMLException(String.format("Include file %s is not in the source directory %s", filePath, yamlDirectoryFile.getCanonicalPath()));
            }
        } catch (IOException ioe) {
            throw new YAMLException(String.format("Include file %s", filePath), ioe);
        }
        return includeFilePath.toAbsolutePath();
    }
}
