package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkThat;

@Immutable
public final class NpmTaskProperties extends BaseNodeTaskProperties {

    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.bamboo-nodejs-plugin:task.builder.npm");
    public static final ValidationContext VALIDATION_CONTEXT = ValidationContext.of("npm task");

    @NotNull
    private String command;
    private boolean useIsolatedCache;

    protected NpmTaskProperties() {
        super();
    }

    public NpmTaskProperties(@Nullable String description,
                             boolean enabled,
                             @NotNull String nodeExecutable,
                             @Nullable String environmentVariables,
                             @Nullable String workingSubdirectory,
                             @NotNull String command,
                             boolean useIsolatedCache,
                             @NotNull List<RequirementProperties> requirements,
                             @NotNull List<? extends ConditionProperties> conditions) throws PropertiesValidationException {
        super(description, enabled, nodeExecutable, environmentVariables, workingSubdirectory, requirements, conditions);
        this.command = command;
        this.useIsolatedCache = useIsolatedCache;

        validate();
    }

    @Override
    public void validate() {
        super.validate();
        checkThat(getValidationContext(), StringUtils.isNotBlank(command), "Command is not defined");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NpmTaskProperties)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final NpmTaskProperties that = (NpmTaskProperties) o;
        return isUseIsolatedCache() == that.isUseIsolatedCache() &&
                Objects.equals(getCommand(), that.getCommand());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getCommand(), isUseIsolatedCache());
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    @Override
    protected ValidationContext getValidationContext() {
        return VALIDATION_CONTEXT;
    }

    @NotNull
    public String getCommand() {
        return command;
    }

    public boolean isUseIsolatedCache() {
        return useIsolatedCache;
    }
}
