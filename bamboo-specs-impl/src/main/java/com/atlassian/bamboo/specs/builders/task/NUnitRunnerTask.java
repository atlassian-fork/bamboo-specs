package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import com.atlassian.bamboo.specs.model.task.NUnitRunnerTaskProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Executes, parses and displays NUnit test results.
 */
public class NUnitRunnerTask extends Task<NUnitRunnerTask, NUnitRunnerTaskProperties> {

    public enum NUnitVersion {
        NUNIT_2, NUNIT_3;
    }

    private String executable;
    private NUnitVersion nUnitVersion;
    private String nUnitTestFiles;
    private String resultFilename;
    private List<String> testsToRun = new ArrayList<>();
    private List<String> testCategoriesToInclude = new ArrayList<>();
    private List<String> testCategoriesToExclude = new ArrayList<>();
    private String commandLineOptions;
    private String environmentVariables;

    /**
     * Sets label (<em>not a path</em>) of NUnit executable.
     * <p>
     * This field is mandatory.
     */
    public NUnitRunnerTask executable(@NotNull String executable) {
        ImporterUtils.checkNotNull("executable", executable);
        this.executable = executable;
        return this;
    }

    /**
     * Sets the version of NUnit corresponding for the provided label.
     * <p>
     * Either nUnitVersion2() or nUnitVersion3() must be called.
     */
    public NUnitRunnerTask nUnitVersion2() {
        return nUnitVersion(NUnitVersion.NUNIT_2);
    }

    /**
     * Sets the version of NUnit corresponding for the provided label.
     * <p>
     * Either nUnitVersion2() or nUnitVersion3() must be called.
     */
    public NUnitRunnerTask nUnitVersion3() {
        return nUnitVersion(NUnitVersion.NUNIT_3);
    }

    private NUnitRunnerTask nUnitVersion(@NotNull NUnitVersion nUnitVersion) {
        ImporterUtils.checkNotNull("nUnitVersion", nUnitVersion);
        this.nUnitVersion = nUnitVersion;
        return this;
    }

    /**
     * Specify an assembly (.dll), Visual Studio project (.csproj), or NUnit Test Suite (.nunit) to test.
     * <p>
     * This field is mandatory.
     */
    public NUnitRunnerTask nUnitTestFiles(@NotNull String nUnitTestFiles) {
        ImporterUtils.checkNotNull("nUnitTestFiles", nUnitTestFiles);
        this.nUnitTestFiles = nUnitTestFiles;
        return this;
    }

    /**
     * The name Bamboo should give to the results file produced by NUnit. This is an XML file.
     * e.g. "TestResult.xml"
     * <p>
     * This field is mandatory.
     */
    public NUnitRunnerTask resultFilename(@NotNull String resultFilename) {
        ImporterUtils.checkNotNull("resultFilename", resultFilename);
        this.resultFilename = resultFilename;
        return this;
    }

    /**
     * Adds tests to run. For each of them specify the full name of the test to run.
     * The name of the test may be that of a test case, test fixture or namespace.
     * <p>
     * In case this field is left empty NUnit will execute all tests from the specified test file.
     */
    public NUnitRunnerTask testsToRun(@NotNull String... testsToRun) {
        ImporterUtils.checkNotNull("testsToRun", testsToRun);
        Arrays.stream(testsToRun)
                .forEach(this.testsToRun::add);
        return this;
    }

    /**
     * Adds categories to include.
     */
    public NUnitRunnerTask testCategoriesToInclude(@NotNull String... testCategoriesToInclude) {
        ImporterUtils.checkNotNull("testCategoriesToInclude", testCategoriesToInclude);
        Arrays.stream(testCategoriesToInclude)
                .forEach(this.testCategoriesToInclude::add);
        return this;
    }

    /**
     * Adds categories to exclude. Exclusions take precedence over inclusions.
     */
    public NUnitRunnerTask testCategoriesToExclude(@NotNull String... testCategoriesToExclude) {
        ImporterUtils.checkNotNull("testCategoriesToExclude", testCategoriesToExclude);
        Arrays.stream(testCategoriesToExclude)
                .forEach(this.testCategoriesToExclude::add);
        return this;
    }

    /**
     * Add any command line options or switches you wish to include when running NUnit.
     */
    public NUnitRunnerTask commandLineOptions(@Nullable String commandLineOptions) {
        this.commandLineOptions = commandLineOptions;
        return this;
    }

    /**
     * Environment variables which will be passed to runner process.
     */
    public NUnitRunnerTask environmentVariables(@Nullable String environmentVariables) {
        this.environmentVariables = environmentVariables;
        return this;
    }

    @NotNull
    @Override
    protected NUnitRunnerTaskProperties build() {
        return new NUnitRunnerTaskProperties(description, taskEnabled, executable, nUnitVersion, nUnitTestFiles, resultFilename,
                testsToRun, testCategoriesToInclude, testCategoriesToExclude, commandLineOptions, environmentVariables,
                requirements, conditions);
    }
}
