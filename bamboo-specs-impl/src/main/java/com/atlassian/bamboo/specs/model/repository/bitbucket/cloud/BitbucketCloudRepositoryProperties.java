package com.atlassian.bamboo.specs.model.repository.bitbucket.cloud;

import com.atlassian.bamboo.specs.api.codegen.annotations.CodeGenerator;
import com.atlassian.bamboo.specs.api.codegen.annotations.Setter;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsChangeDetectionProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryProperties;
import com.atlassian.bamboo.specs.api.model.repository.viewer.VcsRepositoryViewerProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.specs.codegen.emitters.repository.BBCloudAccountAuthenticationEmitter;
import com.atlassian.bamboo.specs.codegen.emitters.repository.BBCloudCheckoutAuthenticationEmitter;
import com.atlassian.bamboo.specs.codegen.emitters.repository.BBCloudRepositorySlugEmitter;
import com.atlassian.bamboo.specs.model.repository.git.AuthenticationProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNoErrors;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotBlank;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateNotContainsShellInjectionRelatedCharacters;

public class BitbucketCloudRepositoryProperties extends VcsRepositoryProperties {

    private static final AtlassianModuleProperties ATLASSIAN_MODULE_PROPERTIES =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.atlassian-bamboo-plugin-bitbucket:bbCloud");
    private static final ValidationContext VALIDATION_CONTEXT = ValidationContext.of("Bitbucket Cloud repository");

    public static final boolean DEFAULT_USE_SUBMODULES = false;
    public static final boolean DEFAULT_FETCH_WHOLE_REPOSITORY = false;
    public static final boolean DEFAULT_SHALLOW_CLONES = false;
    public static final boolean DEFAULT_LFS = false;
    public static final boolean DEFAULT_REMOTE_AGENT_CACHE = false;
    public static final boolean DEFAULT_VERBOSE_LOGS = false;
    public static final Duration DEFAULT_COMMAND_TIMEOUT = Duration.ofMinutes(180);

    @CodeGenerator(BBCloudRepositorySlugEmitter.class)
    private final String repositorySlug;
    @CodeGenerator(BBCloudAccountAuthenticationEmitter.class)
    private final AuthenticationProperties authenticationProperties;
    @CodeGenerator(BBCloudCheckoutAuthenticationEmitter.class)
    private final AuthenticationProperties checkoutAuthenticationProperties;

    private String branch;

    //advanced server opts
    @Setter("shallowClonesEnabled")
    private final boolean useShallowClones;
    @Setter("remoteAgentCacheEnabled")
    private final boolean useRemoteAgentCache;
    @Setter("submodulesEnabled")
    private final boolean useSubmodules;
    private final Duration commandTimeout;
    private final boolean verboseLogs;
    private final boolean fetchWholeRepository;
    @Setter("lfsEnabled")
    private final boolean useLfs;

    //change detection opts
    private VcsChangeDetectionProperties vcsChangeDetection;

    private BitbucketCloudRepositoryProperties() {
        branch = "";

        repositorySlug = null;
        authenticationProperties = null;
        checkoutAuthenticationProperties = null;

        useSubmodules = DEFAULT_USE_SUBMODULES;
        fetchWholeRepository = DEFAULT_FETCH_WHOLE_REPOSITORY;
        useShallowClones = DEFAULT_SHALLOW_CLONES;
        commandTimeout = DEFAULT_COMMAND_TIMEOUT;

        useLfs = DEFAULT_LFS;
        useRemoteAgentCache = DEFAULT_REMOTE_AGENT_CACHE;
        verboseLogs = DEFAULT_VERBOSE_LOGS;
    }


    public BitbucketCloudRepositoryProperties(@Nullable String name,
                                              @Nullable BambooOidProperties oid,
                                              @Nullable String description,
                                              @Nullable String parent,
                                              @Nullable VcsRepositoryViewerProperties repositoryViewerProperties,
                                              @NotNull String repositorySlug,
                                              @Nullable AuthenticationProperties authenticationProperties,
                                              @Nullable AuthenticationProperties checkoutAuthenticationProperties,
                                              @NotNull String branch,
                                              boolean useShallowClones,
                                              boolean useRemoteAgentCache,
                                              boolean useSubmodules,
                                              Duration commandTimeout,
                                              boolean verboseLogs,
                                              boolean fetchWholeRepository,
                                              boolean useLfs,
                                              VcsChangeDetectionProperties vcsChangeDetection) throws PropertiesValidationException {
        super(name, oid, description, parent, repositoryViewerProperties);
        this.repositorySlug = repositorySlug;
        this.authenticationProperties = authenticationProperties;
        this.checkoutAuthenticationProperties = checkoutAuthenticationProperties;
        this.branch = branch;
        this.useShallowClones = useShallowClones;
        this.useRemoteAgentCache = useRemoteAgentCache;
        this.useSubmodules = useSubmodules;
        this.commandTimeout = commandTimeout;
        this.verboseLogs = verboseLogs;
        this.fetchWholeRepository = fetchWholeRepository;
        this.useLfs = useLfs;
        this.vcsChangeDetection = vcsChangeDetection;

        validate();
    }

    @Nullable
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_MODULE_PROPERTIES;
    }


    public String getBranch() {
        return branch;
    }

    public boolean isUseShallowClones() {
        return useShallowClones;
    }

    public boolean isUseRemoteAgentCache() {
        return useRemoteAgentCache;
    }

    public boolean isUseSubmodules() {
        return useSubmodules;
    }

    public Duration getCommandTimeout() {
        return commandTimeout;
    }

    public boolean isVerboseLogs() {
        return verboseLogs;
    }

    public boolean isFetchWholeRepository() {
        return fetchWholeRepository;
    }

    public boolean isUseLfs() {
        return useLfs;
    }

    public VcsChangeDetectionProperties getVcsChangeDetection() {
        return vcsChangeDetection;
    }

    public String getRepositorySlug() {
        return repositorySlug;
    }

    @Nullable
    public AuthenticationProperties getAuthenticationProperties() {
        return authenticationProperties;
    }

    @Nullable
    public AuthenticationProperties getCheckoutAuthenticationProperties() {
        return checkoutAuthenticationProperties;
    }

    @Override
    public void validate() {
        super.validate();
        if (!hasParent()) {
            checkNotBlank(VALIDATION_CONTEXT, "repositorySlug", repositorySlug);
            checkNotBlank(VALIDATION_CONTEXT, "branch", branch);
        }

        final List<ValidationProblem> errors = new ArrayList<>();
        validateNotContainsShellInjectionRelatedCharacters(VALIDATION_CONTEXT, branch)
                .ifPresent(errors::add);

        checkNoErrors(errors);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        BitbucketCloudRepositoryProperties that = (BitbucketCloudRepositoryProperties) o;
        return useShallowClones == that.useShallowClones &&
                useRemoteAgentCache == that.useRemoteAgentCache &&
                useSubmodules == that.useSubmodules &&
                verboseLogs == that.verboseLogs &&
                fetchWholeRepository == that.fetchWholeRepository &&
                useLfs == that.useLfs &&
                Objects.equals(repositorySlug, that.repositorySlug) &&
                Objects.equals(authenticationProperties, that.authenticationProperties) &&
                Objects.equals(checkoutAuthenticationProperties, that.checkoutAuthenticationProperties) &&
                Objects.equals(branch, that.branch) &&
                Objects.equals(commandTimeout, that.commandTimeout) &&
                Objects.equals(vcsChangeDetection, that.vcsChangeDetection);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), repositorySlug, authenticationProperties,
                checkoutAuthenticationProperties, branch, useShallowClones, useRemoteAgentCache,
                useSubmodules, commandTimeout, verboseLogs, fetchWholeRepository, useLfs, vcsChangeDetection);
    }
}
