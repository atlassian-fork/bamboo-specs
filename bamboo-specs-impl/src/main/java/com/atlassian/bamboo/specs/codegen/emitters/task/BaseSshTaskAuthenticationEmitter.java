package com.atlassian.bamboo.specs.codegen.emitters.task;

import com.atlassian.bamboo.specs.api.builders.credentials.SharedCredentialsIdentifier;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.model.credentials.SharedCredentialsIdentifierProperties;
import com.atlassian.bamboo.specs.model.task.BaseSshTaskProperties;
import org.jetbrains.annotations.NotNull;

/**
 * Generate code for SSH/SCP task authorization.
 */
class BaseSshTaskAuthenticationEmitter {
    String emitCodeForAuthentication(@NotNull BaseSshTaskProperties entity, CodeGenerationContext context) {
        final SharedCredentialsIdentifierProperties sharedCredentials = entity.getSharedCredentialsIdentifierProperties();
        if (sharedCredentials == null) {
            switch (entity.getAuthenticationType()) {
                case PASSWORD:
                    return String.format(".authenticateWithPassword(\"%s\")", entity.getPassword());
                case KEY_WITHOUT_PASSPHRASE:
                    return String.format(".authenticateWithKey(\"%s\")", entity.getKey());
                case KEY_WITH_PASSPHRASE:
                    return String.format(".authenticateWithKeyWithPassphrase(\"%s\", \"%s\")", entity.getKey(), entity.getPassphrase());
                default:
                    return ".unknownAuthorization() //" + entity.getAuthenticationType();
            }
        } else {
            switch(entity.getAuthenticationType()) {
                case PASSWORD:
                    return String.format(".authenticateWithUsernamePasswordSharedCredentials(new %s(\"%s\"))", context.importClassName(SharedCredentialsIdentifier.class), sharedCredentials.getName());
                case KEY_WITH_PASSPHRASE:
                case KEY_WITHOUT_PASSPHRASE:
                    return String.format(".authenticateWithSshSharedCredentials(new %s(\"%s\"))", context.importClassName(SharedCredentialsIdentifier.class), sharedCredentials.getName());
                default:
                    return ".unknownSharedCredentialsAuthorization() //" + entity.getAuthenticationType();
            }
        }
    }
}
