package com.atlassian.bamboo.specs.model.trigger;

import com.atlassian.bamboo.specs.api.builders.trigger.RepositoryBasedTrigger.TriggeringRepositoriesType;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;
import com.atlassian.bamboo.specs.api.model.trigger.RepositoryBasedTriggerProperties;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.List;

@Immutable
public final class BitbucketServerTriggerProperties extends RepositoryBasedTriggerProperties {
    private static final String NAME = "Bitbucket Server repository triggered";
    private static final String MODULE_KEY = "com.atlassian.bamboo.plugins.stash.atlassian-bamboo-plugin-stash:stashTrigger";

    private static final AtlassianModuleProperties MODULE = new AtlassianModuleProperties(MODULE_KEY);

    private BitbucketServerTriggerProperties() {
        super(NAME, null, true, TriggeringRepositoriesType.ALL, Collections.emptyList());
    }

    public BitbucketServerTriggerProperties(final String description,
                                            final boolean isEnabled,
                                            final TriggeringRepositoriesType triggeringRepositoriesType,
                                            final List<VcsRepositoryIdentifierProperties> selectedTriggeringRepositories) throws PropertiesValidationException {
        super(NAME, description, isEnabled, triggeringRepositoriesType, selectedTriggeringRepositories);
        validate();
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return MODULE;
    }
}
