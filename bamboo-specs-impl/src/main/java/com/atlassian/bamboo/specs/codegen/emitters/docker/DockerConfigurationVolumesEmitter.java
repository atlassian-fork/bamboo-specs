package com.atlassian.bamboo.specs.codegen.emitters.docker;

import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.api.model.docker.DockerConfigurationProperties;
import com.atlassian.bamboo.specs.codegen.emitters.fragment.FieldSetterEmitter;
import com.atlassian.bamboo.specs.codegen.emitters.value.LiteralEmitter;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DockerConfigurationVolumesEmitter extends FieldSetterEmitter<Map<String, String>> {
    public DockerConfigurationVolumesEmitter() {
        super("volume");
    }

    @NotNull
    @Override
    public String emitCode(@NotNull CodeGenerationContext context, @NotNull Map<String, String> volumes) throws CodeGenerationException {
        final List<String> clauses = new ArrayList<>();
        final LiteralEmitter literalEmitter = new LiteralEmitter();
        final Map<String, String> volumesToEmit = new LinkedHashMap<>(volumes);

        final Map<String, String> defaultVolumes = DockerConfigurationProperties.DEFAULT_VOLUMES;
        final boolean defaultsVolumesUsed = volumes.entrySet().containsAll(defaultVolumes.entrySet());

        if (defaultsVolumesUsed) {
            volumesToEmit.keySet().removeAll(defaultVolumes.keySet());
        } else {
            clauses.add(".withoutDefaultVolumes()");
        }

        for (Map.Entry<String, String> e : volumesToEmit.entrySet()) {
            clauses.add(String.format(".volume(%s, %s)",
                    literalEmitter.emitCode(context, e.getKey()),
                    literalEmitter.emitCode(context, e.getValue())));
        }

        return clauses.stream()
                .collect(Collectors.joining(context.newLine()));
    }
}
