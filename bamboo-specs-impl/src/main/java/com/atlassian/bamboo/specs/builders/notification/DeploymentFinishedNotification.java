package com.atlassian.bamboo.specs.builders.notification;

import com.atlassian.bamboo.specs.api.builders.Applicability;
import com.atlassian.bamboo.specs.api.builders.notification.NotificationType;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.notification.AnyNotificationTypeProperties;
import org.jetbrains.annotations.NotNull;

import java.util.EnumSet;

/**
 * Represents a notification which is sent when a deployment is completed (with either success or failure).
 */
public class DeploymentFinishedNotification extends NotificationType<DeploymentFinishedNotification, AnyNotificationTypeProperties> {
    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("bamboo.deployments:deploymentFinished");

    @NotNull
    @Override
    protected AnyNotificationTypeProperties build() {
        return new AnyNotificationTypeProperties(ATLASSIAN_PLUGIN, "", EnumSet.of(Applicability.DEPLOYMENTS));
    }
}
