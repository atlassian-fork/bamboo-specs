package com.atlassian.bamboo.specs.codegen.emitters.plan.branches;

import com.atlassian.bamboo.specs.api.codegen.CodeEmitter;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.api.model.plan.branches.PlanBranchManagementProperties;
import org.jetbrains.annotations.NotNull;

public class TriggeringOptionEmitter implements CodeEmitter<PlanBranchManagementProperties.TriggeringOption> {

    @NotNull
    @Override
    public String emitCode(@NotNull CodeGenerationContext context, @NotNull PlanBranchManagementProperties.TriggeringOption value) throws CodeGenerationException {
        switch (value) {
            case INHERITED:
                return ".triggerBuildsLikeParentPlan()";
            case MANUAL:
                return ".triggerBuildsManually()";
            case CUSTOM:
                return "";
            default:
                throw new CodeGenerationException("Unrecognized trigger option: " + value);
        }
    }
}




