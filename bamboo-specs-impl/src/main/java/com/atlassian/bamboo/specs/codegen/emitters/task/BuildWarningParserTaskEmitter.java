package com.atlassian.bamboo.specs.codegen.emitters.task;


import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.builders.task.BuildWarningParserTask;
import com.atlassian.bamboo.specs.codegen.emitters.value.EntityPropertiesEmitter;
import com.atlassian.bamboo.specs.model.task.BuildWarningParserTaskProperties;
import org.jetbrains.annotations.NotNull;

public class BuildWarningParserTaskEmitter extends EntityPropertiesEmitter<BuildWarningParserTaskProperties> {
    @NotNull
    @Override
    public String emitCode(@NotNull CodeGenerationContext context, @NotNull BuildWarningParserTaskProperties entity) throws CodeGenerationException {
        builderClass = BuildWarningParserTask.class;
        fieldsToSkip.add("defaultRepository");
        fieldsToSkip.add("associateWithRepository");
        if (entity.isAssociateWithRepository()) {
            if (entity.isDefaultRepository()) {
                context.incIndentation();
                final String defaultRepositoryCall = context.newLine() + ".defaultRepository()";
                context.decIndentation();
                return emitConstructorInvocation(context, entity) + defaultRepositoryCall + emitFields(context, entity);
            }
        }
        return super.emitCode(context, entity);
    }
}
