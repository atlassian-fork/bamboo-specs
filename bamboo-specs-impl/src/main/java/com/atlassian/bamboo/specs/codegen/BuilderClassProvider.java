package com.atlassian.bamboo.specs.codegen;


import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.api.codegen.annotations.Builder;
import org.jetbrains.annotations.NotNull;

public final class BuilderClassProvider {
    private BuilderClassProvider() {
    }

    /**
     * Find a Bamboo Specs builder class producing specific entity.
     */
    @NotNull
    public static Class findBuilderClass(final Class entityClass) throws CodeGenerationException {
        if (entityClass.isAnnotationPresent(Builder.class)) {
            Builder builderAnnotation = (Builder) entityClass.getAnnotation(Builder.class);
            return builderAnnotation.value();
        }
        //no annotation: fallback to finding by name
        try {
            if (entityClass.isMemberClass()) {
                Class enclosingBuilderClass = findBuilderClass(entityClass.getEnclosingClass());
                if (enclosingBuilderClass == null) {
                    return null;
                }
                for (Class memberBuilderClasses : enclosingBuilderClass.getClasses()) {
                    if (memberBuilderClasses.getSimpleName().equals(entityClass.getSimpleName().replace("Properties", ""))) {
                        return memberBuilderClasses;
                    }
                }
            }
            String fullQualifiedName = entityClass.getCanonicalName();
            String builderClassName = fullQualifiedName.replace("model", "builders").replace("Properties", "");
            return Class.forName(builderClassName);
        } catch (Exception e) {
            throw new CodeGenerationException("Could not find builder for entityClass " + entityClass.getCanonicalName());
        }
    }


}
