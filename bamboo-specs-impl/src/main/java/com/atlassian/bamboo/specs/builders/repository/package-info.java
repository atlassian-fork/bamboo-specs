/**
 * Various types of source code repositories.
 */
package com.atlassian.bamboo.specs.builders.repository;
