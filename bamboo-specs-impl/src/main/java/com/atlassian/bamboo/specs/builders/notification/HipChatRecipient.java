package com.atlassian.bamboo.specs.builders.notification;

import com.atlassian.bamboo.specs.api.builders.notification.NotificationRecipient;
import com.atlassian.bamboo.specs.model.notification.HipChatRecipientProperties;
import org.jetbrains.annotations.NotNull;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotBlank;

/**
 * Represents Hipchat account Bamboo can send notifications to.
 */
public class HipChatRecipient extends NotificationRecipient<HipChatRecipient, HipChatRecipientProperties> {

    private String apiToken;
    private String room;
    private boolean notifyUsers;

    /**
     * Specifies api token of the Hipchat account.
     */
    public HipChatRecipient apiToken(@NotNull String apiToken) {
        checkNotBlank("apiToken", apiToken);
        this.apiToken = apiToken;
        return this;
    }

    /**
     * Specifies room to send notifications to.
     */
    public HipChatRecipient room(@NotNull String room) {
        checkNotBlank("room", room);
        this.room = room;
        return this;
    }

    /**
     * Specifies if Hipchat should notify its users when new notification appears in the Hipchat room.
     */
    public HipChatRecipient notifyUsers(boolean notifyUsers) {
        this.notifyUsers = notifyUsers;
        return this;
    }

    @NotNull
    @Override
    protected HipChatRecipientProperties build() {
        return new HipChatRecipientProperties(apiToken, room, notifyUsers);
    }
}
