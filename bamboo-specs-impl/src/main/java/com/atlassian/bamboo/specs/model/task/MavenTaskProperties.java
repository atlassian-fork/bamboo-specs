package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.codegen.annotations.CodeGenerator;
import com.atlassian.bamboo.specs.api.codegen.annotations.SkipCodeGen;
import com.atlassian.bamboo.specs.api.codegen.annotations.Setter;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.builders.task.MavenTask;
import com.atlassian.bamboo.specs.codegen.emitters.task.MavenVersionEmitter;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkThat;

@Immutable
public final class MavenTaskProperties extends TaskProperties {
    private static final AtlassianModuleProperties V2_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.maven:task.builder.mvn2");
    private static final AtlassianModuleProperties V3_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.maven:task.builder.mvn3");

    @NotNull
    protected String goal;
    @Nullable
    protected String projectFile;
    @Nullable
    protected String environmentVariables;
    @Nullable
    protected String jdk;
    @Nullable
    protected String executableLabel;
    protected boolean hasTests;
    @SkipCodeGen
    @Nullable
    protected MavenTask.TestDirectoryOption testDirectoryOption = MavenTask.TestDirectoryOption.STANDARD;
    @Setter("testResultsPath")
    @Nullable
    protected String testResultsDirectory;
    @Nullable
    protected String workingSubdirectory;
    protected boolean useMavenReturnCode;
    @CodeGenerator(MavenVersionEmitter.class)
    protected int version = MavenTask.MAVEN_V3;

    private MavenTaskProperties() {
        super();
    }

    public MavenTaskProperties(@Nullable String description, boolean enabled,
                               @NotNull String goal,
                               @Nullable String projectFile,
                               @Nullable String environmentVariables,
                               @Nullable String jdk,
                               @NotNull String executableLabel,
                               boolean hasTests,
                               @Nullable MavenTask.TestDirectoryOption testDirectoryOption,
                               @Nullable String testResultsDirectory,
                               @Nullable String workingSubdirectory,
                               boolean useMavenReturnCode,
                               int version,
                               @NotNull List<RequirementProperties> requirements,
                               @NotNull List<? extends ConditionProperties> conditions) throws PropertiesValidationException {
        super(description, enabled, requirements, conditions);
        this.goal = goal;
        this.projectFile = projectFile;
        this.environmentVariables = environmentVariables;
        this.jdk = jdk;
        this.executableLabel = executableLabel;
        this.hasTests = hasTests;
        if (testDirectoryOption != null) {
            this.testDirectoryOption = testDirectoryOption;
        }
        this.testResultsDirectory = testResultsDirectory;
        this.workingSubdirectory = workingSubdirectory;
        this.useMavenReturnCode = useMavenReturnCode;
        this.version = version;

        validate();
    }

    @NotNull
    public String getGoal() {
        return goal;
    }

    @Nullable
    public String getProjectFile() {
        return projectFile;
    }

    @Nullable
    public String getEnvironmentVariables() {
        return environmentVariables;
    }

    @Nullable
    public String getJdk() {
        return jdk;
    }

    @Nullable
    public String getExecutableLabel() {
        return executableLabel;
    }

    public boolean isHasTests() {
        return hasTests;
    }

    @NotNull
    public MavenTask.TestDirectoryOption getTestDirectoryOption() {
        return testDirectoryOption;
    }

    @Nullable
    public String getTestResultsDirectory() {
        return testResultsDirectory;
    }

    @Nullable
    public String getWorkingSubdirectory() {
        return workingSubdirectory;
    }

    public boolean isUseMavenReturnCode() {
        return useMavenReturnCode;
    }

    @NotNull
    public int getVersion() {
        return version;
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        if (version == MavenTask.MAVEN_V2) {
            return V2_PLUGIN;
        } else if (version == MavenTask.MAVEN_V3) {
            return V3_PLUGIN;
        }
        throw new IllegalStateException("Version " + version + " is not supported. Please use values: 2 or 3");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final MavenTaskProperties that = (MavenTaskProperties) o;
        return hasTests == that.hasTests &&
                useMavenReturnCode == that.useMavenReturnCode &&
                version == that.version &&
                Objects.equals(goal, that.goal) &&
                Objects.equals(projectFile, that.projectFile) &&
                Objects.equals(environmentVariables, that.environmentVariables) &&
                Objects.equals(jdk, that.jdk) &&
                Objects.equals(executableLabel, that.executableLabel) &&
                testDirectoryOption == that.testDirectoryOption &&
                Objects.equals(testResultsDirectory, that.testResultsDirectory) &&
                Objects.equals(workingSubdirectory, that.workingSubdirectory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(),
                goal,
                projectFile,
                environmentVariables,
                jdk,
                executableLabel,
                hasTests,
                testDirectoryOption,
                testResultsDirectory,
                workingSubdirectory,
                useMavenReturnCode,
                version);
    }

    @Override
    public void validate() {
        super.validate();
        final ValidationContext context = ValidationContext.of("Maven task");
        checkThat(context, StringUtils.isNotBlank(goal), "Goal is not defined");
        checkThat(context, StringUtils.isNotBlank(executableLabel), "Executable label is not defined");
        checkThat(context, version == MavenTask.MAVEN_V2 || version == MavenTask.MAVEN_V3, "Only versions 2 or 3 are supported, not " + version);
    }
}
