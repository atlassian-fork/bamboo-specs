package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.codegen.annotations.CodeGenerator;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.codegen.emitters.task.VcsCommitTaskEmitter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkRequiredNotBlank;

@CodeGenerator(VcsCommitTaskEmitter.class)
public final class VcsCommitTaskProperties extends BaseVcsTaskProperties {
    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.vcs:task.vcs.commit");
    private static final ValidationContext VALIDATION_CONTEXT = ValidationContext.of("VCS Commit Task");

    @NotNull
    private final String commitMessage;

    private VcsCommitTaskProperties() {
        super();
        this.commitMessage = null;
    }

    public VcsCommitTaskProperties(@Nullable String description,
                                   boolean enabled,
                                   @NotNull List<RequirementProperties> requirements,
                                   @NotNull List<? extends ConditionProperties> conditions,
                                   boolean defaultRepository,
                                   @Nullable VcsRepositoryIdentifierProperties repository,
                                   @Nullable String workingSubdirectory,
                                   @NotNull String commitMessage) {
        super(description, enabled, requirements, conditions, defaultRepository, repository, workingSubdirectory);
        this.commitMessage = commitMessage;
        validate();
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    @NotNull
    @Override
    protected ValidationContext getValidationContext() {
        return VALIDATION_CONTEXT;
    }

    @Override
    public void validate() {
        super.validate();
        checkRequiredNotBlank(getValidationContext().with("commit message"), commitMessage);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VcsCommitTaskProperties)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final VcsCommitTaskProperties that = (VcsCommitTaskProperties) o;
        return Objects.equals(commitMessage, that.commitMessage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), commitMessage);
    }

    @NotNull
    public String getCommitMessage() {
        return commitMessage;
    }
}
