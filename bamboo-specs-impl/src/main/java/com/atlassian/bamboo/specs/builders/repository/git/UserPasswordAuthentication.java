package com.atlassian.bamboo.specs.builders.repository.git;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.model.repository.git.UserPasswordAuthenticationProperties;
import org.jetbrains.annotations.Nullable;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * User name/password authentication method for git repository.
 */
public class UserPasswordAuthentication extends EntityPropertiesBuilder<UserPasswordAuthenticationProperties> {
    private String username;
    private String password;

    /**
     * Specifies new username/password credentials with given git user name.
     *
     * @param username user name
     */
    public UserPasswordAuthentication(String username) {
        checkNotNull("username", username);

        this.username = username;
    }

    /**
     * Sets git user name.
     */
    public UserPasswordAuthentication username(String username) {
        checkNotNull("username", username);

        this.username = username;
        return this;
    }

    /**
     * Sets password to a git server. The password can be both in Bamboo encrypted and plain form.
     *
     * @see <a href="https://confluence.atlassian.com/bamboo/bamboo-specs-encryption-970268127.html">Encryption in Bamboo Specs</a>
     */
    public UserPasswordAuthentication password(@Nullable String password) {
        this.password = password;
        return this;
    }

    protected UserPasswordAuthenticationProperties build() {
        return new UserPasswordAuthenticationProperties(username, password);
    }
}
