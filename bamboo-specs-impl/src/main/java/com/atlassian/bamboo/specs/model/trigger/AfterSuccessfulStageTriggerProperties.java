package com.atlassian.bamboo.specs.model.trigger;

import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.codegen.annotations.ConstructFrom;
import com.atlassian.bamboo.specs.api.codegen.annotations.Setter;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.builders.trigger.DeploymentTriggerProperties;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotBlank;

@ConstructFrom({"stageName"})
@Immutable
public class AfterSuccessfulStageTriggerProperties extends DeploymentTriggerProperties {
    public static final String NAME = "After successful stage";
    public static final String MODULE_KEY = "com.atlassian.bamboo.triggers.atlassian-bamboo-triggers:afterSuccessfulStage";
    private static final AtlassianModuleProperties ATLASSIAN_MODULE = EntityPropertiesBuilders.build(new AtlassianModule(MODULE_KEY));

    private final String stageName;
    @Setter("triggerByBranch")
    private final String releaseBranch;

    private AfterSuccessfulStageTriggerProperties() {
        super(NAME, null, true);
        stageName = null;
        releaseBranch = null;
    }

    public AfterSuccessfulStageTriggerProperties(String description, boolean triggerEnabled, String stageName, String releaseBranch) throws PropertiesValidationException {
        super(NAME, description, triggerEnabled);
        this.stageName = stageName;
        this.releaseBranch = releaseBranch;
        validate();
    }

    public String getStageName() {
        return stageName;
    }

    public String getReleaseBranch() {
        return releaseBranch;
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_MODULE;
    }

    @Override
    public void validate() throws PropertiesValidationException {
        super.validate();
        checkNotBlank("stageName", stageName);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        AfterSuccessfulStageTriggerProperties that = (AfterSuccessfulStageTriggerProperties) o;
        return Objects.equals(stageName, that.stageName) &&
                Objects.equals(releaseBranch, that.releaseBranch);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), stageName, releaseBranch);
    }
}
