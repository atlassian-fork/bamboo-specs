package com.atlassian.bamboo.specs.util;

import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.constructor.AbstractConstruct;
import org.yaml.snakeyaml.constructor.Construct;
import org.yaml.snakeyaml.nodes.Node;
import org.yaml.snakeyaml.nodes.ScalarNode;
import org.yaml.snakeyaml.nodes.Tag;
import org.yaml.snakeyaml.representer.Represent;

import java.time.Duration;

public enum DurationYamler implements CustomYamlers.CustomYamler {
    INSTANCE;


    @Override
    public Represent getRepresenter() {
        return new RepresentDuration();
    }

    @Override
    public Construct getConstructor() {
        return new ConstructDuration();
    }

    @Override
    public Class<?> getYamledClass() {
        return Duration.class;
    }

    private static class RepresentDuration implements Represent {
        @Override
        public Node representData(final Object data) {
            return representScalar(new Tag(Duration.class), data.toString());
        }
    }

    private static class ConstructDuration extends AbstractConstruct {
        @Override
        public Object construct(final Node node) {
            final String val = (String) toScalarValue((ScalarNode) node);
            return Duration.parse(val);
        }
    }

    private static Node representScalar(final Tag tag, final String value) {
        final DumperOptions.ScalarStyle style = DumperOptions.ScalarStyle.PLAIN;
        return new ScalarNode(tag, value, null, null, style.getChar());
    }

    private static Object toScalarValue(final ScalarNode node) {
        return node.getValue();
    }
}
