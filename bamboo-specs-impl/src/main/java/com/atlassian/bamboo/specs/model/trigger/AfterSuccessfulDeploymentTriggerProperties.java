package com.atlassian.bamboo.specs.model.trigger;

import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.codegen.annotations.ConstructFrom;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.builders.trigger.DeploymentTriggerProperties;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotBlank;

@ConstructFrom("environment")
@Immutable
public class AfterSuccessfulDeploymentTriggerProperties extends DeploymentTriggerProperties {
    public static final String NAME = "After successful deployment";
    public static final String MODULE_KEY = "com.atlassian.bamboo.triggers.atlassian-bamboo-triggers:afterSuccessfulDeployment";
    private static final AtlassianModuleProperties ATLASSIAN_MODULE = EntityPropertiesBuilders.build(new AtlassianModule(MODULE_KEY));
    private final String environment;

    private AfterSuccessfulDeploymentTriggerProperties() {
        environment = null;
    }

    public AfterSuccessfulDeploymentTriggerProperties(String description, boolean triggerEnabled, String environment) throws PropertiesValidationException{
        super(NAME, description, triggerEnabled);
        this.environment = environment;
        validate();
    }

    public String getEnvironment() {
        return environment;
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_MODULE;
    }

    @Override
    public void validate() throws PropertiesValidationException {
        super.validate();
        checkNotBlank("environment", environment);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        AfterSuccessfulDeploymentTriggerProperties that = (AfterSuccessfulDeploymentTriggerProperties) o;
        return Objects.equals(environment, that.environment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), environment);
    }
}
