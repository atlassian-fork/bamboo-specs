package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.builders.Applicability;
import com.atlassian.bamboo.specs.api.codegen.annotations.ConstructFrom;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkArgument;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkRequired;

@ConstructFrom({"testType"})
@Immutable
public final class TestParserTaskProperties extends TaskProperties {
    private static final AtlassianModuleProperties JUNIT_ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.testresultparser:task.testresultparser.junit");

    private static final AtlassianModuleProperties NUNIT_ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugin.dotnet:nunit");

    private static final AtlassianModuleProperties TESTNG_ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.testresultparser:task.testresultparser.testng");

    private static final AtlassianModuleProperties MOCHA_ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.bamboo-nodejs-plugin:task.reporter.mocha");

    private static final AtlassianModuleProperties MSTEST_ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugin.dotnet:mstest");

    private final TestType testType;
    private final List<String> resultDirectories;
    private final boolean pickUpTestResultsCreatedOutsideOfThisBuild;

    public enum TestType {
        JUNIT, TESTNG, NUNIT, MOCHA, MSTEST
    }

    private TestParserTaskProperties() {
        this.testType = TestType.JUNIT;
        this.resultDirectories = Collections.emptyList();
        this.pickUpTestResultsCreatedOutsideOfThisBuild = Defaults.getDefaultPickUpTestResultsCreatedOutsideOfThisBuild();
    }

    public TestParserTaskProperties(final TestType testType,
                                    final String description,
                                    final boolean enabled,
                                    final Collection<String> resultDirectories,
                                    @Nullable final Boolean pickUpTestResultsCreatedOutsideOfThisBuild,
                                    @NotNull List<RequirementProperties> requirements,
                                    @NotNull List<? extends ConditionProperties> conditions) throws PropertiesValidationException {
        super(description, enabled, requirements, conditions);
        this.testType = testType;
        this.resultDirectories = Collections.unmodifiableList(new ArrayList<>(resultDirectories));
        this.pickUpTestResultsCreatedOutsideOfThisBuild = Defaults.getOrDefaultPickUpTestResultsCreatedOutsideOfThisBuild(pickUpTestResultsCreatedOutsideOfThisBuild);
        validate();
    }

    private static class Defaults {
        static boolean getDefaultPickUpTestResultsCreatedOutsideOfThisBuild() {
            return false;
        }

        static boolean getOrDefaultPickUpTestResultsCreatedOutsideOfThisBuild(@Nullable final Boolean pickUpTestResultsCreatedOutsideOfThisBuild) {
            return pickUpTestResultsCreatedOutsideOfThisBuild != null ? pickUpTestResultsCreatedOutsideOfThisBuild : getDefaultPickUpTestResultsCreatedOutsideOfThisBuild();
        }
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        switch (testType) {
            case JUNIT:
                return JUNIT_ATLASSIAN_PLUGIN;
            case TESTNG:
                return TESTNG_ATLASSIAN_PLUGIN;
            case NUNIT:
                return NUNIT_ATLASSIAN_PLUGIN;
            case MOCHA:
                return MOCHA_ATLASSIAN_PLUGIN;
            case MSTEST:
                return MSTEST_ATLASSIAN_PLUGIN;
            default:
                throw new IllegalStateException("Unsupported test type: " + testType);
        }
    }

    public TestType getTestType() {
        return testType;
    }

    public List<String> getResultDirectories() {
        return resultDirectories;
    }

    public boolean getPickUpTestResultsCreatedOutsideOfThisBuild() {
        return pickUpTestResultsCreatedOutsideOfThisBuild;
    }

    @Override
    public void validate() throws PropertiesValidationException {
        final ValidationContext context = ValidationContext.of("Test parser task");
        checkRequired(context.with("Test type"), testType);
        checkArgument(context.with("Result directories"), !resultDirectories.isEmpty(), "Result directory not set.");
    }

    @Override
    public EnumSet<Applicability> applicableTo() {
        return EnumSet.of(Applicability.PLANS);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final TestParserTaskProperties that = (TestParserTaskProperties) o;
        return Objects.equals(this.getTestType(), that.getTestType()) &&
                Objects.equals(this.getResultDirectories(), that.getResultDirectories()) &&
                Objects.equals(this.getPickUpTestResultsCreatedOutsideOfThisBuild(), that.getPickUpTestResultsCreatedOutsideOfThisBuild());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), this.getTestType(), this.getResultDirectories(), this.getPickUpTestResultsCreatedOutsideOfThisBuild());
    }

}
