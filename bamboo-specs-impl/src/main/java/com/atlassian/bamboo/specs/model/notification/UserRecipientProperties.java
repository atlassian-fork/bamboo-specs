package com.atlassian.bamboo.specs.model.notification;

import com.atlassian.bamboo.specs.api.codegen.annotations.ConstructFrom;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.notification.NotificationRecipientProperties;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

@ConstructFrom({"userName"})
public class UserRecipientProperties extends NotificationRecipientProperties {
    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN = new AtlassianModuleProperties("com.atlassian.bamboo.plugin.system.notifications:recipient.user");

    private final String userName;

    private UserRecipientProperties() {
        userName = null;
    }

    public UserRecipientProperties(@NotNull String userName) {
        this.userName = userName;
        validate();
    }

    public String getUserName() {
        return userName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserRecipientProperties that = (UserRecipientProperties) o;
        return Objects.equals(getUserName(), that.getUserName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUserName());
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    @Override
    public void validate() {
        ValidationContext validationContext = ValidationContext.of("userRecipient");
        ImporterUtils.checkNotBlank(validationContext, "userName", userName);
    }
}
