package com.atlassian.bamboo.specs.model.repository.bitbucket.server;

import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

public class BitbucketServerMirrorProperties implements EntityProperties {
    public static final ValidationContext VALIDATION_CONTEXT = ValidationContext.of("Bitbucket Server mirror");

    private final String name;
    private final String url;

    private BitbucketServerMirrorProperties() {
        name = null;
        url = null;
    }

    public BitbucketServerMirrorProperties(@Nullable final String name, @Nullable final String url) {
        this.name = name;
        this.url = url;
        validate();
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BitbucketServerMirrorProperties that = (BitbucketServerMirrorProperties) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(url, that.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, url);
    }

    @Override
    public void validate() {
        ImporterUtils.checkThat(VALIDATION_CONTEXT, isNotBlank(url) || isNotBlank(name),
                "Either url or name must be specified");
    }
}
