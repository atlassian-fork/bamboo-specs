package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.codegen.annotations.Setter;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.util.InliningUtils.preventInlining;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkThat;

@Immutable
public final class AntTaskProperties extends TaskProperties {
    private static final AtlassianModuleProperties MODULE_PROPERTIES =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.ant:task.builder.ant");

    public static final String DEFAULT_TEST_RESULTS_DIRECTORY = preventInlining("**/test-reports/*.xml");

    @NotNull
    protected String target;
    @Nullable
    protected String buildFile;
    @Nullable
    protected String environmentVariables;
    @Nullable
    protected String jdk;
    @Nullable
    protected String executableLabel;
    protected boolean hasTests;
    @Setter("testResultsPath")
    @NotNull
    protected String testResultsDirectory;
    @Nullable
    protected String workingSubdirectory;

    protected AntTaskProperties() {
        super();
        this.testResultsDirectory = DEFAULT_TEST_RESULTS_DIRECTORY;
    }

    public AntTaskProperties(@Nullable String description, boolean enabled,
                             @NotNull String target,
                             @Nullable String buildFile,
                             @Nullable String environmentVariables,
                             @Nullable String jdk,
                             @NotNull String executableLabel,
                             boolean hasTests,
                             @Nullable String testResultsDirectory,
                             @Nullable String workingSubdirectory,
                             @NotNull List<RequirementProperties> requirements,
                             @NotNull List<? extends ConditionProperties> conditions) throws PropertiesValidationException {
        super(description, enabled, requirements, conditions);
        this.target = target;
        this.buildFile = buildFile;
        this.environmentVariables = environmentVariables;
        this.jdk = jdk;
        this.executableLabel = executableLabel;
        this.hasTests = hasTests;
        this.testResultsDirectory = StringUtils.isBlank(testResultsDirectory) ? DEFAULT_TEST_RESULTS_DIRECTORY : testResultsDirectory;
        this.workingSubdirectory = workingSubdirectory;

        validate();
    }

    @NotNull
    public String getTarget() {
        return target;
    }

    @Nullable
    public String getBuildFile() {
        return buildFile;
    }

    @Nullable
    public String getEnvironmentVariables() {
        return environmentVariables;
    }

    @Nullable
    public String getJdk() {
        return jdk;
    }

    @Nullable
    public String getExecutableLabel() {
        return executableLabel;
    }

    public boolean isHasTests() {
        return hasTests;
    }


    @NotNull
    public String getTestResultsDirectory() {
        return testResultsDirectory;
    }

    @Nullable
    public String getWorkingSubdirectory() {
        return workingSubdirectory;
    }


    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return MODULE_PROPERTIES;
    }

    @Override
    public void validate() {
        super.validate();
        final ValidationContext context = ValidationContext.of("Ant task");
        checkThat(context, StringUtils.isNotBlank(target), "Target is not defined");
        checkThat(context, StringUtils.isNotBlank(executableLabel), "Executable label is not defined");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        AntTaskProperties that = (AntTaskProperties) o;
        return hasTests == that.hasTests &&
                Objects.equals(target, that.target) &&
                Objects.equals(buildFile, that.buildFile) &&
                Objects.equals(environmentVariables, that.environmentVariables) &&
                Objects.equals(jdk, that.jdk) &&
                Objects.equals(executableLabel, that.executableLabel) &&
                Objects.equals(testResultsDirectory, that.testResultsDirectory) &&
                Objects.equals(workingSubdirectory, that.workingSubdirectory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), target, buildFile, environmentVariables, jdk, executableLabel, hasTests, testResultsDirectory, workingSubdirectory);
    }
}
