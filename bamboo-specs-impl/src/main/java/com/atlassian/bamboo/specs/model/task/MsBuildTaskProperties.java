package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkThat;

@Immutable
public class MsBuildTaskProperties extends TaskProperties {
    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugin.dotnet:msbuild");
    private static final ValidationContext VALIDATION_CONTEXT = ValidationContext.of("MsBuild task");

    private final String executable;
    private final String projectFile;
    private final String options;
    private final String environmentVariables;
    private final String workingSubdirectory;

    private MsBuildTaskProperties() {
        this.executable = null;
        this.projectFile = null;
        this.options = null;
        this.environmentVariables = null;
        this.workingSubdirectory = null;
    }

    public MsBuildTaskProperties(@Nullable String description,
                                 boolean enabled,
                                 @NotNull String executable,
                                 @NotNull String projectFile,
                                 @NotNull String options,
                                 @NotNull String environmentVariables,
                                 @NotNull String workingSubdirectory,
                                 @NotNull List<RequirementProperties> requirements,
                                 @NotNull List<? extends ConditionProperties> conditions) throws PropertiesValidationException {
        super(description, enabled, requirements, conditions);
        this.executable = executable;
        this.projectFile = projectFile;
        this.options = options;
        this.environmentVariables = environmentVariables;
        this.workingSubdirectory = workingSubdirectory;

        validate();
    }

    @Override
    public void validate() {
        super.validate();

        checkThat(VALIDATION_CONTEXT, StringUtils.isNotBlank(executable), "Executable is not defined");
        checkThat(VALIDATION_CONTEXT, StringUtils.isNotBlank(projectFile), "Project file is not defined");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MsBuildTaskProperties)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final MsBuildTaskProperties that = (MsBuildTaskProperties) o;
        return Objects.equals(getExecutable(), that.getExecutable()) &&
                Objects.equals(getProjectFile(), that.getProjectFile()) &&
                Objects.equals(getOptions(), that.getOptions()) &&
                Objects.equals(getEnvironmentVariables(), that.getEnvironmentVariables()) &&
                Objects.equals(getWorkingSubdirectory(), that.getWorkingSubdirectory());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getExecutable(), getProjectFile(), getOptions(),
                getEnvironmentVariables(), getWorkingSubdirectory());
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    public String getExecutable() {
        return executable;
    }

    public String getProjectFile() {
        return projectFile;
    }

    public String getOptions() {
        return options;
    }

    public String getEnvironmentVariables() {
        return environmentVariables;
    }

    public String getWorkingSubdirectory() {
        return workingSubdirectory;
    }
}
