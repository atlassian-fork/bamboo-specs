package com.atlassian.bamboo.specs.builders.trigger;

import com.atlassian.bamboo.specs.api.builders.trigger.Trigger;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.util.CronExpressionCreationHelper;
import com.atlassian.bamboo.specs.model.trigger.ScheduledTriggerProperties;
import org.jetbrains.annotations.NotNull;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents a Scheduled trigger.
 */
public class ScheduledTrigger extends Trigger<ScheduledTrigger, ScheduledTriggerProperties> {
    private String cronExpression = "0 0 0 ? * *";

    /**
     * Creates trigger which will schedule a build basing on a cron expression.
     */
    public ScheduledTrigger() throws PropertiesValidationException {
    }

    /**
     * Schedules build execution every {@link TimeUnit#SECONDS}, {@link TimeUnit#MINUTES} or {@link
     * TimeUnit#HOURS}.
     */
    public ScheduledTrigger scheduleEvery(int every, @NotNull TimeUnit at) {
        return cronExpression(CronExpressionCreationHelper.scheduleEvery(every, at));
    }

    /**
     * Schedules build execution to every day at specified time.
     */
    public ScheduledTrigger scheduleOnceDaily(@NotNull LocalTime at) {
        return cronExpression(CronExpressionCreationHelper.scheduleOnceDaily(at));
    }

    /**
     * Schedules build execution to every week on specified week days at specified time.
     */
    public ScheduledTrigger scheduleWeekly(@NotNull LocalTime at, DayOfWeek... onDays) {
        return cronExpression(CronExpressionCreationHelper.scheduleWeekly(at, onDays));
    }

    /**
     * Schedules build execution to every week on specified week days and time.
     */
    public ScheduledTrigger scheduleWeekly(@NotNull LocalTime at, @NotNull Collection<DayOfWeek> days) {
        return cronExpression(CronExpressionCreationHelper.scheduleWeekly(at, days));
    }

    /**
     * Schedules build execution to every month on specified day of month and time.
     */
    public ScheduledTrigger scheduleMonthly(@NotNull LocalTime at, int dayOfMonth) {
        return cronExpression(CronExpressionCreationHelper.scheduleMonthly(at, dayOfMonth));
    }

    /**
     * Schedules build execution according to the cron expression. Default value is '0 0 0 ? * *'.
     * <p>
     * Cron expression won't be properly validated until sent out to Bamboo.
     */
    public ScheduledTrigger cronExpression(@NotNull String cronExpression) {
        checkNotNull("cronExpression", cronExpression);
        this.cronExpression = cronExpression;
        return this;
    }

    @Override
    protected ScheduledTriggerProperties build() {
        return new ScheduledTriggerProperties(description, triggerEnabled, cronExpression, null, ScheduledTriggerProperties.Container.PLAN);
    }
}
