package com.atlassian.bamboo.specs.builders.repository.git;

import com.atlassian.bamboo.specs.api.builders.credentials.SharedCredentials;
import com.atlassian.bamboo.specs.api.builders.credentials.SharedCredentialsIdentifier;
import com.atlassian.bamboo.specs.api.builders.repository.VcsChangeDetection;
import com.atlassian.bamboo.specs.api.builders.repository.VcsRepository;
import com.atlassian.bamboo.specs.api.model.repository.VcsChangeDetectionProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.model.repository.git.AuthenticationProperties;
import com.atlassian.bamboo.specs.model.repository.git.GitRepositoryProperties;
import com.atlassian.bamboo.specs.model.repository.git.SharedCredentialsAuthenticationProperties;
import org.jetbrains.annotations.NotNull;

import java.time.Duration;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotBlank;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents a git repository in Bamboo.
 */
public class GitRepository extends VcsRepository<GitRepository, GitRepositoryProperties> {

    private String url;
    private String branch;

    private AuthenticationProperties authentication;

    private boolean useShallowClones;
    private boolean useRemoteAgentCache = true;
    private boolean useSubmodules;
    private Duration commandTimeout = Duration.ofMinutes(180);
    private boolean verboseLogs;
    private boolean fetchWholeRepository;
    private boolean useLfs;

    private VcsChangeDetectionProperties vcsChangeDetection;

    public GitRepository() {
    }

    /**
     * Sets git repository url.
     *
     * @param url repository url
     */
    public GitRepository url(@NotNull String url) {
        checkNotBlank("url", url);
        this.url = url;
        return this;
    }

    /**
     * Sets branch to check out.
     */
    public GitRepository branch(@NotNull String branch) {
        checkNotNull("branch", branch);
        this.branch = branch;
        return this;
    }

    /**
     * Removes authentication details. Bamboo will not enforce any authentication method when connecting to the repository.
     * Depending on the selected protocol and git client configuration, authentication may still be performed out of Bamboo's control.
     */
    public GitRepository withoutAuthentication() {
        this.authentication = null;
        return this;
    }

    /**
     * Selects a previously defined {@link SharedCredentials} to authenticate with git server. Currently SSH and username/password credentials are supported.
     */
    public GitRepository authentication(@NotNull SharedCredentialsIdentifier sharedCredentialsIdentifier) {
        checkNotNull("sharedCredentialsIdentifier", sharedCredentialsIdentifier);
        this.authentication = new SharedCredentialsAuthenticationProperties(EntityPropertiesBuilders.build(sharedCredentialsIdentifier));
        return this;
    }

    /**
     * Specifies username/password authentication.
     */
    public GitRepository authentication(@NotNull UserPasswordAuthentication userPasswordAuthentication) {
        checkNotNull("userPasswordAuthentication", userPasswordAuthentication);
        this.authentication = userPasswordAuthentication.build();
        return this;
    }

    /**
     * Specifies SSH private key authentication.
     */
    public GitRepository authentication(@NotNull SshPrivateKeyAuthentication sshPrivateKeyAuthentication) {
        checkNotNull("sshPrivateKeyAuthentication", sshPrivateKeyAuthentication);
        this.authentication = sshPrivateKeyAuthentication.build();
        return this;
    }

    /**
     * Enables/disables shallow clones when checking out from the repository.
     * Fetches the shallowest commit history possible. Do not use if your build depends on full repository history.
     * Shallow clones are switched off by default.
     */
    public GitRepository shallowClonesEnabled(boolean useShallowClones) {
        this.useShallowClones = useShallowClones;
        return this;
    }

    /**
     * Enables/disables submodule support. Turned off by default.
     */
    public GitRepository submodulesEnabled(boolean useSubmodules) {
        this.useSubmodules = useSubmodules;
        return this;
    }

    /**
     * Enables/disables caching repository content on the remote and elastic agents. Bamboo uses caching to reduce bandwidth needed when retrieving
     * source code from the repository. The feature is turned on by default.
     */
    public GitRepository remoteAgentCacheEnabled(boolean useRemoteAgentCache) {
        this.useRemoteAgentCache = useRemoteAgentCache;
        return this;
    }

    /**
     * Specifies how much time is given for git commands to finish. Default is 180 minutes.
     */
    public GitRepository commandTimeout(Duration commandTimeout) {
        this.commandTimeout = commandTimeout;
        return this;
    }

    /**
     * Specifies how much time in minutes is given for git commands to finish. Default is 180 minutes.
     */
    public GitRepository commandTimeoutInMinutes(int commandTimeoutMinutes) {
        return commandTimeout(Duration.ofMinutes(commandTimeoutMinutes));
    }

    /**
     * Enables/disables verbose logs from git commands. Off by default.
     */
    public GitRepository verboseLogs(boolean verboseLogs) {
        this.verboseLogs = verboseLogs;
        return this;
    }

    /**
     * Enforces (or not) fetching all remote refs from the repository rather than single branch. Off by default.
     */
    public GitRepository fetchWholeRepository(boolean fetchWholeRepository) {
        this.fetchWholeRepository = fetchWholeRepository;
        return this;
    }

    /**
     * Enables/disables git lfs support. Off by default.
     */
    public GitRepository lfsEnabled(boolean useLfs) {
        this.useLfs = useLfs;
        return this;
    }

    /**
     * Resets all change detection options to defaults.
     */
    public GitRepository defaultChangeDetection() {
        this.vcsChangeDetection = null;
        return this;
    }

    /**
     * Sets change detection options for this repository.
     *
     * @see VcsChangeDetection
     */
    public GitRepository changeDetection(@NotNull VcsChangeDetection vcsChangeDetection) {
        checkNotNull("vcsChangeDetection", vcsChangeDetection);
        this.vcsChangeDetection = EntityPropertiesBuilders.build(vcsChangeDetection);
        return this;
    }

    @Override
    protected GitRepositoryProperties build() {
        return new GitRepositoryProperties(name,
                oid, description,
                parent,
                repositoryViewer,
                url,
                branch,
                authentication,
                vcsChangeDetection,
                useShallowClones,
                useRemoteAgentCache,
                useSubmodules,
                commandTimeout,
                verboseLogs,
                fetchWholeRepository,
                useLfs);
    }
}
