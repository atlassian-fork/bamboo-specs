package com.atlassian.bamboo.specs.codegen.emitters.permission;

import com.atlassian.bamboo.specs.api.builders.permission.PermissionType;
import com.atlassian.bamboo.specs.api.builders.permission.Permissions;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.api.model.permission.AnonymousUserPermissionsProperties;
import com.atlassian.bamboo.specs.api.model.permission.GroupPermissionProperties;
import com.atlassian.bamboo.specs.api.model.permission.LoggedInUserPermissionsProperties;
import com.atlassian.bamboo.specs.api.model.permission.PermissionsProperties;
import com.atlassian.bamboo.specs.api.model.permission.UserPermissionProperties;
import com.atlassian.bamboo.specs.codegen.emitters.value.EntityPropertiesEmitter;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class PermissionPropertiesEmitter extends EntityPropertiesEmitter<PermissionsProperties> {

    @NotNull
    @Override
    public String emitCode(@NotNull final CodeGenerationContext context, @NotNull final PermissionsProperties permissionProperties) throws CodeGenerationException {

        List<String> codeLines = new ArrayList<>();
        builderClass = Permissions.class;
        fieldsToSkip.add("userPermissions");
        fieldsToSkip.add("groupPermissions");
        fieldsToSkip.add("loggedInUserPermissions");
        fieldsToSkip.add("anonymousUserPermissions");

        try {
            context.incIndentation();

            codeLines.add(super.emitCode(context, permissionProperties));
            permissionProperties.getUserPermissions().forEach(userPermissionProperties -> codeLines.add(userPermissions(context, userPermissionProperties)));
            permissionProperties.getGroupPermissions().forEach(groupPermissionProperties -> codeLines.add(groupPermissions(context, groupPermissionProperties)));
            codeLines.add(loggedInUserPermission(context, permissionProperties.getLoggedInUserPermissions()));
            codeLines.add(anonymousPermissions(context, permissionProperties.getAnonymousUserPermissions()));

            return StringUtils.join(codeLines.stream().filter(Objects::nonNull).collect(Collectors.toList()), context.newLine());
        } finally {
            context.decIndentation();
        }
    }


    @Nullable
    private String userPermissions(@NotNull final CodeGenerationContext context, UserPermissionProperties userPermission) {
        if (userPermission.getPermissionTypes().isEmpty()) return null;
        return ".userPermissions(\"" +
                userPermission.getUsername() +
                "\", " +
                permissions(context, userPermission.getPermissionTypes()) +
                ")";
    }

    @Nullable
    private String groupPermissions(@NotNull final CodeGenerationContext context, GroupPermissionProperties groupPermission) {
        if (groupPermission.getPermissionTypes().isEmpty()) return null;
        return ".groupPermissions(\"" +
                groupPermission.getGroup() +
                "\", " +
                permissions(context, groupPermission.getPermissionTypes()) +
                ")";
    }

    @Nullable
    private String loggedInUserPermission(@NotNull final CodeGenerationContext context, LoggedInUserPermissionsProperties loggedInUserPermission) {
        if (loggedInUserPermission.getPermissionTypes().isEmpty()) return null;
        return ".loggedInUserPermissions(" +
                permissions(context, loggedInUserPermission.getPermissionTypes()) +
                ")";
    }

    @Nullable
    private String anonymousPermissions(@NotNull final CodeGenerationContext context, AnonymousUserPermissionsProperties anonymousUserPermission) throws CodeGenerationException {
        if (anonymousUserPermission.getPermissionTypes().isEmpty()) return null;
        if (anonymousUserPermission.getPermissionTypes().size() > 1 ||  anonymousUserPermission.getPermissionTypes().get(0) != PermissionType.VIEW) {
            throw new CodeGenerationException(
                    String.format("Could not generate code for anonymous permission(s): %s. The only supported permission for anonymous user is: %s",
                            anonymousUserPermission.getPermissionTypes(), PermissionType.VIEW));
        }
        return ".anonymousUserPermissionView()";
    }

    private String permissions(@NotNull final CodeGenerationContext context, final Collection<PermissionType> permissions) {
        context.importClassName(PermissionType.class);
        final String prefix = PermissionType.class.getSimpleName() + ".";
        return permissions.stream()
                .map(Object::toString)
                .collect(Collectors.joining(", " + prefix, prefix, ""));
    }




}
