package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.model.task.VcsPushTaskProperties;
import org.jetbrains.annotations.NotNull;

/**
 * This task will push commits created locally by other tasks to the remote repository. Only
 * <a href="https://www.atlassian.com/git/tutorials/what-is-version-control/">DVCS repositories</a> (which distinguish
 * between local and remote commits) can be selected for this task.
 */
public class VcsPushTask extends BaseVcsTask<VcsPushTask, VcsPushTaskProperties> {
    @NotNull
    @Override
    protected VcsPushTaskProperties build() {
        return new VcsPushTaskProperties(
                description,
                taskEnabled,
                requirements,
                conditions,
                defaultRepository,
                repository,
                workingSubdirectory);
    }
}
