package com.atlassian.bamboo.specs.model.repository.viewer;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.repository.viewer.VcsRepositoryViewerProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkRequired;

@Immutable
public final class GenericRepositoryViewerProperties implements VcsRepositoryViewerProperties {

    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.repository.atlassian-bamboo-repository-viewers:genericRepositoryViewer");


    private final String viewerUrl;
    private final String repositoryPath;

    private GenericRepositoryViewerProperties() {
        this.viewerUrl = null;
        this.repositoryPath = null;
    }

    public GenericRepositoryViewerProperties(@NotNull String viewerUrl,
                                             @Nullable String repositoryPath) throws PropertiesValidationException {
        this.viewerUrl = viewerUrl;
        this.repositoryPath = repositoryPath;
        validate();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GenericRepositoryViewerProperties)) {
            return false;
        }
        GenericRepositoryViewerProperties that = (GenericRepositoryViewerProperties) o;
        return Objects.equals(getViewerUrl(), that.getViewerUrl()) &&
                Objects.equals(getRepositoryPath(), that.getRepositoryPath());
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getViewerUrl(), getRepositoryPath());
    }

    @NotNull
    public String getViewerUrl() {
        return viewerUrl;
    }

    @Nullable
    public String getRepositoryPath() {
        return repositoryPath;
    }

    @Override
    public void validate() {
        final ValidationContext context = ValidationContext.of("Generic repository");
        checkRequired(context.with("viewerUrl"), viewerUrl);
    }
}
