package com.atlassian.bamboo.specs.builders.repository.bitbucket.server;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.model.repository.bitbucket.server.BitbucketServerMirrorProperties;
import org.jetbrains.annotations.NotNull;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotBlank;

/**
 * Represents mirror of Bitbucket Server repository. Either name or url must be specified.
 */
public class BitbucketServerMirror extends EntityPropertiesBuilder<BitbucketServerMirrorProperties> {
    private String name = null;
    private String url = null;

    /**
     * Name of the mirror as displayed in the web interface.
     * Either name or url needs to be defined, the missing part is going to be fetched from Bitbucket.
     * When in repository specs mode, both name and url must be defined.
     */
    public BitbucketServerMirror name(@NotNull final String name) {
        checkNotBlank("name", name);
        this.name = name;
        return this;
    }

    /**
     * Clone url for the ssh interface. E.g. 'ssh://git@mirror-au.example.com:7997/foo/bar.git'
     * Either name or url needs to be defined, the missing part is going to be fetched from Bitbucket.
     * When in repository specs mode, both name and url must be defined.
     */
    public BitbucketServerMirror url(@NotNull final String url) {
        checkNotBlank("url", url);
        this.url = url;
        return this;
    }

    @Override
    protected BitbucketServerMirrorProperties build() {
        return new BitbucketServerMirrorProperties(name, url);
    }

}
