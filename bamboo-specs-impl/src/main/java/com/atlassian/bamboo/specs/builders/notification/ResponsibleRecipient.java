package com.atlassian.bamboo.specs.builders.notification;

import com.atlassian.bamboo.specs.api.builders.Applicability;
import com.atlassian.bamboo.specs.api.builders.notification.NotificationRecipient;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.notification.AnyNotificationRecipientProperties;
import org.jetbrains.annotations.NotNull;

import java.util.EnumSet;

/**
 * Represents a responsible user who broke the build.
 */
public class ResponsibleRecipient extends NotificationRecipient<ResponsibleRecipient, AnyNotificationRecipientProperties> {

    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.brokenbuildtracker:" +
                    "recipient.responsible");

    @NotNull
    @Override
    protected AnyNotificationRecipientProperties build() {
        return new AnyNotificationRecipientProperties(ATLASSIAN_PLUGIN, "", EnumSet.of(Applicability.PLANS));
    }
}
