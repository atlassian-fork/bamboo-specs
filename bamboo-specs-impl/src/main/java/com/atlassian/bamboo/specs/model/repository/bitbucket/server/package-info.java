/**
 * Bitbucket Server repository type.
 */
package com.atlassian.bamboo.specs.model.repository.bitbucket.server;
