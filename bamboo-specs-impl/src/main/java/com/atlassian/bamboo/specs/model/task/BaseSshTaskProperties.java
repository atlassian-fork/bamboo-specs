package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.codegen.annotations.SkipCodeGen;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.credentials.SharedCredentialsIdentifierProperties;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.util.InliningUtils.preventInlining;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkPositive;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkRequired;

public abstract class BaseSshTaskProperties extends TaskProperties {
    public static final int DEFAULT_PORT = preventInlining(22);

    private final String host;
    private final String username;
    @SkipCodeGen
    private final SshTaskProperties.AuthenticationType authenticationType;

    @Nullable
    @SkipCodeGen
    private final String password;

    @Nullable
    @SkipCodeGen
    private final String key;

    @Nullable
    @SkipCodeGen
    private final String passphrase;
    @Nullable
    @SkipCodeGen
    private final SharedCredentialsIdentifierProperties sharedCredentialsIdentifierProperties;

    @Nullable
    private final String hostFingerprint;
    private final int port;

    public enum AuthenticationType {
        PASSWORD, KEY_WITHOUT_PASSPHRASE, KEY_WITH_PASSPHRASE
    }

    protected BaseSshTaskProperties() {
        this.host = null;
        this.username = null;
        this.authenticationType = null;
        this.password = null;
        this.key = null;
        this.passphrase = null;
        this.sharedCredentialsIdentifierProperties = null;
        this.hostFingerprint = null;
        this.port = DEFAULT_PORT;

    }

    public BaseSshTaskProperties(final String description,
                                 final boolean enabled,
                                 final String host,
                                 final String username,
                                 final AuthenticationType authenticationType,
                                 @Nullable final String password,
                                 @Nullable final String key,
                                 @Nullable final String passphrase,
                                 @Nullable final SharedCredentialsIdentifierProperties sharedCredentialsIdentifierProperties,
                                 @Nullable final String hostFingerprint,
                                 final int port,
                                 @NotNull List<RequirementProperties> requirements,
                                 @NotNull List<? extends ConditionProperties> conditions) throws PropertiesValidationException {
        super(description, enabled, requirements, conditions);
        this.host = host;
        this.username = username;
        this.authenticationType = authenticationType;
        this.password = password;
        this.key = key;
        this.passphrase = passphrase;
        this.sharedCredentialsIdentifierProperties = sharedCredentialsIdentifierProperties;
        this.hostFingerprint = hostFingerprint;
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public String getUsername() {
        return username;
    }

    public AuthenticationType getAuthenticationType() {
        return authenticationType;
    }

    @Nullable
    public String getPassword() {
        return password;
    }

    @Nullable
    public String getKey() {
        return key;
    }

    @Nullable
    public String getPassphrase() {
        return passphrase;
    }

    @Nullable
    public SharedCredentialsIdentifierProperties getSharedCredentialsIdentifierProperties() {
        return sharedCredentialsIdentifierProperties;
    }

    @Nullable
    public String getHostFingerprint() {
        return hostFingerprint;
    }

    public int getPort() {
        return port;
    }

    @Override
    public void validate() throws PropertiesValidationException {
        super.validate();
        final ValidationContext context = ValidationContext.of("Common SSH task");
        checkRequired(context.with("host"), host);
        checkRequired(context.with("authenticationType"), authenticationType);
        if(sharedCredentialsIdentifierProperties == null) {
            checkRequired(context.with("username"), username);
        }
        checkPositive(context.with("port"), "port", port);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final BaseSshTaskProperties that = (BaseSshTaskProperties) o;
        return port == that.port &&
                Objects.equals(host, that.host) &&
                Objects.equals(username, that.username) &&
                authenticationType == that.authenticationType &&
                Objects.equals(password, that.password) &&
                Objects.equals(key, that.key) &&
                Objects.equals(passphrase, that.passphrase) &&
                Objects.equals(sharedCredentialsIdentifierProperties, that.sharedCredentialsIdentifierProperties) &&
                Objects.equals(hostFingerprint, that.hostFingerprint);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(),
                host,
                username,
                authenticationType,
                password,
                key,
                passphrase,
                sharedCredentialsIdentifierProperties,
                hostFingerprint,
                port);
    }
}
