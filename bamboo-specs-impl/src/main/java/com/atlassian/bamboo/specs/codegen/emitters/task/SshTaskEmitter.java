package com.atlassian.bamboo.specs.codegen.emitters.task;

import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.builders.task.SshTask;
import com.atlassian.bamboo.specs.codegen.emitters.value.EntityPropertiesEmitter;
import com.atlassian.bamboo.specs.model.task.SshTaskProperties;
import org.jetbrains.annotations.NotNull;

public class SshTaskEmitter extends EntityPropertiesEmitter<SshTaskProperties> {
    @NotNull
    @Override
    public String emitCode(@NotNull CodeGenerationContext context, @NotNull SshTaskProperties entity) throws CodeGenerationException {
        builderClass = SshTask.class;

        return emitConstructorInvocation(context, entity)
                + context.incIndentation()
                + new BaseSshTaskAuthenticationEmitter().emitCodeForAuthentication(entity, context)
                + context.decIndentation()
                + emitFields(context, entity);
    }
}
