package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.model.task.CleanWorkingDirectoryTaskProperties;
import org.jetbrains.annotations.NotNull;

/**
 * Represents a task that cleans build's working directory.
 */
public class CleanWorkingDirectoryTask extends Task<CleanWorkingDirectoryTask, CleanWorkingDirectoryTaskProperties> {
    @NotNull
    @Override
    protected CleanWorkingDirectoryTaskProperties build() {
        return new CleanWorkingDirectoryTaskProperties(description, taskEnabled, requirements, conditions);
    }
}
