package com.atlassian.bamboo.specs.model.task;

/**
 * Possible scope of variables.
 *
 * @see com.atlassian.bamboo.specs.builders.task.InjectVariablesTask
 */
public enum InjectVariablesScope {
    LOCAL,
    RESULT
}
