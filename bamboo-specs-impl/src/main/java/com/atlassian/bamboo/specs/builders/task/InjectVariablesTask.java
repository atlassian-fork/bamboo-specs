package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.model.task.InjectVariablesScope;
import com.atlassian.bamboo.specs.model.task.InjectVariablesTaskProperties;
import org.jetbrains.annotations.NotNull;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotEmpty;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents a task that injects Bamboo variables from a file in a simple "key=value" format. File
 * is read on agent when the task is run.
 * <p>
 * By default scope of extracted variables is set to {@link InjectVariablesScope#LOCAL} and the
 * namespace is set to "inject".
 */
public class InjectVariablesTask extends Task<InjectVariablesTask, InjectVariablesTaskProperties> {
    private String path;
    private String namespace = "inject";
    private InjectVariablesScope scope = InjectVariablesScope.LOCAL;

    /**
     * Specifies path to properties file.
     * File will be read when task is run and variables will be created.
     */
    public InjectVariablesTask path(@NotNull String path) {
        checkNotEmpty("path", path);
        this.path = path;
        return this;
    }

    /**
     * Specifies namespace to avoid name conflicts with existing variables.
     */
    public InjectVariablesTask namespace(@NotNull String namespace) {
        checkNotEmpty("namespace", namespace);
        this.namespace = namespace;
        return this;
    }

    /**
     * Specifies scope of the created variables to local.
     * They will be available only in current job.
     */
    public InjectVariablesTask scopeLocal() {
        this.scope = InjectVariablesScope.LOCAL;
        return this;
    }

    /**
     * Specifies scope of the created variables to result.
     * They will be available in subsequent stages of this plan and in releases created from the result.
     */
    public InjectVariablesTask scopeResult() {
        this.scope = InjectVariablesScope.RESULT;
        return this;
    }

    /**
     * Specifies scope of the created variables to result.
     */
    public InjectVariablesTask scope(@NotNull InjectVariablesScope scope) {
        checkNotNull("scope", scope);
        this.scope = scope;
        return this;
    }

    @NotNull
    @Override
    protected InjectVariablesTaskProperties build() {
        return new InjectVariablesTaskProperties(description,
                taskEnabled,
                path,
                namespace,
                scope,
                requirements,
                conditions);

    }
}
