package com.atlassian.bamboo.specs.model.notification;

import com.atlassian.bamboo.specs.api.codegen.annotations.ConstructFrom;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.notification.NotificationRecipientProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import org.apache.commons.validator.routines.EmailValidator;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkThat;

@ConstructFrom({"email"})
public class EmailRecipientProperties extends NotificationRecipientProperties {
    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN = new AtlassianModuleProperties("com.atlassian.bamboo.plugin.system.notifications:recipient.email");
    private final String email;

    private EmailRecipientProperties() {
        email = null;
    }

    public EmailRecipientProperties(String email) {
        this.email = email;
        validate();
    }

    public String getEmail() {
        return email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmailRecipientProperties that = (EmailRecipientProperties) o;
        return Objects.equals(getEmail(), that.getEmail());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getEmail());
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    @Override
    public void validate() {
        ValidationContext context = ValidationContext.of("emailRecipient");
        checkThat(context, EmailValidator.getInstance().isValid(email), "%s is not a valid email", email);
    }
}
