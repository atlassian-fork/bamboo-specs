package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.repository.VcsRepositoryIdentifier;
import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.model.task.BuildWarningParserTaskProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotBlank;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNegative;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents Bamboo task which scans logs and files for build warnings.
 */
public class BuildWarningParserTask extends Task<BuildWarningParserTask, BuildWarningParserTaskProperties> {
    public enum WarningSeverity {LOW, NORMAL, HIGH}

    private String parser;
    @Nullable
    private String filePattern;
    private boolean associateWithRepository;
    private boolean defaultRepository;
    @Nullable
    private VcsRepositoryIdentifierProperties repository;

    private boolean failBuild;
    private int failBuildThreshold;
    private WarningSeverity failBuildSeverity = WarningSeverity.LOW;

    /**
     * Sets the repository for this task to plan's default repository. Default repository is the repository which is the
     * first on the list of plan's repositories.
     */
    public BuildWarningParserTask defaultRepository() {
        this.associateWithRepository = true;
        this.defaultRepository = true;
        this.repository = null;
        return this;
    }

    /**
     * Sets the repository for this task.
     */
    public BuildWarningParserTask repository(@NotNull String repositoryName) {
        checkNotNull("repositoryName", repositoryName);
        this.associateWithRepository = true;
        this.defaultRepository = false;
        return repository(new VcsRepositoryIdentifier().name(repositoryName));
    }

    /**
     * Sets the repository for this task.
     */
    public BuildWarningParserTask repository(@NotNull VcsRepositoryIdentifier repositoryIdentifier) {
        checkNotNull("repositoryIdentifier", repositoryIdentifier);
        this.associateWithRepository = true;
        this.defaultRepository = false;
        this.repository = EntityPropertiesBuilders.build(repositoryIdentifier);
        return this;
    }

    /**
     * Remove association with source repository.
     */
    public BuildWarningParserTask noRepository() {
        this.associateWithRepository = false;
        this.defaultRepository = false;
        this.repository = null;
        return this;
    }

    /**
     * Sets the parser.
     * @param parser display name of the parser (as seen in the Bamboo UI)
     */
    public BuildWarningParserTask parser(@NotNull String parser) {
        checkNotBlank("parser", parser);
        this.parser = parser;
        return this;
    }

    /**
     * Instruments the task to parse build log for warnings. Enabled by default.
     *
     * @see #parseFiles(String)
     */
    public BuildWarningParserTask parseLogs() {
        this.filePattern = null;
        return this;
    }

    /**
     * Instruments the task to parse files matching a pattern for warnings.
     *
     * @param globPattern glob pattern to match
     */
    public BuildWarningParserTask parseFiles(@NotNull String globPattern) {
        this.filePattern = globPattern;
        return this;
    }

    /**
     * Enable this flag for the task to fail if the number of warnings exceeds a threshold.
     *
     * @see #failBuildThreshold(int)
     * @see #failBuildSeverity
     */
    public BuildWarningParserTask failBuild(boolean failBuild) {
        this.failBuild = failBuild;
        return this;
    }

    /**
     * Sets maximum number of warnings for which the build passes.
     * Requires the {@link #failBuild(boolean) failBuild} flag to be set to true.
     *
     */
    public BuildWarningParserTask failBuildThreshold(int failBuildThreshold) {
        checkNotNegative("failBuildThreshold", failBuildThreshold);
        this.failBuildThreshold = failBuildThreshold;
        return this;
    }

    /**
     * Sets minimum severity of warnings that count towards fail build threshold.
     * Requires the {@link #failBuild(boolean) failBuild} flag to be set to true.
     */
    public BuildWarningParserTask failBuildSeverity(@NotNull WarningSeverity failBuildSeverity) {
        checkNotNull("failBuildSeverity", failBuildSeverity);
        this.failBuildSeverity = failBuildSeverity;
        return this;
    }


    @NotNull
    @Override
    protected BuildWarningParserTaskProperties build() {
        return new BuildWarningParserTaskProperties(description,
                taskEnabled,
                requirements,
                conditions,
                parser,
                filePattern,
                associateWithRepository,
                defaultRepository,
                repository,
                failBuild,
                failBuildThreshold,
                failBuildSeverity);
    }
}
