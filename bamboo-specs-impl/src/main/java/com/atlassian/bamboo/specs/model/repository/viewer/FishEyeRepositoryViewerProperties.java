package com.atlassian.bamboo.specs.model.repository.viewer;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.repository.viewer.VcsRepositoryViewerProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkRequired;

@Immutable
public final class FishEyeRepositoryViewerProperties implements VcsRepositoryViewerProperties {

    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.repository.atlassian-bamboo-repository-viewers:fisheye");


    private final String fishEyeUrl;
    private final String repositoryName;
    private final String repositoryPath;

    private FishEyeRepositoryViewerProperties() {
        this.fishEyeUrl = null;
        this.repositoryName = null;
        this.repositoryPath = null;
    }

    public FishEyeRepositoryViewerProperties(@NotNull String fishEyeUrl,
                                             @NotNull String repositoryName,
                                             @Nullable String repositoryPath) throws PropertiesValidationException {
        this.fishEyeUrl = fishEyeUrl;
        this.repositoryName = repositoryName;
        this.repositoryPath = repositoryPath;
        validate();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FishEyeRepositoryViewerProperties)) {
            return false;
        }
        FishEyeRepositoryViewerProperties that = (FishEyeRepositoryViewerProperties) o;
        return Objects.equals(getFishEyeUrl(), that.getFishEyeUrl()) &&
                Objects.equals(getRepositoryName(), that.getRepositoryName()) &&
                Objects.equals(getRepositoryPath(), that.getRepositoryPath());
    }

    @Override
    public void validate() {
        final ValidationContext context = ValidationContext.of("Fisheye repository viewer");
        checkRequired(context.with("fishEyeUrl"), fishEyeUrl);
        checkRequired(context.with("repositoryName"), repositoryName);
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFishEyeUrl(), getRepositoryName(), getRepositoryPath());
    }

    @NotNull
    public String getFishEyeUrl() {
        return fishEyeUrl;
    }

    @NotNull
    public String getRepositoryName() {
        return repositoryName;
    }

    @Nullable
    public String getRepositoryPath() {
        return repositoryPath;
    }
}
