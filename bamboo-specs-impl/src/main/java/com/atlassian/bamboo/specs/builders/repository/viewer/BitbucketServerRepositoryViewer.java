package com.atlassian.bamboo.specs.builders.repository.viewer;

import com.atlassian.bamboo.specs.api.builders.repository.viewer.VcsRepositoryViewer;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.repository.viewer.AnyVcsRepositoryViewerProperties;
import com.atlassian.bamboo.specs.api.model.repository.viewer.VcsRepositoryViewerProperties;
import com.atlassian.bamboo.specs.builders.repository.bitbucket.server.BitbucketServerRepository;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;


/**
 * Represents Bitbucket Server viewer.
 * <p>
 * This viewer is designed to be used with {@link BitbucketServerRepository}
 */
public class BitbucketServerRepositoryViewer extends VcsRepositoryViewer {
    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.stash.atlassian-bamboo-plugin-stash:bbServerViewer");

    @NotNull
    protected VcsRepositoryViewerProperties build() throws PropertiesValidationException {
        return new AnyVcsRepositoryViewerProperties(ATLASSIAN_PLUGIN, Collections.emptyMap());
    }
}
