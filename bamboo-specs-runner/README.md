# Bamboo Specs Runner
## Introduction

Bamboo 6.0 introduced the Bamboo Specs feature, which allows to store plan configuration as source code
and thus manage plans via IDE rather than Bamboo UI. This module contains a Maven plugin which scans a 
Bamboo Specs project looking for annotated classes and executes them in order to publish configuration to 
the Bamboo server. Thus, the Bamboo Specs Runner simplifies and standardises way how plans are published. 

## Installation

Add dependency in the build section in pom.xml, e.g.:

```
  <build>
    <plugins>
      <plugin>
        <groupId>com.atlassian.bamboo</groupId>
        <artifactId>bamboo-specs-runner</artifactId>
        <version>1.0.2</version>
      </plugin>      
    </plugins>    
  </build>
```

## Usage

Call the `mvn bamboo-specs-runner:run` goal from a command line. 

Alternatively, you can declare the goal in pom.xml, either bound to a certain build phase, e.g.:
 
```
<profile>
  <id>publish</id>
  <build>
    <plugins>
      <plugin>
        <groupId>com.atlassian.bamboo</groupId>
        <artifactId>bamboo-specs-runner</artifactId>
        <executions>
          <execution>
            <phase>install</phase>
            <goals>
              <goal>run</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>
</profile>
```
 
or declared as the default goal, e.g.:

```
<profile>
  <id>publish</id>
  <build>
    <defaultGoal>bamboo-specs-runner:run</defaultGoal>
  </build>
</profile>
```

## Documentation

General Bamboo documentation: https://confluence.atlassian.com/display/BAMBOO

Bamboo Specs reference manual: https://docs.atlassian.com/bamboo-specs-docs/latest

Bamboo Specs API JavaDocs: https://docs.atlassian.com/bamboo-specs/latest


## Issue tracker

You can report bugs and feature requests here: https://jira.atlassian.com/browse/BAM


## Contributing

Pull requests, issues and comments welcome. For pull requests:

 * Add tests for new features and bug fixes
 * Follow the existing style
 * Separate unrelated changes into multiple pull requests

See the existing issues for things to start contributing.

For bigger changes, make sure you start a discussion first by creating an issue and explaining the intended change.

Atlassian requires contributors to sign a Contributor License Agreement, known as a CLA. This serves as a record 
stating that the contributor is entitled to contribute the code/documentation/translation to the project and is 
willing to have it used in distributions and derivative works (or is willing to transfer ownership).

Prior to accepting your contributions we ask that you please follow the appropriate link below to digitally sign 
the CLA. The Corporate CLA is for those who are contributing as a member of an organization and the individual 
CLA is for those contributing as an individual.

* [Corporate CLA](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [Individual CLA](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)


## License

Copyright (c) 2017 Atlassian and others. Apache 2.0 licensed, see LICENSE.txt file.
