package com.atlassian.bamboo.specs.maven.sandbox;

import java.security.Permission;

public interface ThreadPermissionVerifier {
    void checkPermission(Permission perm);

    void checkPermission(Permission perm, Object context);
}
