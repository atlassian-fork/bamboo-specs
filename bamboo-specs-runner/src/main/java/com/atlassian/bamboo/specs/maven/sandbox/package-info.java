/**
 * Contains classes to allow running Bamboo specs in secure way.
 * It's planned to be used on server side in repository stred plans feature.
 * It's still early stage implementation.
 */
package com.atlassian.bamboo.specs.maven.sandbox;
