package com.atlassian.bamboo.specs.maven.sandbox;

import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;

public final class SecureMethodInvoker {
    public static class SecureMethodInvocationException extends RuntimeException {
        public SecureMethodInvocationException(final Exception e) {
            super(e);
        }
    }

    private SecureMethodInvoker() {
    }

    public static void startPerThreadSecurity(
            final Map<Thread, ThreadPermissionVerifier> specializedVerifiers,
            final ThreadPermissionVerifier defaultPermissionVerifier) {
        BambooSpecsSecurityManager.setPermissionCheckers(specializedVerifiers, defaultPermissionVerifier);
    }

    public static void endPerThreadSecurity() {
        BambooSpecsSecurityManager.clearPermissionCheckers();
    }

    public static <T> T invoke(final Supplier<T> hostileCode) {
        return invokeInternal(hostileCode);
    }

    private static <T> T invokeInternal(final Supplier<T> hostileCode) {
        final AtomicReference<RuntimeException> exceptionRef = new AtomicReference<>();
        final AtomicReference<Error> errorRef = new AtomicReference<>();
        final AtomicReference<T> result = new AtomicReference<>();
        final Runnable runnable = () -> {
            try {
                System.setSecurityManager(new BambooSpecsSecurityManager());
                result.set(hostileCode.get());
            } catch (final RuntimeException e) {
                exceptionRef.set(e);
            } catch (final Error e) {
                errorRef.set(e);
            } catch (final Exception e) {
                exceptionRef.set(new SecureMethodInvocationException(e));
            }
        };

        final SecurityManager prevSecurityManager = System.getSecurityManager();
        try {
            final Thread thread = new Thread(runnable);
            thread.setName("Low privilege runner");
            thread.start();
            thread.join();

            System.setSecurityManager(prevSecurityManager);
        } catch (final InterruptedException ignored) {
            //we got interrupted, oh well
        }
        throwIfNonEmpty(exceptionRef, errorRef);
        return result.get();
    }

    private static void throwIfNonEmpty(final AtomicReference<RuntimeException> exceptionRef, final AtomicReference<Error> errorRef) {
        final RuntimeException exception = exceptionRef.get();
        if (exception != null) {
            throw exception;
        }

        final Error error = errorRef.get();
        if (error != null) {
            throw error;
        }
    }
}
