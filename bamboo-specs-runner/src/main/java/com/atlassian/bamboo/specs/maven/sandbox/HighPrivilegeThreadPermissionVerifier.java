package com.atlassian.bamboo.specs.maven.sandbox;

import java.security.Permission;

/**
 * A permission checker that grants all permissions.
 */
public enum HighPrivilegeThreadPermissionVerifier implements ThreadPermissionVerifier {
    INSTANCE;

    @Override
    public void checkPermission(final Permission perm) {
    }

    @Override
    public void checkPermission(final Permission perm, final Object context) {
    }
}
