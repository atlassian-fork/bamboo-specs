package com.atlassian.bamboo.specs.api.validators;

import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(JUnitParamsRunner.class)
public class LabelValidatorTest {
    @Test
    @Parameters({"label", "abc-def", "hello_world", "{label}"})
    public void detectsValidLabels(String label) {
        final Optional<ValidationProblem> validationProblem = LabelValidator.validateLabel(label);
        assertThat(validationProblem.isPresent(), is(false));
    }

    @Test
    @Parameters({"xss&label<script>", "label:with:colons", "label\nwith.weird[characters]", "", "#!/bin/bash"})
    public void detectsInvalidLabels(String label) {
        final Optional<ValidationProblem> validationProblem = LabelValidator.validateLabel(label);
        assertThat(validationProblem.isPresent(), is(true));
        assertThat(validationProblem.get(), validationMessage(containsString(label)));
    }

    @Test
    public void producesErrorMessageWithContext() {
        final ValidationContext context = ValidationContext.of("plan").with("labels");
        final Optional<ValidationProblem> validationProblem = LabelValidator.validateLabel(context, "!invalid");
        assertThat(validationProblem.isPresent(), is(true));
        assertThat(validationProblem.get(), validationMessage(allOf(
                containsString(context.toString()),
                containsString("!invalid"))));
    }

    private Matcher<ValidationProblem> validationMessage(Matcher<String> messageMatcher) {
        return new FeatureMatcher<ValidationProblem, String>(messageMatcher, "message", "message") {
            @Override
            protected String featureValueOf(ValidationProblem actual) {
                return actual.getMessage();
            }
        };
    }
}
