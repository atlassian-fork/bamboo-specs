package com.atlassian.bamboo.specs.api.common;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.specs.api.validators.common.ValidationUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.lang.reflect.Method;
import java.util.Collections;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNoErrors;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertThat;

public class ImporterUtilsTest {
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    /**
     * To avoid confusion, methods from {@link ImporterUtils} should not start with {@code #validate} prefix, since such
     * methods are defined by {@link ValidationUtils} and behave differently (they do not throw exception when the
     * condition is not met).
     * <p>
     * Non-throwing methods prefixed with #validate should be used in {@link ValidationUtils} instead (e.g.
     * {@link ValidationUtils#validateRequired(ValidationContext, Object)}).
     */
    @Test
    public void testMethodsAreNotPrefixedWithCheck() {
        final Method[] declaredMethods = ImporterUtils.class.getDeclaredMethods();
        for (Method declaredMethod : declaredMethods) {
            assertThat(declaredMethod.getName(), not(startsWith("validate")));
        }
    }

    @Test
    public void checkNotThrowingWhenNoErrors() throws PropertiesValidationException {
        checkNoErrors(Collections.emptyList());
    }

    @Test
    public void checkIsThrowingWhenSomeErrors() throws PropertiesValidationException {
        expectedException.expect(PropertiesValidationException.class);
        checkNoErrors(Collections.singletonList(new ValidationProblem("")));
    }

}