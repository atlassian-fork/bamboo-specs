package com.atlassian.bamboo.specs.api.validators.plan;

import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.builders.repository.AnyVcsRepository;
import com.atlassian.bamboo.specs.api.builders.repository.PlanRepositoryLink;
import com.atlassian.bamboo.specs.api.builders.repository.VcsRepositoryIdentifier;
import com.atlassian.bamboo.specs.api.model.repository.PlanRepositoryLinkProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;

@RunWith(JUnitParamsRunner.class)
public class PlanValidatorTest {
    private static final AtlassianModule SVN_MODULE = new AtlassianModule("com.atlassian.bamboo.plugin.system.repository:svnv2");
    private static final String REPO_NAME = "the boring repository";
    private static final String VALIDATION_ERROR = "Duplicated repository name within a plan: " + REPO_NAME;

    @Test
    @Parameters
    public void testPlanReposNamesMustBeUnique(List<PlanRepositoryLinkProperties> repositories) {
        final List<String> errors = PlanValidator.validateUniqueRepositoriesNames(repositories).stream()
                .map(ValidationProblem::getMessage)
                .collect(Collectors.toList());
        assertThat(errors, hasSize(1));
        assertThat(errors, hasItem(VALIDATION_ERROR));
    }

    public Object[] parametersForTestPlanReposNamesMustBeUnique() {
        return new Object[][]{
                {Arrays.asList(createGlobalRepository(REPO_NAME), createGlobalRepository(REPO_NAME))},
                {Arrays.asList(createPlanRepository(REPO_NAME), createPlanRepository(REPO_NAME))},
                {Arrays.asList(createPlanRepository(REPO_NAME), createGlobalRepository(REPO_NAME))},
                {Arrays.asList(createGlobalRepository(REPO_NAME), createPlanRepository(REPO_NAME))}
        };
    }

    private PlanRepositoryLinkProperties createGlobalRepository(final String name) {
        final PlanRepositoryLink planRepositoryLink = PlanRepositoryLink.linkToGlobalRepository(new VcsRepositoryIdentifier(name));
        return EntityPropertiesBuilders.build(planRepositoryLink);
    }

    private PlanRepositoryLinkProperties createPlanRepository(final String name) {
        final AnyVcsRepository repository = new AnyVcsRepository(SVN_MODULE).name(name);
        final PlanRepositoryLink planRepositoryLink = new PlanRepositoryLink().localRepositoryDefinition(repository);

        return EntityPropertiesBuilders.build(planRepositoryLink);
    }
}
