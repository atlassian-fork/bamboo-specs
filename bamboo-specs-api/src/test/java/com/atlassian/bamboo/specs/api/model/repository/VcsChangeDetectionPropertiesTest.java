package com.atlassian.bamboo.specs.api.model.repository;

import com.atlassian.bamboo.specs.api.builders.repository.VcsChangeDetection;
import com.atlassian.bamboo.specs.api.builders.repository.VcsChangeDetection.FileFilteringOption;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.time.Duration;
import java.util.Random;

import static org.junit.Assert.assertEquals;

public class VcsChangeDetectionPropertiesTest {
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void vcsChangedDetectionPropertiesConstructedSuccessfully() throws PropertiesValidationException {
        final Random random = new Random();
        final boolean commitIsolationEnabled = random.nextBoolean();
        final FileFilteringOption filterFilePatternOptions = FileFilteringOption.INCLUDE_ONLY;
        final String filterFilePatternRegex = "\\.txt$";
        final String changesetFilePatternRegex = "bin/.*";
        final boolean quietPeriodEnabled = true;
        final Duration quietPeriod = Duration.ofSeconds(7);
        final int maxRetries = 3;

        final VcsChangeDetection changeDetectionBuilder = new VcsChangeDetection()
                .commitIsolationEnabled(commitIsolationEnabled)
                .filterFilePatternOption(filterFilePatternOptions)
                .filterFilePatternRegex(filterFilePatternRegex)
                .changesetFilterPatternRegex(changesetFilePatternRegex)
                .quietPeriodEnabled(quietPeriodEnabled)
                .quietPeriod(quietPeriod)
                .quietPeriodMaxRetries(maxRetries);

        final VcsChangeDetectionProperties changeDetectionProperties = EntityPropertiesBuilders.build(changeDetectionBuilder);

        assertEquals(changeDetectionProperties.isCommitIsolationEnabled(), commitIsolationEnabled);
        assertEquals(changeDetectionProperties.getFilterFilePatternOption(), filterFilePatternOptions);
        assertEquals(changeDetectionProperties.getFilterFilePatternRegex(), filterFilePatternRegex);
        assertEquals(changeDetectionProperties.getChangesetFilterPatternRegex(), changesetFilePatternRegex);
        assertEquals(changeDetectionProperties.isQuietPeriodEnabled(), quietPeriodEnabled);
        assertEquals(changeDetectionProperties.getQuietPeriod(), quietPeriod);
        assertEquals(changeDetectionProperties.getMaxRetries(), maxRetries);

    }

    @Test
    public void vcsChangedDetectionPropertiesMaxRetriesMustBePositive() throws PropertiesValidationException {
        final boolean quietPeriodEnabled = true;
        final int maxRetries = -1;

        expectedException.expect(PropertiesValidationException.class);

        EntityPropertiesBuilders.build(new VcsChangeDetection()
                .quietPeriodEnabled(quietPeriodEnabled)
                .quietPeriodMaxRetries(maxRetries));
    }

    @Test
    public void vcsChangedDetectionPropertiesQuietPeriodMustBePositive() throws PropertiesValidationException {
        final boolean quietPeriodEnabled = true;
        final Duration quietPeriod = Duration.ofSeconds(-1);

        expectedException.expect(PropertiesValidationException.class);

        EntityPropertiesBuilders.build(new VcsChangeDetection()
                .quietPeriodEnabled(quietPeriodEnabled)
                .quietPeriod(quietPeriod));
    }

    @Test
    public void vcsChangedDetectionPropertiesChangesetFilePatternMustBeProperRegex() throws PropertiesValidationException {
        final String changesetFilePatternRegex = "*";

        expectedException.expect(PropertiesValidationException.class);

        EntityPropertiesBuilders.build(new VcsChangeDetection()
                .changesetFilterPatternRegex(changesetFilePatternRegex));
    }

    @Test
    public void vcsChangedDetectionPropertiesFilterFilePatternMustBeProperRegex() throws PropertiesValidationException {
        final String filterFilePatternRegex = "*";

        expectedException.expect(PropertiesValidationException.class);

        EntityPropertiesBuilders.build(new VcsChangeDetection()
                .filterFilePatternOption(FileFilteringOption.INCLUDE_ONLY)
                .filterFilePatternRegex(filterFilePatternRegex));
    }

    @Test
    public void vcsChangedDetectionPropertiesFilterFilePatternMustBeDefinedIfOptionsAre() throws PropertiesValidationException {
        expectedException.expect(PropertiesValidationException.class);

        EntityPropertiesBuilders.build(new VcsChangeDetection()
                .filterFilePatternOption(FileFilteringOption.INCLUDE_ONLY));
    }
}