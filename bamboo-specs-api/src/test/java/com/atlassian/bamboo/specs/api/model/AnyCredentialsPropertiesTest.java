package com.atlassian.bamboo.specs.api.model;

import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.builders.BambooOid;
import com.atlassian.bamboo.specs.api.builders.credentials.AnySharedCredentials;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.credentials.SharedCredentialsProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class AnyCredentialsPropertiesTest {
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void creatingCredentialsPropertiesWithNullNameThrowsException() throws Exception {
        expectedException.expect(PropertiesValidationException.class);
        new AnySharedCredentials(null, null)
                .configuration(Collections.emptyMap());
    }

    @Test
    public void creatingCredentialsPropertiesWithNullConfigurationThrowsException() throws Exception {
        expectedException.expect(PropertiesValidationException.class);
        new AnySharedCredentials("name", null)
                .configuration(null);
    }

    @Test
    public void credentialsPropertiesConstructedSuccessfully() throws PropertiesValidationException {
        final String name = "name";
        final Map<String, Object> configuration = new HashMap<>();
        configuration.put("key", "value");
        final String plugin = "A:B";
        final String oid = "1";

        final SharedCredentialsProperties sharedCredentialsProperties =
                EntityPropertiesBuilders.build(new AnySharedCredentials(name, new AtlassianModule(plugin))
                        .configuration(configuration)
                        .oid(new BambooOid(oid)));

        assertEquals(sharedCredentialsProperties.getName(), name);
        assertEquals(sharedCredentialsProperties.getAtlassianPlugin().getCompleteModuleKey(), plugin);
        assertEquals(sharedCredentialsProperties.getOid().getOid(), oid);
    }

    @Test
    public void credentialsPropertiesWithoutOidConstructedSuccessfully() throws PropertiesValidationException {
        final String name = "name";
        final Map<String, Object> configuration = new HashMap<>();
        configuration.put("key", "value");
        final String plugin = "A:B";

        final SharedCredentialsProperties sharedCredentialsProperties =
                EntityPropertiesBuilders.build(new AnySharedCredentials(name, new AtlassianModule(plugin))
                        .configuration(configuration));

        assertNull(sharedCredentialsProperties.getOid());
    }

    @Test
    public void credentialsPropertiesWithoutModuleThrowsException() throws PropertiesValidationException {
        final String name = "name";
        final Map<String, Object> configuration = new HashMap<>();
        configuration.put("key", "value");

        expectedException.expect(PropertiesValidationException.class);
        EntityPropertiesBuilders.build(new AnySharedCredentials(name, null)
                .configuration(configuration));
    }

    @Test
    public void creatingCredentialsPropertiesWithEmptyNameThrowsException() throws PropertiesValidationException {
        final String name = "   ";
        final Map<String, Object> configuration = new HashMap<>();
        configuration.put("key", "value");
        final String plugin = "A:B";
        final String oid = "1";

        expectedException.expect(PropertiesValidationException.class);
        EntityPropertiesBuilders.build(new AnySharedCredentials(name, new AtlassianModule(plugin))
                .configuration(configuration)
                .oid(new BambooOid(oid)));
    }

    @Test
    public void creatingCredentialsPropertiesWithTooLongNameThrowsException() throws PropertiesValidationException {
        final String name = RandomStringUtils.random(256);
        final Map<String, Object> configuration = new HashMap<>();
        configuration.put("key", "value");
        final String plugin = "A:B";
        final String oid = "1";

        expectedException.expect(PropertiesValidationException.class);
        EntityPropertiesBuilders.build(new AnySharedCredentials(name, new AtlassianModule(plugin))
                .configuration(configuration)
                .oid(new BambooOid(oid)));
    }

    @Test
    public void creatingCredentialsPropertiesWithXssCharactersInNameThrowsException() throws PropertiesValidationException {
        final String name = "<>";
        final Map<String, Object> configuration = new HashMap<>();
        configuration.put("key", "value");
        final String plugin = "A:B";
        final String oid = "1";

        expectedException.expect(PropertiesValidationException.class);
        EntityPropertiesBuilders.build(new AnySharedCredentials(name, new AtlassianModule(plugin))
                .configuration(configuration)
                .oid(new BambooOid(oid)));
    }

}