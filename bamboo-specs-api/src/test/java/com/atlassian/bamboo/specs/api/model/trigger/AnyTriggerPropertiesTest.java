package com.atlassian.bamboo.specs.api.model.trigger;

import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.builders.repository.VcsRepositoryIdentifier;
import com.atlassian.bamboo.specs.api.builders.trigger.AnyTrigger;
import com.atlassian.bamboo.specs.api.builders.trigger.RepositoryBasedTrigger.TriggeringRepositoriesType;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.jetbrains.annotations.NotNull;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

public class AnyTriggerPropertiesTest {
    private static final String NAME = "name";
    private static final String DESCRIPTION = "description";
    private static final Map<String, String> CONFIGURATION = new HashMap<String, String>() {{
        put("param", "value");
    }};
    private static final VcsRepositoryIdentifier REPOSITORY = new VcsRepositoryIdentifier("repository");

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @NotNull
    private AnyTrigger cleanAnyTrigger() {
        return new AnyTrigger(new AtlassianModule("plugin:key"));
    }

    @Test
    public void creatingAnyTriggerWithNullAtlassianModule() throws Exception {
        expectedException.expect(PropertiesValidationException.class);
        final AtlassianModule atlassianModule = null;
        new AnyTrigger(atlassianModule);
    }

    @Test
    public void creatingAnyTriggerWithNullName() throws Exception {
        expectedException.expect(PropertiesValidationException.class);
        EntityPropertiesBuilders.build(cleanAnyTrigger());
    }

    @Test
    public void creatingAnyTriggerWithEmptyName() throws Exception {
        expectedException.expect(PropertiesValidationException.class);
        EntityPropertiesBuilders.build(cleanAnyTrigger().name(""));
    }

    @Test
    public void testSuccessfulCreation() throws Exception {
        AnyTriggerProperties properties = EntityPropertiesBuilders.build(cleanAnyTrigger()
                .name(NAME)
                .description(DESCRIPTION)
                .configuration(CONFIGURATION)
                .enabled(false)
                .selectedTriggeringRepositories(REPOSITORY));

        assertThat(properties.getName(), equalTo(NAME));
        assertThat(properties.getDescription(), equalTo(DESCRIPTION));
        assertThat(properties.getConfiguration(), equalTo(CONFIGURATION));
        assertThat(properties.getTriggeringRepositoriesType(), equalTo(TriggeringRepositoriesType.SELECTED));
        VcsRepositoryIdentifierProperties vcsRepositoryIdentifierProperties = EntityPropertiesBuilders.build(REPOSITORY);
        assertThat(properties.getSelectedTriggeringRepositories(), containsInAnyOrder(vcsRepositoryIdentifierProperties));
        assertFalse(properties.isEnabled());
    }

    @Test
    public void testSuccessfulCreationWithAllTriggeringRepositories() throws Exception {
        AnyTriggerProperties properties = EntityPropertiesBuilders.build(cleanAnyTrigger()
                .name(NAME)
                .description(DESCRIPTION)
                .configuration(CONFIGURATION)
                .enabled(false)
                .allAvailableTriggeringRepositories());

        assertThat(properties.getName(), equalTo(NAME));
        assertThat(properties.getDescription(), equalTo(DESCRIPTION));
        assertThat(properties.getConfiguration(), equalTo(CONFIGURATION));
        assertThat(properties.getTriggeringRepositoriesType(), equalTo(TriggeringRepositoriesType.ALL));
        assertThat(properties.getSelectedTriggeringRepositories(), is(empty()));
        assertFalse(properties.isEnabled());
    }

    @Test
    public void testSuccessfulCreationWithAllTriggeringRepositoriesByDefault() throws Exception {
        AnyTriggerProperties properties = EntityPropertiesBuilders.build(cleanAnyTrigger()
                .name(NAME)
                .description(DESCRIPTION)
                .configuration(CONFIGURATION)
                .enabled(false));

        assertThat(properties.getName(), equalTo(NAME));
        assertThat(properties.getDescription(), equalTo(DESCRIPTION));
        assertThat(properties.getConfiguration(), equalTo(CONFIGURATION));
        assertThat(properties.getTriggeringRepositoriesType(), equalTo(TriggeringRepositoriesType.ALL));
        assertThat(properties.getSelectedTriggeringRepositories(), is(empty()));
        assertFalse(properties.isEnabled());
    }

}

