package com.atlassian.bamboo.specs.api.builders.label;

import com.atlassian.bamboo.specs.api.model.label.EmptyLabelsListProperties;

@Deprecated
public class EmptyLabelsList extends Label {
    public EmptyLabelsList() {
        super("");
    }

    @Override
    protected EmptyLabelsListProperties build() {
        return new EmptyLabelsListProperties();
    }
}
