package com.atlassian.bamboo.specs.api.validators;

import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public final class AtlassianModuleValidator {
    private AtlassianModuleValidator() {
    }

    private static final String SEPARATOR = ":";

    public static List<ValidationProblem> validate(@NotNull final AtlassianModuleProperties moduleProperties) {
        final List<ValidationProblem> errors = new ArrayList<>();

        final String key = moduleProperties.getCompleteModuleKey();

        if (StringUtils.contains(key, SEPARATOR)) {
            final String pluginKey = pluginKeyFromCompleteKey(key);
            final String moduleKey = moduleKeyFromCompleteKey(key);

            if (StringUtils.isBlank(pluginKey)) {
                errors.add(new ValidationProblem("Plugin key part of complete module key can not be empty."));
            }
            if (StringUtils.isBlank(moduleKey)) {
                errors.add(new ValidationProblem("Module key part of complete module key can not be empty."));
            }
        } else {
            errors.add(new ValidationProblem("Plugin module key must contain ':' separator."));
        }

        return errors;
    }

    private static String pluginKeyFromCompleteKey(final String key) {
        if (key != null) {
            return key.split(SEPARATOR)[0];
        }
        return "";
    }

    private static String moduleKeyFromCompleteKey(final String key) {
        if (key != null) {
            final String[] split = key.split(SEPARATOR, 2);
            if (split.length == 2) {
                return split[1];
            }
        }
        return "";
    }
}
