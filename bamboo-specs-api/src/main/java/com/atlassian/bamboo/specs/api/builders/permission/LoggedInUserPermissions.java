package com.atlassian.bamboo.specs.api.builders.permission;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.model.permission.LoggedInUserPermissionsProperties;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

public class LoggedInUserPermissions extends EntityPropertiesBuilder<LoggedInUserPermissionsProperties> {

    private final Set<PermissionType> permissionTypes = new LinkedHashSet<>();

    public LoggedInUserPermissions permissions(final PermissionType... permissionTypes) {
        Arrays.stream(permissionTypes).forEach(this.permissionTypes::add);
        return this;
    }

    @Override
    protected LoggedInUserPermissionsProperties build() {
        return new LoggedInUserPermissionsProperties(permissionTypes);
    }

}
