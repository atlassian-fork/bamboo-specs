package com.atlassian.bamboo.specs.api.model.plan.dependencies;


import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.model.plan.PlanIdentifierProperties;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;

import javax.annotation.concurrent.Immutable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Immutable
public class DependenciesProperties implements EntityProperties {
    private final DependenciesConfigurationProperties dependenciesConfigurationProperties;
    private final List<PlanIdentifierProperties> childPlans;

    protected DependenciesProperties() {
        dependenciesConfigurationProperties = new DependenciesConfigurationProperties();
        childPlans = new ArrayList<>();
    }

    public DependenciesProperties(DependenciesConfigurationProperties dependenciesConfigurationProperties, List<PlanIdentifierProperties> childPlans) {
        this.dependenciesConfigurationProperties = dependenciesConfigurationProperties;
        this.childPlans = Collections.unmodifiableList(new ArrayList<>(childPlans));
        validate();
    }

    public DependenciesConfigurationProperties getDependenciesConfigurationProperties() {
        return dependenciesConfigurationProperties;
    }

    public List<PlanIdentifierProperties> getChildPlans() {
        return childPlans;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DependenciesProperties that = (DependenciesProperties) o;
        return Objects.equals(dependenciesConfigurationProperties, that.dependenciesConfigurationProperties)
                && Objects.equals(childPlans, that.childPlans);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dependenciesConfigurationProperties, childPlans);
    }

    @Override
    public void validate() {
        ImporterUtils.checkNotNull("dependenciesConfigurationProperties", dependenciesConfigurationProperties);
        ImporterUtils.checkNotNull("childPlans", childPlans);
    }
}
