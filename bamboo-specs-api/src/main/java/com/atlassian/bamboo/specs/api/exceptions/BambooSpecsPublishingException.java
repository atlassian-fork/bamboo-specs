package com.atlassian.bamboo.specs.api.exceptions;

import com.atlassian.bamboo.specs.api.builders.RootEntityPropertiesBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Exception thrown for unsuccessful publishing of Bamboo Specs.
 */
public final class BambooSpecsPublishingException extends RuntimeException {

    /**
     * Known possible failure types.
     */
    public enum ErrorType {
        /**
         * Used whenever Bamboo was unreachable (server not found).
         */
        UNKNOWN_HOST,

        /**
         * Used when establishing connection with Bamboo was unsuccessful. May indicate invalid server URL (e.g. wrong port).
         */
        CONNECTION_ERROR,

        /**
         * Indicates that an error with network protocol was encountered while connecting to Bamboo, e.g. an unsupported
         * protocol was used instead of HTTP(S).
         */
        PROTOCOL_ERROR,

        /**
         * When the credentials used to contact Bamboo were invalid.
         */
        UNAUTHORIZED
    }

    @Nullable
    private final ErrorType errorType;
    @Nullable
    private final String debugMessage;

    /**
     * Creates an instance of the exception.
     *
     * @param entityProperties properties which caused the error
     * @param errorType        type of error (if known)
     * @param message          error message (if available)
     * @param debugMessage     additional message of the exception which should be displayed for debug logging (if available)
     * @param cause            underlying exception that caused the error (if available)
     */
    public BambooSpecsPublishingException(@NotNull RootEntityPropertiesBuilder<?> entityProperties,
                                          @Nullable ErrorType errorType,
                                          @Nullable String message,
                                          @Nullable String debugMessage,
                                          @Nullable Throwable cause) {
        super(formatErrorMessage(entityProperties, message), cause);
        this.errorType = errorType;
        this.debugMessage = debugMessage;
    }

    /**
     * Get the type of encountered error or {@code null} if unknown.
     */
    @Nullable
    public ErrorType getErrorType() {
        return errorType;
    }

    /**
     * Returns the message of this exception.
     */
    @NotNull
    public String getMessage() {
        return super.getMessage();
    }

    /**
     * Returns additional message for debugging, {@code null} if not available.
     */
    @Nullable
    public String getDebugMessage() {
        return debugMessage;
    }

    /**
     * Returns the cause of this throwable or {@code null} if the cause is nonexistent or unknown.
     */
    @Nullable
    public Throwable getCause() {
        return super.getCause();
    }

    @NotNull
    private static String formatErrorMessage(@NotNull RootEntityPropertiesBuilder<?> entityProperties, @Nullable String message) {
        return message != null
                ? String.format("An error occurred while publishing %s: %s", entityProperties.humanReadableId(), message)
                : String.format("An error occurred while publishing %s", entityProperties.humanReadableId());
    }
}
