package com.atlassian.bamboo.specs.api.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ReleaseNamingHelper {
    private ReleaseNamingHelper() {

    }

    public static class SplitVersionName {
        private String prefix;
        private Long number;
        private String suffix;

        public SplitVersionName(String prefix, String number, String suffix) {
            this.prefix = prefix;
            this.number = number != null ? Long.parseLong(number) : null;
            this.suffix = suffix;
        }

        @Nullable
        public String getPrefix() {
            return prefix;
        }

        @Nullable
        public Long getNumberToIncrement() {
            return number;
        }

        @Nullable
        public String getSuffix() {
            return suffix;
        }
    }

    public static SplitVersionName splitVersionName(@NotNull String versionName) {
        char[] originalChars = versionName.toCharArray();

        boolean inVariable = false;
        boolean inNumber = false;
        int numberEndIndex = -1;
        int numberStartIndex = 0;

        for (int i = originalChars.length - 1; i >= 0; i--) {
            char c = originalChars[i];
            if (c >= '0' && c <= '9') {
                if (!inVariable && !inNumber) {
                    inNumber = true;
                    numberEndIndex = i;
                }
            } else {
                if (inNumber) {
                    numberStartIndex = i + 1; // technically c is the letter before number, so have to +1
                    break;
                }

                if (c == '}') {
                    inVariable = true;
                } else if (c == '{') {
                    inVariable = false;
                }
            }
        }

        if (numberEndIndex >= 0) {
            String oldVersionNumber = versionName.substring(numberStartIndex, numberEndIndex + 1);
            return new SplitVersionName(versionName.substring(0, numberStartIndex), oldVersionNumber, versionName.substring(numberEndIndex + 1));
        }

        // no numbers to increment lets just return original name
        return new SplitVersionName(versionName, null, null);
    }
}
