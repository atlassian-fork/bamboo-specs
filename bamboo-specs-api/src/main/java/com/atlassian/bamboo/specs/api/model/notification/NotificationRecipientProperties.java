package com.atlassian.bamboo.specs.api.model.notification;

import com.atlassian.bamboo.specs.api.builders.Applicability;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;
import java.util.EnumSet;
import java.util.Set;

@Immutable
public abstract class NotificationRecipientProperties implements EntityProperties {
    @NotNull
    public abstract AtlassianModuleProperties getAtlassianPlugin();

    /**
     * @return determines if notification recipient is applicable to {@link Applicability#PLANS} or {@link Applicability#DEPLOYMENTS}.
     */
    public Set<Applicability> applicableTo() {
        return EnumSet.allOf(Applicability.class);
    }


}
