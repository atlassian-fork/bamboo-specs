package com.atlassian.bamboo.specs.api.codegen;

public class CodeGenerationException extends Exception {
    public CodeGenerationException(final String s) {
        super(s);
    }

    public CodeGenerationException(final String s, final Throwable throwable) {
        super(s, throwable);
    }
}
