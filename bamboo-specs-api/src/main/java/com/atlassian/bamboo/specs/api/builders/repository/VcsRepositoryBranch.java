package com.atlassian.bamboo.specs.api.builders.repository;


import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryBranchProperties;
import org.jetbrains.annotations.NotNull;

/**
 * Defines a branch a repository should be switched to.
 */
public class VcsRepositoryBranch extends EntityPropertiesBuilder<VcsRepositoryBranchProperties> {
    private String repositoryName;
    private String branchName;
    private String branchDisplayName;

    /**
     * Defines a branch a repository should be switched to. The repository has to be
     * connected to the plan: see {@link Plan#linkedRepositories(String...)} and {@link Plan#planRepositories(VcsRepository[])}
     *
     * @param repositoryName name of the repository
     * @param branchName     name of the branch
     */
    public VcsRepositoryBranch(@NotNull String repositoryName, @NotNull String branchName) {
        this.repositoryName = repositoryName;
        this.branchName = branchName;
    }

    /**
     * Specifies a branch.
     */
    public VcsRepositoryBranch branchName(@NotNull String branchName) {
        this.branchName = branchName;
        return this;
    }

    /**
     * Specifies display name for the branch. By default, it is equal to the branch name and does not need to be specified.
     * <p>
     * Some vcs configurations can result in branch name being relatively long.
     * For instance, in case of Subversion, branch name contains a relative path of the branch. In such cases showing full branch name
     * in the UI is not practical, hence the separate parameter for the display name.
     * </p>
     */
    public VcsRepositoryBranch branchDisplayName(@NotNull String branchDisplayName) {
        this.branchDisplayName = branchDisplayName;
        return this;
    }

    @Override
    protected VcsRepositoryBranchProperties build() throws PropertiesValidationException {
        return new VcsRepositoryBranchProperties(repositoryName, branchName, branchDisplayName);
    }
}
