package com.atlassian.bamboo.specs.api.builders.deployment;

import com.atlassian.bamboo.specs.api.builders.BambooOid;
import com.atlassian.bamboo.specs.api.builders.RootEntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.model.deployment.DeploymentProperties;
import com.atlassian.bamboo.specs.api.model.deployment.EnvironmentProperties;
import com.atlassian.bamboo.specs.api.model.deployment.ReleaseNamingProperties;
import com.atlassian.bamboo.specs.api.model.plan.PlanIdentifierProperties;
import com.atlassian.bamboo.specs.api.rsbs.RepositoryStoredSpecsData;
import com.atlassian.bamboo.specs.api.rsbs.RunnerSettings;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotBlank;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;
import static org.apache.commons.lang3.StringUtils.defaultString;

/**
 * Represents a deployment project.
 */
public class Deployment extends RootEntityPropertiesBuilder<DeploymentProperties> {

    public static final String TYPE = "deployment";

    private BambooOidProperties oid;
    private PlanIdentifierProperties plan;
    private String name;
    private String description;
    private ReleaseNamingProperties releaseNaming;
    private List<EnvironmentProperties> environments = new ArrayList<>();

    /**
     * Specifies a deployment project.
     *
     * @param plan plan that serves as source of artifacts and variables for this deployment project
     * @param name name of the project
     */
    public Deployment(@NotNull PlanIdentifier plan, @NotNull String name) {
        checkNotNull("plan", plan);
        checkNotBlank("name", name);

        this.plan = EntityPropertiesBuilders.build(plan);
        this.name = name;
    }

    /**
     * Sets the deployment name.
     */
    public Deployment name(@NotNull String name) throws PropertiesValidationException {
        checkNotBlank("name", name);
        this.name = name;
        return this;
    }

    /**
     * Sets the deployments's oid.
     * <p>
     * If set, it is used to identify the deployment project.
     * If a deployment with specified oid does not exist, a new one is created, otherwise it is updated.
     */
    public Deployment oid(@Nullable String oid) throws PropertiesValidationException {
        return oid(oid != null ? new BambooOid(oid) : null);
    }

    /**
     * Sets the deployments's oid.
     * <p>
     * If set, it is used to identify the deployment project.
     * If a deployment with specified oid does not exist, a new one is created, otherwise it is updated.
     */
    public Deployment oid(@Nullable BambooOid oid) throws PropertiesValidationException {
        this.oid = oid != null ? EntityPropertiesBuilders.build(oid) : null;
        return this;
    }

    /**
     * Sets the deployment description.
     */
    public Deployment description(String description) {
        this.description = description;
        return this;
    }

    public Deployment releaseNaming(@NotNull ReleaseNaming releaseNaming) {
        checkNotNull("releaseNaming", releaseNaming);
        this.releaseNaming = EntityPropertiesBuilders.build(releaseNaming);
        return this;
    }

    /**
     * Adds the environments to the end of environments list. The order of environments is preserved.
     */
    public Deployment environments(@NotNull Environment... environments) {
        checkNotNull("environments", environments);
        Arrays.stream(environments)
                .map(EntityPropertiesBuilders::build)
                .forEach(this.environments::add);
        return this;
    }

    protected DeploymentProperties build() {
        final RepositoryStoredSpecsData repositoryStoredSpecsData = RunnerSettings.getRepositoryStoredSpecsData();
        return new DeploymentProperties(oid, plan, name, description, releaseNaming, environments, repositoryStoredSpecsData);
    }

    @Override
    public String humanReadableType() {
        return TYPE;
    }

    @Override
    public String humanReadableId() {
        return String.format("%s %s",  TYPE, defaultString(name, "<unknown>"));
    }

    public BambooOidProperties getOid() {
        return oid;
    }

    public String getName() {
        return name;
    }
}
