package com.atlassian.bamboo.specs.api.model.plan;

import com.atlassian.bamboo.specs.api.codegen.annotations.CodeGeneratorName;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.BambooKeyProperties;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

@CodeGeneratorName("com.atlassian.bamboo.specs.codegen.emitters.plan.PlanIdentifierEmitter")
@Immutable
public class PlanIdentifierProperties extends AbstractPlanIdentifierProperties {
    private BambooKeyProperties projectKey;

    private PlanIdentifierProperties() {
    }

    public PlanIdentifierProperties(final BambooKeyProperties projectKey, final BambooKeyProperties key, final BambooOidProperties oid) {
        super(key, oid);
        this.projectKey = projectKey;
        validate();
    }

    public BambooKeyProperties getProjectKey() {
        return projectKey;
    }

    public boolean isFullKeyDefined() {
        return key != null && projectKey != null;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final PlanIdentifierProperties that = (PlanIdentifierProperties) o;
        return Objects.equals(getProjectKey(), that.getProjectKey());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getProjectKey());
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.NO_CLASS_NAME_STYLE)
                .append("oid", getOid())
                .append("key", getKey())
                .append("projectKey", projectKey)
                .build();
    }


    @Override
    public void validate() {
        if (!isOidDefined() && !isFullKeyDefined()) {
            throw new PropertiesValidationException("Either full key and project key or oid need to be defined when referencing plan");
        }
    }
}
