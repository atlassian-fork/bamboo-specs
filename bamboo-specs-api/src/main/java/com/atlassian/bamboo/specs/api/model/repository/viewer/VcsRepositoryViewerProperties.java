package com.atlassian.bamboo.specs.api.model.repository.viewer;

import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;

@Immutable
public interface VcsRepositoryViewerProperties extends EntityProperties {
    @NotNull
    AtlassianModuleProperties getAtlassianPlugin();
}
