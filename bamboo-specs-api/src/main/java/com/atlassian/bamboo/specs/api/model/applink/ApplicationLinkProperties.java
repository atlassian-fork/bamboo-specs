package com.atlassian.bamboo.specs.api.model.applink;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

public class ApplicationLinkProperties implements EntityProperties {
    private final String name;
    private final String id;

    private ApplicationLinkProperties() {
        name = null;
        id = null;
    }

    public ApplicationLinkProperties(@Nullable final String name, @Nullable final String id) {
        this.name = name;
        this.id = id;
        validate();
    }

    public boolean isNameDefined() {
        return StringUtils.isNotBlank(name);
    }

    public boolean isIdDefined() {
        return StringUtils.isNotBlank(id);
    }

    @Nullable
    public String getName() {
        return name;
    }

    @Nullable
    public String getId() {
        return id;
    }


    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ApplicationLinkProperties that = (ApplicationLinkProperties) o;
        return Objects.equals(getName(), that.getName()) &&
                Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getId());
    }

    @Override
    public String toString() {
        return "ApplicationLinkProperties{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                '}';
    }

    @Override
    public void validate() {
        if (StringUtils.isBlank(name) && StringUtils.isBlank(id)) {
            throw new PropertiesValidationException("Either application name or id need to be defined when referencing an external application");
        }
    }
}
