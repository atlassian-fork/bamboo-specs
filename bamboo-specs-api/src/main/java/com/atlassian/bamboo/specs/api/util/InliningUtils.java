package com.atlassian.bamboo.specs.api.util;

import org.jetbrains.annotations.NotNull;

/**
 * Utilities for preventing inlining of constants.
 * <p>
 * Methods from this class should be used whenever String or primitive type constants are exposed as part of the Specs
 * API, to prevent them from being inlined during compilation.
 */
public final class InliningUtils {
    private InliningUtils() {
    }

    /**
     * Prevents inlining of a String value.
     *
     * @param string any string
     * @return the string passed as argument
     */
    @NotNull
    public static String preventInlining(@NotNull String string) {
        return string;
    }

    /**
     * Prevents inlining of a boolean value.
     *
     * @param bool any boolean
     * @return the boolean passed as argument
     */
    public static boolean preventInlining(boolean bool) {
        return bool;
    }

    /**
     * Prevents inlining of an integer value.
     *
     * @param integer any integer
     * @return the integer passed as argument
     */
    public static int preventInlining(int integer) {
        return integer;
    }
}
