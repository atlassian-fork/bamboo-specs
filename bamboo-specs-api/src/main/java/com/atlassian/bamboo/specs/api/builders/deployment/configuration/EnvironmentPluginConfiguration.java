package com.atlassian.bamboo.specs.api.builders.deployment.configuration;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.model.deployment.configuration.EnvironmentPluginConfigurationProperties;
import org.jetbrains.annotations.NotNull;

public abstract class EnvironmentPluginConfiguration<T extends EnvironmentPluginConfigurationProperties> extends EntityPropertiesBuilder<T> {
    @NotNull
    @Override
    protected abstract T build();
}
