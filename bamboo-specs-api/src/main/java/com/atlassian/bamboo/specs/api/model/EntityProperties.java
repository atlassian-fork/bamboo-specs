package com.atlassian.bamboo.specs.api.model;


import javax.annotation.concurrent.Immutable;

@Immutable
public interface EntityProperties {
    void validate();
}
