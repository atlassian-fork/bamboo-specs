package com.atlassian.bamboo.specs.api.codegen.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to mark fields which hold sensitive information, like passwords or SSH keys. Emitted code won't contain
 * sensitive information but will include method reference with an code comment. Effectively code won't be compiling
 * in order to prevent user from accidentally breaking the plan/deployment definitions.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.FIELD})
public @interface Secret {

}
