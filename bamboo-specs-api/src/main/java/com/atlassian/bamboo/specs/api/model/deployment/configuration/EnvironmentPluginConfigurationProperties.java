package com.atlassian.bamboo.specs.api.model.deployment.configuration;

import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.EntityProperties;

public interface EnvironmentPluginConfigurationProperties extends EntityProperties {
    AtlassianModuleProperties getAtlassianPlugin();
}
