package com.atlassian.bamboo.specs.api.model;


import com.atlassian.bamboo.specs.api.codegen.annotations.ConstructFrom;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.validators.BambooKeyValidator;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNoErrors;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

@ConstructFrom("key")
@Immutable
public final class BambooKeyProperties implements EntityProperties {
    private String key;

    private BambooKeyProperties() {
    }

    public BambooKeyProperties(@NotNull final String key) throws PropertiesValidationException {
        this.key = key;

        validate();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BambooKeyProperties that = (BambooKeyProperties) o;
        return Objects.equals(getKey(), that.getKey());
    }

    @Override
    public String toString() {
        return key;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getKey());
    }

    @NotNull
    public String getKey() {
        return key;
    }

    @Override
    public void validate() {
        checkNotNull("key", key);
        checkNoErrors(BambooKeyValidator.validate(ValidationContext.of("Key"), this));
    }
}
