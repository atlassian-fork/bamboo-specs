package com.atlassian.bamboo.specs.api.model.permission;

import com.atlassian.bamboo.specs.api.builders.permission.Permissions;
import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.codegen.annotations.ConstructFrom;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.model.plan.PlanIdentifierProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.api.validators.permission.PermissionValidator;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNoErrors;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;


@ConstructFrom("planIdentifier")
@Immutable
public class PlanPermissionsProperties implements EntityProperties {

    private final PlanIdentifierProperties planIdentifier;
    private final PermissionsProperties permissions;

    private PlanPermissionsProperties() {
        this.planIdentifier = null;
        this.permissions = null;
    }

    public PlanPermissionsProperties(@NotNull final PlanIdentifier planIdentifier,
                                     @NotNull final Permissions permissions) throws PropertiesValidationException {
        this.planIdentifier = EntityPropertiesBuilders.build(planIdentifier);
        this.permissions = EntityPropertiesBuilders.build(permissions);
        validate();
    }

    @Override
    public void validate() throws PropertiesValidationException {
        checkNotNull("planIdentifier", planIdentifier);
        checkNotNull("permissions", permissions);
        checkNoErrors(PermissionValidator.validatePermissions(permissions, PermissionValidator.PermissionTarget.PLAN));
    }

    public PlanIdentifierProperties getPlanIdentifier() {
        return planIdentifier;
    }

    public PermissionsProperties getPermissions() {
        return permissions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlanPermissionsProperties that = (PlanPermissionsProperties) o;
        return Objects.equals(getPlanIdentifier(), that.getPlanIdentifier()) &&
                Objects.equals(getPermissions(), that.getPermissions());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPlanIdentifier(), getPermissions());
    }
}
