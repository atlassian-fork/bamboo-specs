package com.atlassian.bamboo.specs.api.codegen.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * If builder class has no default constructor this annotation should be used to list
 * fields that should be used to invoke constructor of the builder.
 * Fields must be listed in the order they are passed to the constructor.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ConstructFrom {
    String[] value();
}
