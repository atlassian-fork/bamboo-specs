package com.atlassian.bamboo.specs.api.validators.project;

import com.atlassian.bamboo.specs.api.model.project.ProjectProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateDescription;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateName;

public final class ProjectValidator {
    private ProjectValidator() {
    }

    @NotNull
    public static List<ValidationProblem> validate(@NotNull final ProjectProperties projectProperties) {
        final ValidationContext context = ValidationContext.of("Project");
        final List<ValidationProblem> errors = new ArrayList<>();

        if (projectProperties.getKey() == null && projectProperties.getOid() == null) {
            errors.add(new ValidationProblem("Either key or oid need to be defined for project"));
        }

        errors.addAll(validateName(context, projectProperties.getName(), false));
        errors.addAll(validateDescription(context, projectProperties.getDescription()));

        return errors;
    }
}
