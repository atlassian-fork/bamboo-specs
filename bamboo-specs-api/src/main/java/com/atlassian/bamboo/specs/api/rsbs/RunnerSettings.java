package com.atlassian.bamboo.specs.api.rsbs;

import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public final class RunnerSettings {
    private static Path yamlDir;
    private static boolean isRestEnabled = true;
    private static RepositoryStoredSpecsData repositoryStoredSpecsData;

    private RunnerSettings() {
    }

    public static void setRepositoryStoredSpecsData(final RepositoryStoredSpecsData repositoryStoredSpecsData) {
        RunnerSettings.repositoryStoredSpecsData = repositoryStoredSpecsData;
    }

    public static void setRestEnabled(final boolean value) {
        RunnerSettings.isRestEnabled = value;
    }

    public static void setYamlDir(final Path yamlDir) {
        RunnerSettings.yamlDir = yamlDir;

        try {
            Files.createDirectories(yamlDir);
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Nullable
    public static Path getYamlDir() {
        return yamlDir;
    }

    public static boolean isRestEnabled() {
        return isRestEnabled;
    }

    public static RepositoryStoredSpecsData getRepositoryStoredSpecsData() {
        return repositoryStoredSpecsData;
    }
}
