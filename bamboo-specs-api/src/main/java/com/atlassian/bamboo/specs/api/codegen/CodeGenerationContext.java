package com.atlassian.bamboo.specs.api.codegen;

import java.util.Set;
import java.util.TreeSet;

/**
 * Context for generating Bamboo Specs code.
 */
public class CodeGenerationContext {
    private int indentationLevel = 0;
    private final Set<String> imports = new TreeSet<>();

    /**
     * Increment code indentation.
     */
    public String incIndentation() {
        indentationLevel++;
        return "";
    }

    /**
     * Decrement code indentation.
     */
    public String decIndentation() {
        indentationLevel--;
        return "";
    }

    /**
     * Adds a class to the list of classes used in the generated code. The resulting
     * list is used to generate import statements
     *
     * @param clazz class object
     * @return class short name
     */
    public String importClassName(Class clazz) {
        if (clazz.isMemberClass()) {
            Class enclosingClass = clazz.getEnclosingClass();
            String enclosingClassName = importClassName(enclosingClass);
            return enclosingClassName + "." + clazz.getSimpleName();
        } else {
            imports.add(clazz.getCanonicalName());
            return clazz.getSimpleName();
        }
    }

    /**
     * Produces a string with new line followed by a number of spaces determined according to the current indentation
     * level.
     */
    public String newLine() {
        StringBuilder builder = new StringBuilder();
        builder.append("\n");
        for (int i = 0; i < indentationLevel; i++) {
            builder.append("    ");
        }
        return builder.toString();
    }

    /**
     * @return current indentation level.
     */
    public int getIndentationLevel() {
        return indentationLevel;
    }

    public Set<String> getImports() {
        return imports;
    }
}
