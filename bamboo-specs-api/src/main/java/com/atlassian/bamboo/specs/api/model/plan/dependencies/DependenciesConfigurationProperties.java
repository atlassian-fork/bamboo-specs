package com.atlassian.bamboo.specs.api.model.plan.dependencies;


import com.atlassian.bamboo.specs.api.builders.plan.dependencies.DependenciesConfiguration.DependencyBlockingStrategy;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkRequired;

@Immutable
public final class DependenciesConfigurationProperties implements EntityProperties {

    private final boolean enabledForBranches;
    private final boolean requireAllStagesPassing;
    private final DependencyBlockingStrategy blockingStrategy;

    protected DependenciesConfigurationProperties() {
        enabledForBranches = true;
        requireAllStagesPassing = false;
        blockingStrategy = DependencyBlockingStrategy.NONE;
    }

    public DependenciesConfigurationProperties(final boolean enabledForBranches,
                                               final boolean requireAllStagesPassing,
                                               final DependencyBlockingStrategy blockingStrategy) throws PropertiesValidationException {
        this.enabledForBranches = enabledForBranches;
        this.requireAllStagesPassing = requireAllStagesPassing;
        this.blockingStrategy = blockingStrategy;

        validate();
    }

    public boolean isEnabledForBranches() {
        return enabledForBranches;
    }

    public boolean isRequireAllStagesPassing() {
        return requireAllStagesPassing;
    }

    public DependencyBlockingStrategy getBlockingStrategy() {
        return blockingStrategy;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DependenciesConfigurationProperties that = (DependenciesConfigurationProperties) o;
        return isEnabledForBranches() == that.isEnabledForBranches() &&
                isRequireAllStagesPassing() == that.isRequireAllStagesPassing() &&
                getBlockingStrategy() == that.getBlockingStrategy();
    }

    @Override
    public int hashCode() {
        return Objects.hash(isEnabledForBranches(), isRequireAllStagesPassing(), getBlockingStrategy());
    }

    @Override
    public void validate() {
        final ValidationContext context = ValidationContext.of("Dependencies configuration");
        checkRequired(context.with("blockingStrategy"), blockingStrategy);
    }
}
