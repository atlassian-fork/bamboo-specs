package com.atlassian.bamboo.specs.api.builders;

public enum Applicability {
    PLANS, DEPLOYMENTS
}
