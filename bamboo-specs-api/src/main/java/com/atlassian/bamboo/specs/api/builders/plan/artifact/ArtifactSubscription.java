package com.atlassian.bamboo.specs.api.builders.plan.artifact;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.plan.artifact.ArtifactSubscriptionProperties;
import org.jetbrains.annotations.NotNull;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotBlank;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents an artifact subscription.
 * <p>
 * Artifact subscriptions define jobs that use an artifact.
 */
public class ArtifactSubscription extends EntityPropertiesBuilder<ArtifactSubscriptionProperties> {
    private String artifactName;
    private String destination;

    /**
     * Specify an artifact subscription.
     */
    public ArtifactSubscription() {
    }

    /**
     * Sets name of the artifact to download. Artifact must be defined in one of the preceding stages.
     */
    public ArtifactSubscription artifact(@NotNull String artifactName) {
        checkNotBlank("artifactName", artifactName);
        this.artifactName = artifactName;
        return this;
    }

    /**
     * Sets artifact download destination. Empty by default.
     *
     * @param destination to download artifact to. Can be both relative and absolute path.
     */
    public ArtifactSubscription destination(@NotNull String destination) throws PropertiesValidationException {
        checkNotNull("destination", destination);
        this.destination = destination;
        return this;
    }

    protected ArtifactSubscriptionProperties build() {
        return new ArtifactSubscriptionProperties(artifactName, destination);
    }
}
