package com.atlassian.bamboo.specs.api.model.plan;

import com.atlassian.bamboo.specs.api.codegen.annotations.ConstructFrom;
import com.atlassian.bamboo.specs.api.codegen.annotations.Setter;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.validators.plan.StageValidator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNoErrors;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

@ConstructFrom({"name"})
@Immutable
public final class StageProperties implements EntityProperties {
    private final String name;
    private final String description;
    @Setter("manual")
    private final boolean manualStage;
    private final boolean finalStage;

    private List<JobProperties> jobs;

    private StageProperties() {
        name = null;
        description = "";
        manualStage = false;
        finalStage = false;
        jobs = Collections.emptyList();
    }

    public StageProperties(final String name,
                           final String description,
                           final boolean manualStage,
                           final boolean finalStage,
                           final List<JobProperties> jobs) throws PropertiesValidationException {
        this.name = name;
        this.description = description;
        this.manualStage = manualStage;
        this.finalStage = finalStage;
        this.jobs = Collections.unmodifiableList(new ArrayList<>(jobs));

        validate();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StageProperties that = (StageProperties) o;
        return isManual() == that.isManual() &&
                isFinalStage() == that.isFinalStage() &&
                Objects.equals(getName(), that.getName()) &&
                Objects.equals(getDescription(), that.getDescription()) &&
                Objects.equals(getJobs(), that.getJobs());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getDescription(), isManual(), isFinalStage(), getJobs());
    }

    @NotNull
    public String getName() {
        return name;
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    public boolean isManual() {
        return manualStage;
    }

    public boolean isFinalStage() {
        return finalStage;
    }

    @NotNull
    public List<JobProperties> getJobs() {
        return jobs;
    }

    @Override
    public void validate() {
        checkNotNull("name", name);
        checkNotNull("jobs", jobs);

        checkNoErrors(StageValidator.validate(this));
    }
}
