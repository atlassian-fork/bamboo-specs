package com.atlassian.bamboo.specs.api.validators;

import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkArgument;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotBlank;

public final class CronExpressionClientSideValidator {
    /**
     * Full Cron validation will be performed by quartz on Bamboo side, here we just check if the format <i>seems</i>
     * like a valid expression.
     */
    private static final String CRON_FORMAT_REGEXP = "(.*\\s){5}(.*)(\\s.*)?";

    private CronExpressionClientSideValidator() {
    }

    public static void validate(String cronExpression) {
        checkNotBlank("cronExpression", cronExpression);
        checkArgument(ValidationContext.empty(), cronExpression.matches(CRON_FORMAT_REGEXP),
                cronExpression + " doesn't seem like a valid cron expression");
    }
}
