package com.atlassian.bamboo.specs.api.builders.permission;

import com.atlassian.bamboo.specs.api.builders.BambooOid;
import com.atlassian.bamboo.specs.api.builders.RootEntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.builders.deployment.Deployment;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.model.permission.DeploymentPermissionsProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.jetbrains.annotations.NotNull;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotBlank;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Entity representing permissions for deployment projects.
 * Note that this object needs to be published separately from associated {@link Deployment}.
 * Pre-existing permissions that are not defined in associated {@link Permissions} object are <b>revoked</b> when this object is published.
 */
public class DeploymentPermissions extends RootEntityPropertiesBuilder<DeploymentPermissionsProperties> {
    public static final String TYPE = "deployment permission";

    private BambooOidProperties deploymentOid;
    private String deploymentName;
    private Permissions permissions;

    @Override
    protected DeploymentPermissionsProperties build() {
        if (deploymentOid != null) {
            return new DeploymentPermissionsProperties(deploymentOid, EntityPropertiesBuilders.build(permissions));
        } else {
            return new DeploymentPermissionsProperties(deploymentName, EntityPropertiesBuilders.build(permissions));
        }
    }

    public DeploymentPermissions(@NotNull final BambooOid deploymentOid) {
        this(EntityPropertiesBuilders.build(checkNotNull("deploymentOid", deploymentOid)));
    }

    public DeploymentPermissions(@NotNull final BambooOidProperties deploymentOid) {
        checkNotNull("deploymentOid", deploymentOid);
        this.deploymentOid = deploymentOid;
    }

    public DeploymentPermissions(final String deploymentProjectName) {
        checkNotBlank("deployment project name", deploymentProjectName);
        this.deploymentName = deploymentProjectName;
    }

    public DeploymentPermissions permissions(final Permissions permissions) {
        checkNotNull("permissions", permissions);
        this.permissions = permissions;
        return this;
    }

    public String getDeploymentName() {
        return deploymentName;
    }

    public static DeploymentPermissions createForDeployment(@NotNull final Deployment deployment) {
        if (deployment.getOid() != null) {
            return new DeploymentPermissions(deployment.getOid());
        } else {
            return new DeploymentPermissions(deployment.getName());
        }
    }

    @Override
    public String humanReadableType() {
        return TYPE;
    }

    @Override
    public String humanReadableId() {
        final StringBuilder id = new StringBuilder();
        id.append(TYPE);
        id.append(" for deployment ");
        if (deploymentOid != null) {
            id.append(deploymentOid);
        } else {
            id.append(deploymentName);
        }
        return id.toString();
    }

}
