/**
 * The AnyTask class for handling tasks unsupported by Bamboo Specs.
 */
package com.atlassian.bamboo.specs.api.model.task;
