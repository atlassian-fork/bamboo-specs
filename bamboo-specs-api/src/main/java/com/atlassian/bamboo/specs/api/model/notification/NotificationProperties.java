package com.atlassian.bamboo.specs.api.model.notification;

import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;

import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkThat;

@Immutable
public class NotificationProperties implements EntityProperties {

    private final NotificationTypeProperties type;
    private final List<NotificationRecipientProperties> recipients;

    protected NotificationProperties() {
        type = null;
        recipients = Collections.emptyList();
    }

    public NotificationProperties(NotificationTypeProperties type, List<NotificationRecipientProperties> recipients) {
        this.type = type;
        this.recipients = recipients;
        validate();
    }

    public NotificationTypeProperties getType() {
        return type;
    }

    public List<NotificationRecipientProperties> getRecipients() {
        return recipients;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NotificationProperties that = (NotificationProperties) o;
        return Objects.equals(getType(), that.getType()) &&
                Objects.equals(getRecipients(), that.getRecipients());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getType(), getRecipients());
    }

    @Override
    public void validate() {
        final ValidationContext context = ValidationContext.of("Notification");
        checkNotNull(context.with("type"), "type", type);
        checkThat(context.with("recipients"), recipients != null && !recipients.isEmpty(),
                "Recipients of the notification not defined", recipients);
    }
}
