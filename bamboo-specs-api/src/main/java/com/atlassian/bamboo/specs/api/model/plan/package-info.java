/**
 * Plan, plan branches, stages and jobs.
 */
package com.atlassian.bamboo.specs.api.model.plan;
