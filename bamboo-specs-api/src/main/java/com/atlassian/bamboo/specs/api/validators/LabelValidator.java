package com.atlassian.bamboo.specs.api.validators;

import com.atlassian.bamboo.specs.api.validators.common.BambooStringUtils;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.specs.api.validators.common.ValidationUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class LabelValidator {
    private static final char[] INVALID_LABEL_CHARACTERS = {':', ';', ',', '.', ' ', '\t', '\n', '\r', '\f', '?', '&', '[', ']', '(', ')', '#', '^', '*', '@', '!'};

    public static final char[] INVALID_CHARACTERS = ArrayUtils.addAll(INVALID_LABEL_CHARACTERS, BambooStringUtils.XSS_RELATED_CHARACTERS);
    // pattern for INVALID_CHARACTERS
    public static final String INVALID_CHARACTERS_FIND_EXPR = "[:;,\\.\\?&\\[\\]\\(\\)#\\^\\*@!\\s" + BambooStringUtils.XSS_RELATED_CHARACTERS_FIND_STR + "]";

    private LabelValidator() {
    }

    public static Optional<ValidationProblem> validateLabel(@Nullable String label) {
        return validateLabel(ValidationContext.empty(), label);
    }

    public static Optional<ValidationProblem> validateLabel(@NotNull ValidationContext validationContext, @Nullable String label) {
        return Optional.ofNullable(
                ValidationUtils.validateRequiredString(validationContext, label, true)
                        .orElseGet(() -> StringUtils.containsAny(label, INVALID_CHARACTERS) ?
                                new ValidationProblem(validationContext.with("labels"),
                                        "Label %s contains invalid character: %s",
                                        label, findFirstInvalidCharacter(label)) :
                                null));
    }

    private static String findFirstInvalidCharacter(@Nullable String label) {
        final int indexOfAny = StringUtils.indexOfAny(label, INVALID_CHARACTERS);
        return indexOfAny >= 0 ? StringUtils.substring(label, indexOfAny, indexOfAny + 1) : StringUtils.EMPTY;
    }
}
