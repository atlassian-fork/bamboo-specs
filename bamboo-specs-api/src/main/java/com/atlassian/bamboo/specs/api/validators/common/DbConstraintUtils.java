package com.atlassian.bamboo.specs.api.validators.common;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class DbConstraintUtils {
    //default bamboo limit for medium size varchar
    public static final int DATABASE_STRING_LIMIT = 255;

    private DbConstraintUtils() {
    }

    public static Optional<ValidationProblem> checkLength(@NotNull ValidationContext validationContext,
                                                          @Nullable final String input,
                                                          int length,
                                                          String errorMessage) {
        return Optional.of(errorMessage)
                .filter(message -> StringUtils.length(input) > length)
                .map(message -> new ValidationProblem(validationContext, message));
    }
}
