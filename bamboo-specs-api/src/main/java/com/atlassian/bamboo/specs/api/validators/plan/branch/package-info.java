/**
 * Validate configuration of plan branches.
 */
package com.atlassian.bamboo.specs.api.validators.plan.branch;
