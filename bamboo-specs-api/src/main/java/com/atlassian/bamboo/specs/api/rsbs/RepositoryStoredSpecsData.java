package com.atlassian.bamboo.specs.api.rsbs;


public class RepositoryStoredSpecsData {
    private long specsSourceId;

    private RepositoryStoredSpecsData() {
    }

    public RepositoryStoredSpecsData(final long specsSourceId) {
        this.specsSourceId = specsSourceId;
    }

    public long getSpecsSourceId() {
        return specsSourceId;
    }
}
