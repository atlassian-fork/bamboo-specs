/**
 * Plan dependencies (so that build of one plan may trigger builds of another plans).
 */
package com.atlassian.bamboo.specs.api.model.plan.dependencies;
