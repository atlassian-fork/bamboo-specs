package com.atlassian.bamboo.specs.api.builders.plan.dependencies;


import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.plan.dependencies.DependenciesConfigurationProperties;
import org.jetbrains.annotations.NotNull;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents dependencies configuration.
 */
public class DependenciesConfiguration extends EntityPropertiesBuilder<DependenciesConfigurationProperties> {
    public enum DependencyBlockingStrategy {
        NONE, BLOCK_IF_PARENT_IN_PROGRESS, BLOCK_IF_PARENT_HAS_CHANGES
    }

    private boolean enabledForBranches = true;
    private boolean requireAllStagesPassing = false;
    private DependencyBlockingStrategy blockingStrategy = DependencyBlockingStrategy.NONE;

    /**
     * Enables/disables dependencies support for plan branches. Feature is off by default.
     */
    public DependenciesConfiguration enabledForBranches(boolean enabledForBranches) {
        this.enabledForBranches = enabledForBranches;
        return this;
    }

    /**
     * Controls whether it is required for all stages to be complete before triggering dependant plans.
     * It is only relevant if plan contains manual stages. Feature is off by default.
     */
    public DependenciesConfiguration requireAllStagesPassing(boolean requireAllStagesPassing) {
        this.requireAllStagesPassing = requireAllStagesPassing;
        return this;
    }

    /**
     * Selects dependency blocking strategy. Dependency blocking helps preventing from triggering dependant builds too often.
     * <p>Possible values:
     * <dl>
     * <dt>NONE</dt>
     * <dd>No dependency blocking. Dependant builds are triggered whenever this build finishes successfully.</dd>
     * <dt>BLOCK_IF_PARENT_IN_PROGRESS</dt>
     * <dd>Dependant builds are not triggered if another build of this plan is still running.</dd>
     * <dt>BLOCK_IF_PARENT_HAS_CHANGES</dt>
     * <dd>Dependant builds are not triggered if this plan has unbuilt changes.</dd>
     * </dl>
     */
    public DependenciesConfiguration blockingStrategy(@NotNull DependencyBlockingStrategy blockingStrategy) throws PropertiesValidationException {
        checkNotNull("blockingStrategy", blockingStrategy);
        this.blockingStrategy = blockingStrategy;
        return this;
    }

    protected DependenciesConfigurationProperties build() throws PropertiesValidationException {
        return new DependenciesConfigurationProperties(enabledForBranches, requireAllStagesPassing, blockingStrategy);
    }
}
