package com.atlassian.bamboo.specs.api.model.repository;

import com.atlassian.bamboo.specs.api.codegen.Condition;
import org.apache.commons.lang3.StringUtils;

public class SkipDisplayNameCondition implements Condition<VcsRepositoryBranchProperties> {
    @Override
    public boolean evaluate(VcsRepositoryBranchProperties properties) {
        return StringUtils.isBlank(properties.getBranchDisplayName())
                || properties.getBranchName().equals(properties.getBranchDisplayName());
    }
}
