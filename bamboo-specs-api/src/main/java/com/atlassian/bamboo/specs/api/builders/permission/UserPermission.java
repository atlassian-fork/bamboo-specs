package com.atlassian.bamboo.specs.api.builders.permission;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.model.permission.UserPermissionProperties;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

public class UserPermission extends EntityPropertiesBuilder<UserPermissionProperties> {

    private final String username;
    private final Set<PermissionType> permissionTypes = new LinkedHashSet<>();

    public UserPermission(final String username) {
        this.username = username;
    }

    public UserPermission permissions(final PermissionType... permissionTypes) {
        Arrays.stream(permissionTypes).forEach(this.permissionTypes::add);
        return this;
    }

    @Override
    protected UserPermissionProperties build() {
        return new UserPermissionProperties(username, permissionTypes);
    }
}
