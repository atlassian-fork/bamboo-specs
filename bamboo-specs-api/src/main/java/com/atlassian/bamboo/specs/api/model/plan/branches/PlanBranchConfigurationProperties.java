package com.atlassian.bamboo.specs.api.model.plan.branches;

import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkRequired;

public class PlanBranchConfigurationProperties implements EntityProperties {
    private boolean cleanupEnabled;
    private BranchIntegrationProperties branchIntegrationProperties;


    private PlanBranchConfigurationProperties() {
        cleanupEnabled = true;
    }

    public PlanBranchConfigurationProperties(@NotNull BranchIntegrationProperties branchIntegration, boolean cleanupEnabled) {
        this.branchIntegrationProperties = branchIntegration;
        this.cleanupEnabled = cleanupEnabled;
        validate();
    }

    public boolean isCleanupEnabled() {
        return cleanupEnabled;
    }

    @Nullable
    public BranchIntegrationProperties getBranchIntegrationProperties() {
        return branchIntegrationProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PlanBranchConfigurationProperties that = (PlanBranchConfigurationProperties) o;
        return  isCleanupEnabled() == that.isCleanupEnabled() &&
                Objects.equals(getBranchIntegrationProperties(), that.getBranchIntegrationProperties());
    }

    @Override
    public int hashCode() {
        return Objects.hash(isCleanupEnabled(), getBranchIntegrationProperties());
    }

    @Override
    public void validate() {
        final ValidationContext context = ValidationContext.of("Branch configuration");
        checkRequired(context.with("branchIntegrationProperties"), branchIntegrationProperties);
    }
}
