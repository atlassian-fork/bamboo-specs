package com.atlassian.bamboo.specs.api.codegen.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Custom Bamboo Specs generator for EntityProperties class or a field.
 * This annotation references class by its name rather than directly and should
 * To be used only in order to avoid dependency problems.
 * {@link CodeGenerator} annotation is recommended instead.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.FIELD})
public @interface CodeGeneratorName {
    String value();
}
