package com.atlassian.bamboo.specs.api.builders.label;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.model.label.LabelProperties;
import org.jetbrains.annotations.NotNull;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents a label which can be placed on various Bamboo entities such as plans.
 */
public class Label extends EntityPropertiesBuilder<LabelProperties> {
    private final String name;

    /**
     * Creates a label with given name.
     */
    public Label(@NotNull String name) {
        checkNotNull("name", name);
        this.name = name;
    }

    @Override
    protected LabelProperties build() {
        return new LabelProperties(name);
    }
}
