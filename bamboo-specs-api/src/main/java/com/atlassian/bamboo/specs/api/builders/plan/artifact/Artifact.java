package com.atlassian.bamboo.specs.api.builders.plan.artifact;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.plan.artifact.ArtifactProperties;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import static com.atlassian.bamboo.specs.api.util.InliningUtils.preventInlining;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents Bamboo artifact definition.
 */
public class Artifact extends EntityPropertiesBuilder<ArtifactProperties> {
    public static final boolean SHARED_BY_DEFAULT = preventInlining(false);
    public static final boolean REQUIRED_BY_DEFAULT = preventInlining(false);

    private String name;
    private String copyPattern;
    private String location = "";
    private boolean shared = SHARED_BY_DEFAULT;
    private boolean required = REQUIRED_BY_DEFAULT;

    public Artifact() {
    }

    /**
     * Specify an artifact with given name. Name must be unique within the job; if artifact is shared it must be unique within the plan.
     * <p>
     * In the absence of oid, the name is used to identify the artifact.
     * If the artifact with given name does not exist, a new one is created, otherwise existing one is updated.
     */
    public Artifact(@NotNull String name) throws PropertiesValidationException {
        validateName(name);
        this.name = name;
    }

    /**
     * Sets an artifact name. Name must be unique within the job; if artifact is shared it must be unique within the plan.
     * <p>
     * In the absence of oid, the name is used to identify the artifact.
     * If the artifact with given name does not exist, a new one is created, otherwise existing one is updated.
     */
    public Artifact name(@NotNull String name) throws PropertiesValidationException {
        validateName(name);
        this.name = name;
        return this;
    }

    /**
     * Specify the pattern according to which Bamboo should copy files when creating an actual artifact.
     *
     * @param copyPattern a copy pattern in Ant copy pattern format
     */
    public Artifact copyPattern(@NotNull String copyPattern) throws PropertiesValidationException {
        checkNotNull("copyPattern", copyPattern);
        this.copyPattern = copyPattern;
        return this;
    }

    /**
     * Specifies if artifact is shared. Shared artifacts can be downloaded by jobs in subsequent stages as well as other plans and deployments.
     * False by default.
     */
    public Artifact shared(boolean shared) {
        this.shared = shared;
        return this;
    }

    /**
     * Set artifact to be required. Build will fail if can't publish required artifact.
     */
    public Artifact required() {
        this.required = true;
        return this;
    }

    /**
     * Specifies if artifact is required. Build will fail if can't publish required artifact.
     * False by default.
     */
    public Artifact required(boolean mandatory) {
        this.required = mandatory;
        return this;
    }

    /**
     * Specifies a path in which Bamboo should look for the files when creating an actual artifact.
     */
    public Artifact location(@NotNull String location) throws PropertiesValidationException {
        checkNotNull("location", location);
        this.location = location;
        return this;
    }

    /**
     * Returns defined artifact name, which serves as identifier for this object.
     *
     * @throws IllegalStateException if name is undefined
     */
    @NotNull
    public String getName() {
        if (StringUtils.isBlank(name)) {
            throw new IllegalStateException("Artifact name is undefined");
        }
        return name;
    }

    protected ArtifactProperties build() throws PropertiesValidationException {
        return new ArtifactProperties(name, copyPattern, location, shared, required);
    }

    private void validateName(String name) {
        checkNotNull("name", name);
        ImporterUtils.checkArgument(ArtifactProperties.VALIDATION_CONTEXT,!StringUtils.contains(name,"/"),"Name can not contain '/' character");
    }
}
