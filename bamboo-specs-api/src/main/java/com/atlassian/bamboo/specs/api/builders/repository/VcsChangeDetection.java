package com.atlassian.bamboo.specs.api.builders.repository;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.repository.VcsChangeDetectionProperties;
import org.jetbrains.annotations.Nullable;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents change detection options that can be set when defining a VCS repository in Bamboo.
 * <p>
 * These option control features around change detection an build triggering, such as: filtering of changes, commit isolation, etc.
 */
public class VcsChangeDetection extends EntityPropertiesBuilder<VcsChangeDetectionProperties> {
    public enum FileFilteringOption {
        NONE, INCLUDE_ONLY, EXCLUDE_ALL
    }

    private boolean quietPeriodEnabled = false;
    private Duration quietPeriod = Duration.ofSeconds(10);
    private int maxRetries = 5;
    private boolean commitIsolationEnabled = false;
    private Map<String, Object> configuration = new HashMap<>();

    private String changesetFilterPatternRegex;
    private FileFilteringOption filterFilePatternOption = FileFilteringOption.NONE;
    private String filterFilePatternRegex;

    /**
     * Enables/disables quiet period feature on the repository.
     * <p>
     * Quiet period allows you to delay building after a single commit is detected, aggregating multiple commits per build.
     * Feature is disabled by default.
     */
    public VcsChangeDetection quietPeriodEnabled(boolean quietPeriodEnabled) {
        this.quietPeriodEnabled = quietPeriodEnabled;
        return this;
    }

    /**
     * Defines quiet period duration, that is time Bamboo should wait after a new change, before initiating a build.
     */
    public VcsChangeDetection quietPeriod(Duration quietPeriod) {
        this.quietPeriod = quietPeriod;
        return this;
    }

    /**
     * Defines quiet period duration in seconds, that is time Bamboo should wait after a new change, before initiating a build.
     */
    public VcsChangeDetection quietPeriodInSeconds(int quietPeriodInSeconds) {
        return quietPeriod(Duration.ofSeconds(quietPeriodInSeconds));
    }

    /**
     * Defines maximum retries count for quiet period, that is how many time Bamboo should check for new changes before
     * initiating a build regardless of the outcome.
     */
    public VcsChangeDetection quietPeriodMaxRetries(int maxRetries) {
        this.maxRetries = maxRetries;
        return this;
    }

    /**
     * Enables/disables commit isolation. Commit isolation forces Bamboo to create one build result for each commit.
     * Feature is disabled by default.
     */
    public VcsChangeDetection commitIsolationEnabled(boolean commitIsolationEnabled) {
        this.commitIsolationEnabled = commitIsolationEnabled;
        return this;
    }

    /**
     * Sets plugin specific custom configuration.
     * <p>
     * This fields exists for the sole purpose of future extensibility. As of Bamboo version 6.0, no plugins using this field exist.
     */
    public VcsChangeDetection configuration(@Nullable Map<String, Object> configuration) {
        this.configuration = configuration != null ? new HashMap<>(configuration) : new HashMap<>();
        return this;
    }

    /**
     * Excludes certain changes from being picked up by Bamboo.
     *
     * @param changesetFilterPatternRegex a regular expression to match the commit messages to be excluded.
     */
    public VcsChangeDetection changesetFilterPatternRegex(@Nullable String changesetFilterPatternRegex) throws PropertiesValidationException {
        this.changesetFilterPatternRegex = changesetFilterPatternRegex;
        return this;
    }

    /**
     * Selects method of filtering commits by affected files.
     * <p> Possible values:
     * <dl>
     * <dt>NONE</dt>
     * <dd>filtering off</dd>
     * <dt>INCLUDE_ONLY</dt>
     * <dd>include only commits that affect files with names that match regexp</dd>
     * <dt>EXCLUDE_ALL</dt>
     * <dd>ignore commits that affect files with names that match regexp</dd>
     * </dl>
     * By default, filtering is off.
     */
    public VcsChangeDetection filterFilePatternOption(@Nullable FileFilteringOption filterFilePatternOption) throws PropertiesValidationException {
        this.filterFilePatternOption = filterFilePatternOption;
        return this;
    }

    /**
     * Sets regular expression to be used when filtering commits by affected files.
     *
     * @param filterFilePatternRegex a regular expression to match the file to be included / excluded
     */
    public VcsChangeDetection filterFilePatternRegex(@Nullable String filterFilePatternRegex) throws PropertiesValidationException {
        this.filterFilePatternRegex = filterFilePatternRegex;
        return this;
    }

    protected VcsChangeDetectionProperties build() throws PropertiesValidationException {
        return new VcsChangeDetectionProperties(
                quietPeriodEnabled,
                quietPeriod,
                maxRetries,
                commitIsolationEnabled,
                configuration,
                changesetFilterPatternRegex,
                filterFilePatternOption,
                filterFilePatternRegex);
    }
}
