package com.atlassian.bamboo.specs.api.validators.plan;

import com.atlassian.bamboo.specs.api.model.plan.AbstractPlanProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateRequired;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateDescription;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateName;

public final class AbstractPlanValidator {
    private AbstractPlanValidator() {
    }

    @NotNull
    public static List<ValidationProblem> validate(@NotNull final AbstractPlanProperties abstractPlanProperties) {
        final ValidationContext context = ValidationContext.of("Plan or job");
        final List<ValidationProblem> errors = new ArrayList<>();

        errors.addAll(validateName(context, abstractPlanProperties.getName()));
        errors.addAll(validateDescription(context, abstractPlanProperties.getDescription()));
        validateRequired(context.with("key"), abstractPlanProperties.getKey())
                .ifPresent(errors::add);
        return errors;
    }
}
