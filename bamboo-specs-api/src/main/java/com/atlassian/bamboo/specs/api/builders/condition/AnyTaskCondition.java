package com.atlassian.bamboo.specs.api.builders.condition;

import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.plan.condition.AnyConditionProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public class AnyTaskCondition extends TaskCondition<AnyConditionProperties> {
    protected AtlassianModuleProperties atlassianPlugin;
    protected Map<String, String> configuration = new HashMap<>();

    /**
     * Specifies a condition of given type.
     *
     * @param atlassianModule type of the condition identified by its plugin module key
     * @see AtlassianModule
     */
    public AnyTaskCondition(@NotNull AtlassianModule atlassianModule) throws PropertiesValidationException {
        this.atlassianPlugin = EntityPropertiesBuilders.build(atlassianModule);
    }

    /**
     * Appends the condition configuration.
     *
     * The configuration should be in the format used by respective plugin. No syntactical nor semantic validation is
     * performed on the source data. The configuration is stored 'as is' in the Bamboo DB.
     */
    public AnyTaskCondition configuration(final Map<String, String> configuration) {
        this.configuration.putAll(configuration);
        return this;
    }

    @NotNull
    protected AnyConditionProperties build() throws PropertiesValidationException {
        return new AnyConditionProperties(atlassianPlugin, configuration);
    }
}
