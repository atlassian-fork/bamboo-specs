package com.atlassian.bamboo.specs.api.validators.plan;

import com.atlassian.bamboo.specs.api.model.plan.StageProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateDescription;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateName;

public final class StageValidator {
    private StageValidator() {
    }

    @NotNull
    public static List<ValidationProblem> validate(@NotNull final StageProperties stageProperties) {
        final ValidationContext context = ValidationContext.of("Stage");
        final List<ValidationProblem> errors = new ArrayList<>();

        errors.addAll(validateName(context, stageProperties.getName()));
        errors.addAll(validateDescription(context, stageProperties.getDescription()));

        return errors;
    }
}
