/**
 * Immutable permissions like plan permissions properties, deployment permissions properties etc.
 */
package com.atlassian.bamboo.specs.api.model.permission;
