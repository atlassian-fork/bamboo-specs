/**
 * Generic plan-local and linked source code repositories as well as change detection settings, see also com.atlassian.bamboo.specs.builders.repository for specific repository types.
 */
package com.atlassian.bamboo.specs.api.builders.repository;
