package com.atlassian.bamboo.specs.api.validators.repository;

import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateRequired;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateDescription;
import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateName;

public final class VcsRepositoryValidator {
    private VcsRepositoryValidator() {
    }

    public static List<ValidationProblem> validate(@NotNull final VcsRepositoryProperties vcsRepositoryProperties) {
        final List<ValidationProblem> errors = new ArrayList<>();

        final String parentName = vcsRepositoryProperties.getParentName();
        final ValidationContext context = ValidationContext.of("VCS repository");

        if (!vcsRepositoryProperties.hasParent()) {

            if (parentName != null) {
                errors.add(new ValidationProblem(context, "Global repository cannot have parent"));
            }

            errors.addAll(validateName(context, vcsRepositoryProperties.getName()));
            errors.addAll(validateDescription(context, vcsRepositoryProperties.getDescription()));

            validateRequired(context.with("Atlassian plugin"), vcsRepositoryProperties.getAtlassianPlugin())
                    .ifPresent(errors::add);
        } else {
            if (parentName != null) {
                if (vcsRepositoryProperties.getName() != null) {
                    errors.add(new ValidationProblem(context,
                            "Cannot set name when inheriting data from another repository definition and you have '%s'",
                            vcsRepositoryProperties.getName()));
                }

                if (vcsRepositoryProperties.getDescription() != null) {
                    errors.add(new ValidationProblem(context,
                            "Cannot set description when inheriting data from another repository definition and you have '%'",
                            vcsRepositoryProperties.getDescription()));
                }
            } else {
                //only needed if we accept plan repositories that have no parent
                errors.addAll(validateName(context, vcsRepositoryProperties.getName()));
                errors.addAll(validateDescription(context, vcsRepositoryProperties.getDescription()));
            }
        }

        return errors;
    }

}
