package com.atlassian.bamboo.specs.api.context;

import org.jetbrains.annotations.NotNull;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Optional;

/**
 * Runtime context for specs execution.
 * Can be altered by setting Java properties when running specs.
 * The properties are set automatically when executing specs in RSS mode.
 */
public final class RssRuntimeContext {
    private static final String RSS_CURRENT_BRANCH_PROPERTY_NAME = "rss.current.branch";
    private static final String RSS_IS_DEFAULT_BRANCH_PROPERTY_NAME = "rss.default.branch";

    private static String currentBranch;
    private static Boolean isDefaultBranch;

    static {
        loadProperties();
    }

    /**
     * @return branch of the repository from which currently executed specs are checked out.
     */
    @NotNull
    public static Optional<String> getCurrentRssBranch() {
        return Optional.ofNullable(currentBranch);
    }

    /**
     * @return true if the current branch is the default branch of the repository according to its configuration in Bamboo.
     */
    @NotNull
    public static Optional<Boolean> isDefaultRssBranch() {
        return Optional.ofNullable(isDefaultBranch);
    }

    private static void loadProperties() {
        String property = System.getProperty(RSS_CURRENT_BRANCH_PROPERTY_NAME);
        if (property != null) {
            try {
                currentBranch = new String(Base64.getDecoder().decode(property.getBytes(StandardCharsets.ISO_8859_1)), StandardCharsets.UTF_8);
            } catch (IllegalArgumentException e) {
                currentBranch = property;
            }
        }

        property = System.getProperty(RSS_IS_DEFAULT_BRANCH_PROPERTY_NAME);
        if (property != null) {
            isDefaultBranch = Boolean.valueOf(property);
        }
    }

    private RssRuntimeContext() {
    }
}
