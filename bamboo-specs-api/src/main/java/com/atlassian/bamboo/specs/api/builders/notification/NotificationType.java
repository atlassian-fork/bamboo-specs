package com.atlassian.bamboo.specs.api.builders.notification;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.model.notification.NotificationTypeProperties;
import org.jetbrains.annotations.NotNull;

/**
 * Represents notification type.
 */
public abstract class NotificationType<N extends NotificationType<N,P>, P extends NotificationTypeProperties> extends EntityPropertiesBuilder<P> {
    @NotNull
    @Override
    protected abstract P build();
}
