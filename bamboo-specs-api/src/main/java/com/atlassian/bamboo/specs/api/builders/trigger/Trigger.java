package com.atlassian.bamboo.specs.api.builders.trigger;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.model.trigger.TriggerProperties;

/**
 * Represents a definition of trigger.
 * <p>
 * This class contains common data only. In order to define a specific type of trigger one should use the specialised
 * implementation or, if such is not available, {@link AnyTrigger} class.
 */
public abstract class Trigger<T extends Trigger<T, P>, P extends TriggerProperties> extends EntityPropertiesBuilder<P> {
    protected boolean triggerEnabled;
    protected String name;
    protected String description;

    protected Trigger() {
        triggerEnabled = true;
        name = null;
        description = "";
    }

    /**
     * Sets the name of this trigger. Used as an visual identifier in Bamboo UI.
     * <p>
     * The name property must be specified by user
     */
    public T name(final String name) {
        this.name = name;
        return (T) this;
    }

    /**
     * Sets the trigger description. Defaults to empty value.
     */
    public T description(final String description) {
        this.description = description;
        return (T) this;
    }

    /**
     * Enables/disables the trigger. Defaults to enabled state.
     */
    public T enabled(final boolean taskEnabled) {
        this.triggerEnabled = taskEnabled;
        return (T) this;
    }

    protected abstract P build();
}
