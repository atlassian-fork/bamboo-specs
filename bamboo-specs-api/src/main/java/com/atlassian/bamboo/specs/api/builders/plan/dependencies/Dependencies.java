package com.atlassian.bamboo.specs.api.builders.plan.dependencies;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.plan.PlanIdentifierProperties;
import com.atlassian.bamboo.specs.api.model.plan.dependencies.DependenciesConfigurationProperties;
import com.atlassian.bamboo.specs.api.model.plan.dependencies.DependenciesProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents plan's dependencies and their configuration.
 */
public class Dependencies extends EntityPropertiesBuilder<DependenciesProperties> {
    private DependenciesConfigurationProperties dependenciesConfigurationProperties = new DependenciesConfiguration().build();
    private List<PlanIdentifierProperties> childPlans = new ArrayList<>();

    /**
     * Specifies dependencies with default configuration.
     */
    public Dependencies() {
    }

    /**
     * Sets dependencies configuration.
     *
     * @see DependenciesConfiguration
     */
    @NotNull
    public Dependencies configuration(@NotNull final DependenciesConfiguration dependenciesConfiguration) throws PropertiesValidationException {
        checkNotNull("configuration", dependenciesConfiguration);
        this.dependenciesConfigurationProperties = dependenciesConfiguration.build();
        return this;
    }

    /***
     * Adds child plans. When the current plan builds successfully, it will trigger the child plans to build.
     */
    @NotNull
    public Dependencies childPlans(@NotNull PlanIdentifier... childPlans) {
        checkNotNull("childPlan", childPlans);
        for (PlanIdentifier childPlan:childPlans) {
            checkNotNull("childPlan", childPlan);
            this.childPlans.add(EntityPropertiesBuilders.build(childPlan));
        }
        return this;
    }

    protected DependenciesProperties build() {
        return new DependenciesProperties(dependenciesConfigurationProperties, childPlans);
    }
}
