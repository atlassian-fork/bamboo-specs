package com.atlassian.bamboo.specs.api.model.plan.configuration;

import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

public class AllOtherPluginsConfigurationProperties implements PluginConfigurationProperties {
    public static final ValidationContext VALIDATION_CONTEXT = ValidationContext.of("all other plugins");
    public static final String ALL_OTHER_PLUGINS_MODULE_KEY = "com.atlassian.bamboo:#allotherplugins";
    private final Map<String, Object> configuration;

    private AllOtherPluginsConfigurationProperties() {
        configuration = new HashMap<>();
    }

    public AllOtherPluginsConfigurationProperties(Map<String, Object> configuration) {
        this.configuration = Collections.unmodifiableMap(new LinkedHashMap<>(configuration));
    }

    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return new AtlassianModuleProperties(ALL_OTHER_PLUGINS_MODULE_KEY);
    }

    public Map<String, Object> getConfiguration() {
        return configuration;
    }

    @Override
    public void validate() {
        ImporterUtils.checkNotNull(VALIDATION_CONTEXT, "configuration", configuration);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AllOtherPluginsConfigurationProperties that = (AllOtherPluginsConfigurationProperties) o;
        return Objects.equals(configuration, that.configuration);
    }

    @Override
    public int hashCode() {
        return Objects.hash(configuration);
    }
}
