/**
 * Common validators for strings, numbers, database constraints etc.
 */
package com.atlassian.bamboo.specs.api.validators.common;
