/**
 * Contains base @BambooSpec annotation used to mark classes containing configuration as code.
 */
package com.atlassian.bamboo.specs.api;
