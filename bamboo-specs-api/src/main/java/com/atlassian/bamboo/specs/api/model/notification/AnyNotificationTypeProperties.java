package com.atlassian.bamboo.specs.api.model.notification;

import com.atlassian.bamboo.specs.api.builders.Applicability;
import com.atlassian.bamboo.specs.api.codegen.annotations.CodeGeneratorName;
import com.atlassian.bamboo.specs.api.codegen.annotations.SkipCodeGen;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

@CodeGeneratorName("com.atlassian.bamboo.specs.codegen.emitters.notification.AnyNotificationTypeEmitter")
@Immutable
public final class AnyNotificationTypeProperties extends NotificationTypeProperties {

    private final AtlassianModuleProperties atlassianPlugin;
    private final String conditionString;
    @SkipCodeGen
    private final transient Set<Applicability> applicableTo;

    private AnyNotificationTypeProperties() {
        atlassianPlugin = null;
        conditionString = "";
        applicableTo = EnumSet.allOf(Applicability.class);
    }

    public AnyNotificationTypeProperties(@NotNull final AtlassianModuleProperties atlassianPlugin,
                                         final String conditionString) throws PropertiesValidationException {
        this.atlassianPlugin = atlassianPlugin;
        this.conditionString = conditionString;
        applicableTo = EnumSet.allOf(Applicability.class);
        validate();
    }

    public AnyNotificationTypeProperties(@NotNull final AtlassianModuleProperties atlassianPlugin,
                                         final String conditionString,
                                         final EnumSet<Applicability> applicableTo) throws PropertiesValidationException {
        this.atlassianPlugin = atlassianPlugin;
        this.conditionString = conditionString;
        this.applicableTo = Collections.unmodifiableSet(new HashSet<>(applicableTo));
        validate();
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return atlassianPlugin;
    }

    public String getConditionString() {
        return conditionString;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AnyNotificationTypeProperties that = (AnyNotificationTypeProperties) o;
        return Objects.equals(getAtlassianPlugin(), that.getAtlassianPlugin()) &&
                Objects.equals(getConditionString(), that.getConditionString());
    }

    @Override
    public Set<Applicability> applicableTo() {
        return applicableTo;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAtlassianPlugin(), getConditionString());
    }

    @Override
    public void validate() {
        checkNotNull("atlassianPlugin", atlassianPlugin);
    }
}
