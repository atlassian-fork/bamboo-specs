package com.atlassian.bamboo.specs.api.model.plan.artifact;

import com.atlassian.bamboo.specs.api.builders.plan.artifact.Artifact;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

@Immutable
public final class ArtifactProperties implements EntityProperties {

    public static final ValidationContext VALIDATION_CONTEXT = ValidationContext.of("Artifact");

    private final String name;
    private final String copyPattern;
    private final String location;
    private final boolean shared;
    private final boolean required;

    private ArtifactProperties() {
        shared = Artifact.SHARED_BY_DEFAULT;
        required = Artifact.REQUIRED_BY_DEFAULT;
        name = null;
        location = "";
        copyPattern = null;
    }

    public ArtifactProperties(final String name,
                              final String copyPattern,
                              final String location,
                              final boolean shared,
                              final boolean required) throws PropertiesValidationException {
        this.name = name;
        this.copyPattern = copyPattern;
        this.location = location;
        this.shared = shared;
        this.required = required;
        validate();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ArtifactProperties that = (ArtifactProperties) o;
        return isShared() == that.isShared() &&
                isRequired() == that.isRequired() &&
                Objects.equals(getName(), that.getName()) &&
                Objects.equals(getCopyPattern(), that.getCopyPattern()) &&
                Objects.equals(getLocation(), that.getLocation());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getCopyPattern(), getLocation(), isShared(), isRequired());
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.NO_CLASS_NAME_STYLE)
                .append("name", name)
                .append("copyPattern", copyPattern)
                .append("location", location)
                .append("shared", shared)
                .append("required", required)
                .build();
    }

    public String getName() {
        return name;
    }

    public String getCopyPattern() {
        return copyPattern;
    }

    public String getLocation() {
        return location;
    }

    public boolean isShared() {
        return shared;
    }

    public boolean isRequired() {
        return required;
    }

    @Override
    public void validate() {
        ImporterUtils.checkArgument(VALIDATION_CONTEXT, !StringUtils.contains(name,"/"),"Name can not contain '/' character");
    }
}
