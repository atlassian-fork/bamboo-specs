package com.atlassian.bamboo.specs.api.validators;

import com.atlassian.bamboo.specs.api.model.BambooKeyProperties;
import com.atlassian.bamboo.specs.api.validators.common.DbConstraintUtils;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public final class BambooKeyValidator {
    public static final String KEY_REGEXP = "[A-Z][A-Z0-9]{1,}";
    private static final Pattern KEY_PATTERN = Pattern.compile(KEY_REGEXP);

    private BambooKeyValidator() {
    }

    public static List<ValidationProblem> validate(@NotNull ValidationContext validationContext,
                                                   @NotNull BambooKeyProperties keyProperties) {
        final List<ValidationProblem> errors = new ArrayList<>();

        final String key = keyProperties.getKey();
        if (StringUtils.isEmpty(key)) {
            errors.add(new ValidationProblem(validationContext, "A key is required."));
        } else {
            if (!KEY_PATTERN.matcher(key).matches()) {
                errors.add(new ValidationProblem(validationContext.with(key),
                        "A key '%s' must consist of an uppercase letter followed by one or more uppercase alphanumeric characters.",
                        key));
            }

            if (key.length() > DbConstraintUtils.DATABASE_STRING_LIMIT) {
                errors.add(new ValidationProblem(validationContext.with(key),
                        "A key '%s' must not be longer than %d characters.",
                        key, DbConstraintUtils.DATABASE_STRING_LIMIT));
            }
        }

        return errors;
    }
}
