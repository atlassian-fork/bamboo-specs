package com.atlassian.bamboo.specs;

import com.atlassian.bamboo.specs.maven.BambooSpecsFileScanner;
import com.atlassian.bamboo.specs.maven.sandbox.PrivilegedThreads;
import com.atlassian.bamboo.specs.maven.sandbox.SpecsRunner;
import org.apache.maven.plugin.logging.SystemStreamLog;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.AccessControlException;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;

import static com.atlassian.bamboo.specs.SpecsRunnerTest.specRunEndedTl;
import static com.atlassian.bamboo.specs.SpecsRunnerTest.specRunStartedTl;
import static org.junit.jupiter.api.Assertions.assertThrows;

class AllSpecsRunnerTest {
    private static final SystemStreamLog LOG = new SystemStreamLog();

    @AfterEach
    void clearPermissionVerifiers() {
        PrivilegedThreads.resetThreadPermissionCheckers();
    }

    @Test
    void runAllBambooSpecs() throws IOException {
        specRunStartedTl.set(new AtomicReference<>());
        specRunEndedTl.set(new AtomicReference<>());

        final Path targetDir = Paths.get("target");
        final Path classPathRoot = targetDir.resolve("test-classes");

        final Collection<Path> classFiles = BambooSpecsFileScanner.getClassFiles(LOG, classPathRoot);

        final SpecsRunner specsRunner = new SpecsRunner(LOG, classFiles, getClass().getClassLoader(), Collections.emptySet());

        assertThrows(AccessControlException.class, () -> specsRunner.runSpecs(true));
    }
}
