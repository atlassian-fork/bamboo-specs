package com.atlassian.bamboo.specs;

import com.atlassian.bamboo.specs.maven.sandbox.PrivilegedThreads;
import com.atlassian.bamboo.specs.maven.sandbox.SpecsRunner;
import org.apache.maven.plugin.logging.SystemStreamLog;
import org.hamcrest.CoreMatchers;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.AccessControlException;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class SpecsRunnerTest {
    static ThreadLocal<AtomicReference<Class<?>>> specRunStartedTl = new InheritableThreadLocal<>();
    static ThreadLocal<AtomicReference<Class<?>>> specRunEndedTl = new InheritableThreadLocal<>();
    private static final SystemStreamLog LOG = new SystemStreamLog();
    
    @AfterEach
    void clearPermissionVerifiers() {
        PrivilegedThreads.resetThreadPermissionCheckers();
    }

    @Test
    void runsSimpleBambooSpecs() throws IOException, URISyntaxException {
        specRunStartedTl.set(new AtomicReference<>());
        specRunEndedTl.set(new AtomicReference<>());

        final Class<TestSimpleSpec> specClass = TestSimpleSpec.class;

        final SpecsRunner specsRunner = newSpecRunner(specClass);
        specsRunner.runSpecs(true);

        assertThat(specRunStartedTl.get().get(), equalTo(specClass));
        assertThat(specRunEndedTl.get().get(), equalTo(specClass));
    }

    @Test
    void runHostileBambooSpecs() throws IOException, URISyntaxException {
        specRunStartedTl.set(new AtomicReference<>());
        specRunEndedTl.set(new AtomicReference<>());

        final Class<TestHostileSpec> specClass = TestHostileSpec.class;

        final SpecsRunner specsRunner = newSpecRunner(specClass);

        assertThrows(AccessControlException.class, () -> specsRunner.runSpecs(true));

        assertThat(specRunStartedTl.get().get(), equalTo(specClass));
        assertThat(specRunEndedTl.get().get(), CoreMatchers.nullValue());

        final Path tempFile2 = Files.createTempFile(getClass().getName(), getClass().getName());
        Files.delete(tempFile2);
    }

    @NotNull
    private SpecsRunner newSpecRunner(final Class<?> specClass) {
        final String pathToClassWithoutRoot = specClass.getCanonicalName().replace(".", File.separator);

        return new SpecsRunner(LOG, Collections.singleton(Paths.get(pathToClassWithoutRoot)), getClass().getClassLoader(), Collections.emptySet());
    }
}
