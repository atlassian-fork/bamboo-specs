package com.atlassian.bamboo.specs.maven.sandbox;

import com.google.common.collect.Iterables;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.sql.SQLData;
import java.util.Set;

class StackTraceUtilsTest {

    @Test
    void gettingCallerStackWorks() {
        Assertions.assertNull(StackTraceUtils.getCallerStack(SQLData.class));
        Assertions.assertNotNull(StackTraceUtils.getCallerStack(getClass()));
    }

    @Test
    void retrievesCallStackCodeLocations() {
        final StackTraceElement[] stackTrace = {new StackTraceElement(getClass().getName(), "", "", 1)};
        final Set<File> classLocationsOnStack = StackTraceUtils.getClassLocationsOnStack(getClass().getClassLoader(), stackTrace);
        Assertions.assertTrue(Iterables.getOnlyElement(classLocationsOnStack).getPath().endsWith("test-classes"));
    }
}